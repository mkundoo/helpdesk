<?php

class StaffPosition {
    static function getIdByCode($code)
    {
        $sql = 'SELECT id FROM custom_staff_positions WHERE code = '.db_input($code);
        $result = db_query($sql);
        if($row = db_fetch_array($result)) {
            return $row['id'];
        }
        return null;
    }

    static function getNameByCode($code)
    {
        $sql = 'SELECT name FROM custom_staff_positions WHERE code = '.db_input($code);
        $result = db_query($sql);
        if($row = db_fetch_array($result)) {
            return $row['name'];
        }
        return null;
    }
}

?>