<?php

class ReportModel {

    const PERM_AGENTS = 'stats.agents';

    static protected $perms = array(
            self::PERM_AGENTS => array(
                'title' =>
                /* @trans */ 'Stats',
                'desc'  =>
                /* @trans */ 'Ability to view stats of other agents in allowed departments',
                'primary' => true,
            ));

    static function getPermissions() {
        return self::$perms;
    }
}

RolePermission::register(/* @trans */ 'Miscellaneous', ReportModel::getPermissions());

class OverviewReport {
    var $start;
    var $end;

    var $format;

    // Highcharts series name
    // Note: Do not interchange ordering in this array. If interchanged, then you will also have to modify sql query in function getHighchartPlotData()
    var $highchartSeriesNames = array('Created', 'Work-In-Progress', 'Overdue', 'Closed', 'Re-opened', 'Un-Assigned');

    function __construct($start, $end='now', $format=null) {
        global $cfg;

        $this->start = $start;
        $this->end = $end;
        $this->format = $format ?: $cfg->getDateFormat(true);
    }


    function getStartDate($format=null, $translate=true) {

        if (!$this->start)
            return '';

        $format =  $format ?: $this->format;
        if ($translate) {
            $format = str_replace(
                    array('y', 'Y', 'm'),
                    array('yy', 'yyyy', 'mm'),
                    $format);
        }

        return Format::date(Misc::dbtime($this->start), false, $format);
    }


    function getDateRange() {
        global $cfg;

        $start = $this->start ?: 'last month';
        $stop = $this->end ?: 'now';

        // Convert user time to db time
        $start = Misc::dbtime($start);
        // Stop time can be relative.
        if ($stop[0] == '+') {
            // $start time + time(X days)
            $now = time();
            $stop = $start + (strtotime($stop, $now)-$now);
        } else {
            $stop = Misc::dbtime($stop);
        }

        $start = 'FROM_UNIXTIME('.$start.')';
        $stop = 'FROM_UNIXTIME('.$stop.')';

        return array($start, $stop);
    }

    function getPlotData() {
        list($start, $stop) = $this->getDateRange();
        $states = array("created", "closed", "reopened", "assigned", "overdue", "transferred");
        $event_ids = Event::getIds();

        # Fetch all types of events over the timeframe
        $res = db_query('SELECT DISTINCT(E.name) FROM '.THREAD_EVENT_TABLE
            .' T JOIN '.EVENT_TABLE . ' E ON E.id = T.event_id'
            .' WHERE timestamp BETWEEN '.$start.' AND '.$stop
            .' AND T.event_id IN ('.implode(",",$event_ids).')'
            .' ORDER BY 1');
        $events = array();
        while ($row = db_fetch_row($res)) $events[] = $row[0];

        # TODO: Handle user => db timezone offset
        # XXX: Implement annulled column from the %ticket_event table
        $res = db_query('SELECT H.name, DATE_FORMAT(timestamp, \'%Y-%m-%d\'), '
                .'COUNT(DISTINCT T.id)'
            .' FROM '.THREAD_EVENT_TABLE. ' E '
            . ' LEFT JOIN '.EVENT_TABLE. ' H
                ON (E.event_id = H.id)'
            .' JOIN '.THREAD_TABLE. ' T
                ON (T.id = E.thread_id AND T.object_type = "T") '
            .' WHERE E.timestamp BETWEEN '.$start.' AND '.$stop
            .' AND NOT annulled'
            .' AND E.event_id IN ('.implode(",",$event_ids).')'
            .' GROUP BY E.event_id, DATE_FORMAT(E.timestamp, \'%Y-%m-%d\')'
            .' ORDER BY 2, 1');
        # Initialize array of plot values
        $plots = array();
        foreach ($events as $e) { $plots[$e] = array(); }

        $time = null; $times = array();
        # Iterate over result set, adding zeros for missing ticket events
        $slots = array();
        while ($row = db_fetch_row($res)) {
            $row_time = strtotime($row[1]);
            if ($time != $row_time) {
                # New time (and not the first), figure out which events did
                # not have any tickets associated for this time slot
                if ($time !== null) {
                    # Not the first record -- add zeros all the arrays that
                    # did not have at least one entry for the timeframe
                    foreach (array_diff($events, $slots) as $slot)
                        $plots[$slot][] = 0;
                }
                $slots = array();
                $times[] = $time = $row_time;
            }
            # Keep track of states for this timeframe
            $slots[] = $row[0];
            $plots[$row[0]][] = (int)$row[2];
        }
        foreach (array_diff($events, $slots) as $slot)
            $plots[$slot][] = 0;

        return array("times" => $times, "plots" => $plots, "events" => $events);
    }

    function enumTabularGroups() {
        return array(/* "dept"=>__("Department"), */ "topic"=>__("Category"),"priority"=>__("Priority"), "usertype"=>__("User Type"), "serviceprovider"=>__("Service Provider")
            # XXX: This will be relative to permissions based on the
            # logged-in-staff. For basic staff, this will be 'My Stats'
        /* "staff"=>__("Agent") */ );
    }

    function getTabularData($group='dept', $startDate = null, $endDate = null, $subCompanyIds = null) {
        global $thisstaff;
        $stats = null;
        $timings = null;
        $headers = null;
        
        if ($subCompanyIds !== null)
        {
            $event_ids = Event::getIds();
            $event = function ($name) use ($event_ids) {
                return $event_ids[$name];
            };

            list($start, $stop) = $this->getDateRange();
            $times = ThreadEvent::objects()
                ->constrain(array(
                    'thread__entries' => array(
                        'thread__entries__type' => 'R',
                        ),
                ))
                ->constrain(array(
                    'thread__events' => array(
                        'thread__events__event_id' => $event('created'),
                        'event_id' => $event('closed'),
                        'annulled' => 0,
                        ),
                    ))
                ->filter(array(
                        'timestamp__range' => array($start, $stop, true),
                ))
                ->aggregate(array(
                    'ServiceTime' => SqlAggregate::AVG(SqlFunction::timestampdiff(
                    new SqlCode('HOUR'), new SqlField('thread__events__timestamp'), new SqlField('timestamp'))
                    ),
                    /* 'ResponseTime' => SqlAggregate::AVG(SqlFunction::timestampdiff(
                        new SqlCode('HOUR'),new SqlField('thread__entries__parent__created'), new SqlField('thread__entries__created')
                    )), */
                ));

                $stats = ThreadEvent::objects()
                    ->filter(array(
                            'annulled' => 0,
                            'timestamp__range' => array($start, $stop, true),
                            'thread__object_type' => 'T',
                    ))
                    ->aggregate(array(
                        'Opened' => SqlAggregate::COUNT(
                            SqlCase::N()
                                ->when(new Q(array('event_id' => $event('created'))), 1)
                        ),
                        'Assigned' => SqlAggregate::COUNT(
                            SqlCase::N()
                                ->when(new Q(array('event_id' => $event('assigned'))), 1)
                        ),
                        'Overdue' => SqlAggregate::COUNT(
                            SqlCase::N()
                                ->when(new Q(array('event_id' => $event('overdue'))), 1)
                        ),
                        'Closed' => SqlAggregate::COUNT(
                            SqlCase::N()
                                ->when(new Q(array('event_id' => $event('closed'))), 1)
                        ),
                        'Reopened' => SqlAggregate::COUNT(
                            SqlCase::N()
                                ->when(new Q(array('event_id' => $event('reopened'))), 1)
                        ),
                        'Deleted' => SqlAggregate::COUNT(
                            SqlCase::N()
                                ->when(new Q(array('event_id' => $event('deleted'))), 1)
                        ),
                    ));

            $rows = array();
            $cols = array('Opened', 'Assigned', 'Overdue', 'Closed', 'Reopened', 'UnAssigned'); // array('Opened', 'Assigned', 'Overdue', 'Closed', 'Reopened', 'Deleted', 'Service Time');
            switch ($group) {
            case 'dept':
                $headers = array(__('Department'));
                $header = function($row) { return Dept::getLocalNameById($row['dept_id'], $row['dept__name']); };
                $pk = 'dept__id';
                $stats = $stats
                    ->filter(array('dept_id__in' => $thisstaff->getDepts()))
                    ->values('dept__id', 'dept__name', 'dept__flags')
                    ->distinct('dept__id');
                $times = $times
                    ->filter(array('dept_id__in' => $thisstaff->getDepts()))
                    ->values('dept__id')
                    ->distinct('dept__id');
                break;

            case 'topic':
                $headers = array(__('Help Topic'));
                $header = function($row) { return Topic::getLocalNameById($row['topic_id'], $row['topic__topic']); };
                $pk = 'topic_id';
                $topics = Topic::getHelpTopics(false, Topic::DISPLAY_DISABLED);
                $stats = $stats
                    ->values('topic_id', 'topic__topic', 'topic__flags')
                    ->filter(array('dept_id__in' => $thisstaff->getDepts(), 'topic_id__gt' => 0, 'topic_id__in' => array_keys($topics)))
                    ->distinct('topic_id');
                $times = $times
                    ->values('topic_id')
                    ->filter(array('topic_id__gt' => 0))
                    ->distinct('topic_id');


                $topics = Topic::getHelpTopics(false);
                $stats = null;
                $data = array();
                if (count($topics) !== 0)
                {
                    $count = 0;
                    foreach($topics as $key=>$topic)
                    {
                        $data[$key][] = $topic;
                        $sql = '';

                        foreach ($cols as $index => $col)
                    {
                        if ($col === 'Opened') // Created
                        {
                            // $sql .= "SELECT count(*) as total FROM ost_ticket INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket__cdata.priority =".$priority['priority']." and ost_ticket.status_id = 1 and ost_ticket.created >='".$startDate." 00:00:00' and ost_ticket.created <='".$endDate." 23:59:59' ";
                            $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_thread_event.topic_id =".$key." and ost_event.name = 'created' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                        }

                        if ($col === 'Assigned') // Work-In-Progress
                        {
                            // $sql .= "SELECT count(*) as total FROM ost_ticket INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket__cdata.priority =".$priority['priority']." and ost_ticket.status_id = 1 and ost_ticket.staff_id != 0 and ost_ticket.status_id = 1 and ost_ticket.created >='".$startDate."  00:00:00' and ost_ticket.created <='".$endDate." 23:59:59' ";
                            // $sql = "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and  ost_thread.object_type = 'T' and ost_thread_event.topic_id =".$key." and ost_event.name = 'assigned' and ost_ticket.staff_id != 0 and ost_ticket.status_id = 1 and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                        
                            $sql .= "SELECT count(a.ticket_id) as total 
                                    FROM (
                                        SELECT distinct ost_ticket.ticket_id
                                        FROM ost_ticket
                                        INNER JOIN ost_task ON ost_task.object_id = ost_ticket.ticket_id
                                        WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).")  
                                        AND ost_task.created <= '".$endDate." 23:59:59' 
                                        AND ost_ticket.status_id = 1
                                        AND ost_task.closed IS NULL
                                        AND ost_ticket.topic_id=".$key."
                                    ) a  ";
                        }

                        if ($col === 'Overdue')
                        {
                            // $sql .= "SELECT count(*) as total FROM ost_ticket INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket__cdata.priority =".$priority['priority']." and ost_ticket.status_id = 1 and ost_ticket.isoverdue = 1 and ost_ticket.created >='".$startDate." 00:00:00' and ost_ticket.created <='".$endDate." 23:59:59' ";
                            $sql .= "SELECT COUNT(a.object_id) FROM (SELECT distinct ost_thread.object_id FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and  ost_thread.object_type = 'T' and ost_thread_event.topic_id =".$key." and ost_event.name = 'overdue' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59') a ";
                        }

                        if ($col === 'Closed')
                        {
                            // $sql .= "SELECT count(*) as total FROM ost_ticket INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket__cdata.priority =".$priority['priority']." and ost_ticket.status_id = 3 and ost_ticket.closed >='".$startDate." 00:00:00' and ost_ticket.closed <='".$endDate." 23:59:59' ";
                            $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and  ost_thread.object_type = 'T' and ost_thread_event.topic_id =".$key." and ost_event.name = 'closed' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                        }

                        if ($col === 'Reopened')
                        {
                            // $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_thread.object_id INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id WHERE ost_ticket__cdata.priority =".$priority['priority']." and ost_thread.object_type = 'T' and ost_thread_event.annulled = 0 and ost_thread_event.event_id ='".$event('reopened')."' ";
                            $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and  ost_thread.object_type = 'T' and ost_thread_event.topic_id =".$key." and ost_event.name = 'reopened' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                        }

                        /*
                        if ($col === 'Deleted')
                        {
                            // $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_thread.object_id INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id WHERE ost_ticket__cdata.priority =".$priority['priority']." and ost_thread.object_type = 'T' and ost_thread_event.annulled = 0 and ost_thread_event.event_id ='".$event('deleted')."' ";
                            $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and  ost_thread.object_type = 'T' and ost_thread_event.topic_id =".$key." and ost_event.name = 'deleted' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                        }
                        */

                        if ($col === 'UnAssigned') // Tickets with no task
                        {
                            $sql .= "SELECT COUNT(*) as total
                                FROM ost_ticket
                                LEFT JOIN ost_task ON ost_task.object_id = ost_ticket.ticket_id
                                WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).")
                                and ost_task.id IS NULL
                                AND ost_ticket.status_id = 1
                                AND ost_ticket.created <= '".$endDate." 23:59:59'
                                AND ost_ticket.topic_id=".$key." ";
                        }


                        if (count($cols) !== ($index +1))
                        {
                            $sql .= " UNION ALL ";
                        }
                        else {
                            $sql .= ";";
                        }
                    }

                    // Execute SQL
                    $results = db_query($sql);
      
                    // Populate array with data returned by sql query
                    while ($row = db_fetch_array($results))
                    {
                        $data[$key][] = (int)$row['total'];
                    }

                    // Set dummy data for Service Time until it is implemente above.
                    $data[$key][] = (float)0.0;
                    }
                }

                $rows = $data;
                break;

            case 'staff':
                $headers = array(__('Agent'));
                $header = function($row) { return new AgentsName(array(
                    'first' => $row['staff__firstname'], 'last' => $row['staff__lastname'])); };
                $pk = 'staff_id';
                $staff = Staff::getStaffMembers();
                $stats = $stats
                    ->values('staff_id', 'staff__firstname', 'staff__lastname')
                    ->filter(array('staff_id__in' => array_keys($staff)))
                    ->distinct('staff_id');
                $times = $times->values('staff_id')->distinct('staff_id');
                $depts = $thisstaff->getManagedDepartments();
                if ($thisstaff->hasPerm(ReportModel::PERM_AGENTS))
                    $depts = array_merge($depts, $thisstaff->getDepts());
                $Q = Q::any(array(
                    'staff_id' => $thisstaff->getId(),
                ));
                if ($depts)
                    $Q->add(array('dept_id__in' => $depts));
                $stats = $stats->filter(array('staff_id__gt'=>0))->filter($Q);
                $times = $times->filter(array('staff_id__gt'=>0))->filter($Q);
                break;

            case 'priority':
                $headers = array(__('Priority'));
                $stats = null;
                $times = null;
                // break;
                $priorities = array( array('status' => 'High', 'priority' => 3), array('status' => 'Medium', 'priority' => 2), array('status' => 'Low', 'priority' => 1));
                $data = array();
                foreach ($priorities as $key=>$priority)
                {
                    $data[$key][] = $priority['status'];
                    $sql = '';
                    foreach ($cols as $index => $col)
                    {
                        if ($col === 'Opened')
                        {
                            // $sql .= "SELECT count(*) as total FROM ost_ticket INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket__cdata.priority =".$priority['priority']." and ost_ticket.status_id = 1 and ost_ticket.created >='".$startDate." 00:00:00' and ost_ticket.created <='".$endDate." 23:59:59' ";
                            $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and  ost_ticket__cdata.priority =".$priority['priority']." and ost_event.name = 'created' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                        }

                        if ($col === 'Assigned')
                        {
                            // $sql .= "SELECT count(*) as total FROM ost_ticket INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket__cdata.priority =".$priority['priority']." and ost_ticket.status_id = 1 and ost_ticket.staff_id != 0 and ost_ticket.status_id = 1 and ost_ticket.created >='".$startDate."  00:00:00' and ost_ticket.created <='".$endDate." 23:59:59' ";
                            // $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and  ost_thread.object_type = 'T' and  ost_ticket__cdata.priority =".$priority['priority']." and ost_event.name = 'assigned' and ost_ticket.staff_id != 0 and ost_ticket.status_id = 1 and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                            $sql .= "SELECT count(a.ticket_id) as total 
                                FROM (
                                    SELECT distinct ost_ticket.ticket_id
                                    FROM ost_ticket
                                    INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id
                                    INNER JOIN ost_task ON ost_task.object_id = ost_ticket.ticket_id
                                    WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") 
                                    AND ost_task.created <= '".$endDate." 23:59:59' 
                                    AND ost_ticket.status_id = 1
                                    AND ost_task.closed IS NULL
                                    AND ost_ticket__cdata.priority =".$priority['priority']." 
                                ) a  ";
                        }

                        if ($col === 'Overdue')
                        {
                            // $sql .= "SELECT count(*) as total FROM ost_ticket INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket__cdata.priority =".$priority['priority']." and ost_ticket.status_id = 1 and ost_ticket.isoverdue = 1 and ost_ticket.created >='".$startDate." 00:00:00' and ost_ticket.created <='".$endDate." 23:59:59' ";
                            $sql .= "SELECT COUNT(a.object_id) as total FROM (SELECT distinct ost_thread.object_id FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and  ost_thread.object_type = 'T' and ost_ticket__cdata.priority =".$priority['priority']." and ost_event.name = 'overdue' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59') a ";
                        }

                        if ($col === 'Closed')
                        {
                            // $sql .= "SELECT count(*) as total FROM ost_ticket INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket__cdata.priority =".$priority['priority']." and ost_ticket.status_id = 3 and ost_ticket.closed >='".$startDate." 00:00:00' and ost_ticket.closed <='".$endDate." 23:59:59' ";
                            $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and  ost_thread.object_type = 'T' and ost_ticket__cdata.priority =".$priority['priority']." and ost_event.name = 'closed' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                        }

                        if ($col === 'Reopened')
                        {
                            // $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_thread.object_id INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id WHERE ost_ticket__cdata.priority =".$priority['priority']." and ost_thread.object_type = 'T' and ost_thread_event.annulled = 0 and ost_thread_event.event_id ='".$event('reopened')."' ";
                            $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and  ost_thread.object_type = 'T' and ost_ticket__cdata.priority =".$priority['priority']." and ost_event.name = 'reopened' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                        }

                        /*
                        if ($col === 'Deleted')
                        {
                            // $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_thread.object_id INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id WHERE ost_ticket__cdata.priority =".$priority['priority']." and ost_thread.object_type = 'T' and ost_thread_event.annulled = 0 and ost_thread_event.event_id ='".$event('deleted')."' ";
                            $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and  ost_thread.object_type = 'T' and ost_ticket__cdata.priority =".$priority['priority']." and ost_event.name = 'deleted' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                        }
                        */

                        if ($col === 'UnAssigned') // Tickets with no task
                        {
                            $sql .= "SELECT COUNT(*) as total
                                FROM ost_ticket
                                INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id
                                LEFT JOIN ost_task ON ost_task.object_id = ost_ticket.ticket_id
                                WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).")
                                and ost_task.id IS NULL
                                AND ost_ticket.status_id = 1
                                AND ost_ticket.created <= '".$endDate." 23:59:59'
                                AND ost_ticket__cdata.priority =".$priority['priority']." ";
                        }

                        if (count($cols) !== ($index +1))
                        {
                            $sql .= " UNION ALL ";
                        }
                        else {
                            $sql .= ";";
                        }
                    }

                    // Execute SQL
                    $results = db_query($sql);
            
                    // Populate array with data returned by sql query
                    while ($row = db_fetch_array($results))
                    {
                        $data[$key][] = (int)$row['total'];
                    }

                    // Set dummy data for Service Time until it is implemente above.
                    $data[$key][] = (float)0.0;
                }

                $rows = $data;           
                break;

            case "usertype":
                $headers = array(__('User Type'));
                $data = array();
                $stats = null;
                $times = null;

                // Fetch all User Types from table custom_request_from_list_items
                $sql = "SELECT id, code, value FROM custom_request_from_list_items WHERE active = 1;";
                $results = db_query($sql);
                if($results->num_rows > 0)
                {
                    $usertypes = array();
                    while ($row = db_fetch_array($results))
                    {
                        $usertypes[] = $row;
                    }
                    
                    foreach ($usertypes as $key=>$usertype)
                    {
                        $data[$key][] = $usertype['value'];
                        $sql = '';
                        foreach ($cols as $index => $col)
                        {
                            if ($col === 'Opened')
                            {
                                // $sql .= "SELECT count(*) as total FROM ost_ticket WHERE ost_ticket.request_from_list_item_id =".$usertype['id']." and ost_ticket.status_id = 1 ";
                                $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_ticket.request_from_list_item_id =".$usertype['id']." and ost_event.name = 'created' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                            }

                            if ($col === 'Assigned')
                            {
                                // $sql .= "SELECT count(*) as total FROM ost_ticket WHERE ost_ticket.request_from_list_item_id =".$usertype['id']." and ost_ticket.status_id = 1 and ost_ticket.staff_id != 0 and ost_ticket.team_id != 0 ";
                                // $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_ticket.request_from_list_item_id =".$usertype['id']." and ost_event.name = 'assigned' and ost_ticket.staff_id != 0 and ost_ticket.status_id = 1 and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                                $sql .= "SELECT count(a.ticket_id) as total 
                                    FROM (
                                        SELECT distinct ost_ticket.ticket_id
                                        FROM ost_ticket
                                        INNER JOIN ost_task ON ost_task.object_id = ost_ticket.ticket_id
                                        WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") 
                                        AND ost_task.created <= '".$endDate." 23:59:59' 
                                        AND ost_ticket.status_id = 1
                                        AND ost_task.closed IS NULL
                                        AND ost_ticket.request_from_list_item_id =".$usertype['id']." 
                                    ) a  ";
                            }

                            if ($col === 'Overdue')
                            {
                                // $sql .= "SELECT count(*) as total FROM ost_ticket WHERE ost_ticket.request_from_list_item_id =".$usertype['id']." and ost_ticket.status_id = 1 and ost_ticket.isoverdue = 1 ";
                                $sql .= "SELECT COUNT(a.object_id) as total FROM (SELECT distinct ost_thread.object_id FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_ticket.request_from_list_item_id =".$usertype['id']." and ost_event.name = 'overdue' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59') a ";
                            }

                            if ($col === 'Closed')
                            {
                                // $sql .= "SELECT count(*) as total FROM ost_ticket WHERE ost_ticket.request_from_list_item_id =".$usertype['id']." and ost_ticket.status_id = 3 ";
                                $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_ticket.request_from_list_item_id =".$usertype['id']." and ost_event.name = 'closed' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                            }

                            if ($col === 'Reopened')
                            {
                                // $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id WHERE ost_ticket.request_from_list_item_id =".$usertype['id']." and ost_thread.object_type = 'T' and ost_thread_event.annulled = 0 and ost_thread_event.event_id ='".$event('reopened')."' ";
                                $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_ticket.request_from_list_item_id =".$usertype['id']." and ost_event.name = 'reopened' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                            }

                            /*
                            if ($col === 'Deleted')
                            {
                                // $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id  WHERE ost_ticket.request_from_list_item_id =".$usertype['id']." and ost_thread.object_type = 'T' and ost_thread_event.annulled = 0 and ost_thread_event.event_id ='".$event('deleted')."' ";
                                $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_ticket.request_from_list_item_id =".$usertype['id']." and ost_event.name = 'deleted' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                            }
                            */

                            if ($col === 'UnAssigned') // Tickets with no task
                            {
                                $sql .= "SELECT COUNT(*) as total
                                    FROM ost_ticket
                                    LEFT JOIN ost_task ON ost_task.object_id = ost_ticket.ticket_id
                                    WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).")
                                    and ost_task.id IS NULL
                                    AND ost_ticket.status_id = 1
                                    AND ost_ticket.created <= '".$endDate." 23:59:59'
                                    AND ost_ticket.request_from_list_item_id =".$usertype['id']." ";
                            }

                            if (count($cols) !== ($index +1))
                            {
                                $sql .= " UNION ALL ";
                            }
                            else {
                                $sql .= ";";
                            }
                        }

                        // Execute SQL
                        $results = db_query($sql);
                
                        // Populate array with data returned by sql query
                        while ($row = db_fetch_array($results))
                        {
                            $data[$key][] = (int)$row['total'];
                        }

                        // Set dummy data for Service Time until it is implemente above.
                        $data[$key][] = (float)0.0;
                    }
                }
                else
                {
                    // No data to display
                    $stats = null;
                    $times = null;
                }

                $rows = $data; 
                break;

            case "serviceprovider":
                $headers = array(__('Service Provider'));
                $data = array();
                $stats = null;
                $times = null;

                // Fetch all User Types from table custom_request_from_list_items
                $sql = "SELECT id, value FROM custom_service_providers WHERE active = 1;";
                $results = db_query($sql);
                if($results->num_rows > 0)
                {
                    $serviceproviders = array();
                    while ($row = db_fetch_array($results))
                    {
                        $serviceproviders[] = $row;
                    }
                    
                    foreach ($serviceproviders as $key=>$serviceprovider)
                    {
                        $data[$key][] = $serviceprovider['value'];
                        $sql = '';
                        foreach ($cols as $index => $col)
                        {
                            if ($col === 'Opened')
                            {
                                // $sql .= "SELECT count(*) as total FROM ost_ticket WHERE ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." and ost_ticket.status_id = 1 ";
                                $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." and ost_event.name = 'created' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                            }

                            if ($col === 'Assigned')
                            {
                                // $sql .= "SELECT count(*) as total FROM ost_ticket WHERE ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." and ost_ticket.status_id = 1 and ost_ticket.staff_id != 0 and ost_ticket.team_id != 0 ";
                                // $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." and ost_event.name = 'assigned' and ost_ticket.staff_id != 0 and ost_ticket.status_id = 1 and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                                $sql .= "SELECT count(a.ticket_id) as total 
                                    FROM (
                                        SELECT distinct ost_ticket.ticket_id
                                        FROM ost_ticket
                                        INNER JOIN ost_task ON ost_task.object_id = ost_ticket.ticket_id
                                        WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") 
                                        AND ost_task.created <= '".$endDate." 23:59:59' 
                                        AND ost_ticket.status_id = 1
                                        AND ost_task.closed IS NULL
                                        AND ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." 
                                    ) a  ";
                            }

                            if ($col === 'Overdue')
                            {
                                // $sql .= "SELECT count(*) as total FROM ost_ticket WHERE ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." and ost_ticket.status_id = 1 and ost_ticket.isoverdue = 1 ";
                                $sql .= "SELECT COUNT(a.object_id) as total FROM (SELECT distinct ost_thread.object_id FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." and ost_event.name = 'overdue' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59') a ";
                            }

                            if ($col === 'Closed')
                            {
                                // $sql .= "SELECT count(*) as total FROM ost_ticket WHERE ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." and ost_ticket.status_id = 3 ";
                                $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." and ost_event.name = 'closed' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                            }

                            if ($col === 'Reopened')
                            {
                                // $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id WHERE ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." and ost_thread.object_type = 'T' and ost_thread_event.annulled = 0 and ost_thread_event.event_id ='".$event('reopened')."' ";
                                $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." and ost_event.name = 'reopened' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                            }

                            /*
                            if ($col === 'Deleted')
                            {
                                // $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id  WHERE ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." and ost_thread.object_type = 'T' and ost_thread_event.annulled = 0 and ost_thread_event.event_id ='".$event('deleted')."' ";
                                $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." and ost_event.name = 'deleted' and ost_thread_event.timestamp >='".$startDate." 00:00:00' and ost_thread_event.timestamp <='".$endDate." 23:59:59' ";
                            }
                            */

                            if ($col === 'UnAssigned') // Tickets with no task
                            {
                                $sql .= "SELECT COUNT(*) as total
                                    FROM ost_ticket
                                    LEFT JOIN ost_task ON ost_task.object_id = ost_ticket.ticket_id
                                    WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).")
                                    and ost_task.id IS NULL
                                    AND ost_ticket.status_id = 1
                                    AND ost_ticket.created <= '".$endDate." 23:59:59'
                                    AND ost_ticket.request_from_service_provider_id =".$serviceprovider['id']." ";
                            }

                            if (count($cols) !== ($index +1))
                            {
                                $sql .= " UNION ALL ";
                            }
                            else {
                                $sql .= ";";
                            }
                        }

                        // Execute SQL
                        $results = db_query($sql);
                
                        // Populate array with data returned by sql query
                        while ($row = db_fetch_array($results))
                        {
                            $data[$key][] = (int)$row['total'];
                        }

                        // Set dummy data for Service Time until it is implemente above.
                        $data[$key][] = (float)0.0;
                    }
                }
                else
                {
                    // No data to display
                    $stats = null;
                    $times = null;
                }

                $rows = $data; 
                break;

            default:
                # XXX: Die if $group not in $groups
            }

            $timings = array();
            foreach ($times as $T) {
                $timings[$T[$pk]] = $T;
            }
            
            foreach ($stats as $R) {
            if (isset($R['dept__flags'])) {
                if ($R['dept__flags'] & Dept::FLAG_ARCHIVED)
                $status = ' - '.__('Archived');
                elseif ($R['dept__flags'] & Dept::FLAG_ACTIVE)
                $status = '';
                else
                $status = ' - '.__('Disabled');
            }
            if (isset($R['topic__flags'])) {
                if ($R['topic__flags'] & Topic::FLAG_ARCHIVED)
                $status = ' - '.__('Archived');
                elseif ($R['topic__flags'] & Topic::FLAG_ACTIVE)
                $status = '';
                else
                $status = ' - '.__('Disabled');
            }

                $T = $timings[$R[$pk]];
                $rows[] = array($header($R) . $status, $R['Opened'], $R['Assigned'],
                    $R['Overdue'], $R['Closed'], $R['Reopened'], $R['Deleted'],
                    number_format($T['ServiceTime'], 1));
            }
        }

        // If the order of the columns is changed here, please make sure to modify sql query for Priority above.
        return array("columns" => array_merge($headers,
                        array(__('Created'),__('Work-In-Progress'),__('Overdue'),__('Closed'),__('Re-opened'),
                              __('Un-Assigned'),__('Service Time'))),
                     "data" => $rows);
    }


    function getHighchartPlotData($chartXAxisDates, $subCompanyIds = null) {
        $sql = '';
        $countDates = count($chartXAxisDates);
        $plotData = array();

        if ($subCompanyIds !== null)
        {   
            foreach($this->highchartSeriesNames as $highchartSeriesName)
            {
                // Build SQL
                $sql = '';
                $count = 1;
                $data = array();
                foreach ($chartXAxisDates as $chartXAxisDate)
                {
                    if ($highchartSeriesName === 'Created')
                    {
                        // $sql .= "SELECT count(*) as total FROM ost_ticket WHERE status_id = 1 AND created >= '".date('Y-m-d', strtotime($chartXAxisDate))."' AND created < '".date('Y-m-d', strtotime('+1 days', strtotime($chartXAxisDate)))."' ";
                        $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and  ost_event.name = 'created' and ost_thread_event.timestamp >='".date('Y-m-d 00:00:00', strtotime($chartXAxisDate))."' and ost_thread_event.timestamp <'".date('Y-m-d 00:00:00', strtotime('+1 days', strtotime($chartXAxisDate)))."' ";
                    }
                    elseif ($highchartSeriesName === 'Closed')
                    {
                        // $sql .= "SELECT count(*) as total FROM ost_ticket WHERE status_id = 3 AND closed >= '".date('Y-m-d', strtotime($chartXAxisDate))."' AND closed < '".date('Y-m-d', strtotime('+1 days', strtotime($chartXAxisDate)))."' ";
                        $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_event.name = 'closed' and ost_thread_event.timestamp >='".date('Y-m-d 00:00:00', strtotime($chartXAxisDate))."' and ost_thread_event.timestamp <'".date('Y-m-d 00:00:00', strtotime('+1 days', strtotime($chartXAxisDate)))."' ";
                    }
                    elseif ($highchartSeriesName === 'Re-opened')
                    {
                        // $sql .= "SELECT count(*) as total FROM ost_ticket WHERE status_id = 1 AND reopened >= '".date('Y-m-d', strtotime($chartXAxisDate))."' AND reopened < '".date('Y-m-d', strtotime('+1 days', strtotime($chartXAxisDate)))."' ";
                        $sql .= "SELECT count(*) as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_event.name = 'reopened' and ost_thread_event.timestamp >='".date('Y-m-d 00:00:00', strtotime($chartXAxisDate))."' and ost_thread_event.timestamp <'".date('Y-m-d 00:00:00', strtotime('+1 days', strtotime($chartXAxisDate)))."' ";
                    }
                    elseif ($highchartSeriesName === 'Work-In-Progress')
                    {
                        // $sql = "SELECT distinct ost_ticket.number FROM ost_ticket inner join ost_task on ost_task.object_id = ost_ticket.ticket_id where ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_ticket.status_id = 1 AND created <= '".date('Y-m-d', strtotime($chartXAxisDate))."' ";
                        $sql = "SELECT distinct ost_ticket.ticket_id
                            FROM ost_ticket
                            INNER JOIN ost_task ON ost_task.object_id = ost_ticket.ticket_id
                            WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") 
                            AND ost_task.created <= '".date('Y-m-d', strtotime($chartXAxisDate))."' 
                            AND ost_ticket.status_id = 1
                            AND ost_task.closed IS NULL ";

                        $results = db_query($sql);
                        if($row = db_fetch_array($results))
                        {
                            $data[] = (int)$results->num_rows;
                        }
                        else {
                            $data[] = 0;
                        }
                    }
                    elseif ($highchartSeriesName === 'Un-Assigned')
                    {
                        $sql .= "SELECT COUNT(*) as total FROM ost_ticket LEFT JOIN ost_task ON ost_task.object_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_task.id IS NULL AND ost_ticket.status_id = 1 AND ost_ticket.created <= '".date('Y-m-d', strtotime($chartXAxisDate))." 23:59:59' ";
                    }
                    elseif ($highchartSeriesName === 'Overdue')
                    {
                        // $sql .= "SELECT count(*) as total FROM ost_ticket WHERE status_id = 1 AND isoverdue = 1 AND est_duedate <= '".date('Y-m-d', strtotime('-1 days',strtotime($chartXAxisDate)))."' ";
                        $sql = "SELECT ost_thread.object_id as total FROM ost_thread INNER JOIN ost_thread_event on ost_thread_event.thread_id = ost_thread.id LEFT JOIN ost_event on ost_event.id = ost_thread_event.event_id INNER JOIN ost_ticket on ost_ticket.ticket_id = ost_thread.object_id INNER JOIN ost_ticket__cdata on ost_ticket__cdata.ticket_id = ost_ticket.ticket_id WHERE ost_ticket.ar_sub_company_id IN(".implode(',', $subCompanyIds).") and ost_thread.object_type = 'T' and ost_event.name = 'overdue' and ost_thread_event.timestamp >='".date('Y-m-d 00:00:00', strtotime($chartXAxisDate))."' and ost_thread_event.timestamp <'".date('Y-m-d 00:00:00', strtotime('+1 days', strtotime($chartXAxisDate)))."' GROUP BY ost_thread.object_id ";
                        $results = db_query($sql);
                        if($row = db_fetch_array($results))
                        {
                            $data[] = (int)$results->num_rows;
                        }
                        else {
                            $data[] = 0;
                        }
                    }
                    
                    if ($count !== $countDates) {
                        $sql .= ' UNION ALL ';
                    }
                    else {
                        $sql .= ';';
                    }
                    $count++;
                }
                
                if ($highchartSeriesName !== 'Work-In-Progress' && $highchartSeriesName !== 'Overdue')
                {
                    // Execute SQL
                    $results = db_query($sql);

                    // Populate array with data returned by sql query
                    while ($row = db_fetch_array($results))
                    {
                        $data[] = (int)$row['total'];
                    }
                }

                $plotData[] = array(
                    'name' => $highchartSeriesName,
                    'data' => $data
                );
            }
        }
       
        return $plotData;
    }
}
