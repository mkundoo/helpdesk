<?php
//Note that ticket obj is initiated in tickets.php.
if(!defined('OSTSCPINC') || !$thisstaff || !is_object($ticket) || !$ticket->getId()) die('Invalid path');

//Make sure the staff is allowed to access the page.
if(!@$thisstaff->isStaff() || !$ticket->checkStaffPerm($thisstaff)) die('Access Denied');

//Re-use the post info on error...savekeyboards.org (Why keyboard? -> some people care about objects than users!!)
$info=($_POST && $errors)?Format::input($_POST):array();

//Get the goodies.
$dept  = $ticket->getDept();  //Dept
$role  = $ticket->getRole($thisstaff);
$staff = $ticket->getStaff(); //Assigned or closed by..
$user  = $ticket->getOwner(); //Ticket User (EndUser)
$team  = $ticket->getTeam();  //Assigned team.
$sla   = NULL; // $ticket->getSLA();
$lock  = $ticket->getLock();  //Ticket lock obj
if (!$lock && $cfg->getTicketLockMode() == Lock::MODE_ON_VIEW)
    $lock = $ticket->acquireLock($thisstaff->getId());
$mylock = ($lock && $lock->getStaffId() == $thisstaff->getId()) ? $lock : null;
$id    = $ticket->getId();    //Ticket ID.
$isManager = $dept->isManager($thisstaff); //Check if Agent is Manager
$canRelease = ($isManager || $role->hasPerm(Ticket::PERM_RELEASE)); //Check if Agent can release tickets
$canAnswer = ($isManager || $role->hasPerm(Ticket::PERM_REPLY)); //Check if Agent can mark as answered/unanswered

// Get ec3_ticket_id
$ec3_ticket_id = $ticket->getEC3TicketId();
$ticketObject = ($ec3_ticket_id !== NULL) ? $ticket->fetchTicketFromEC3($ec3_ticket_id, $_COOKIE['accessToken']) : NULL;
// var_dump($ticketObject);
$_subCompany = NULL;
$_subCompanyId = NULL;
$_location = NULL;
$_assetTypeName = NULL;
$_assetName = NULL;
$_subAssetName = NULL;
$_requestFromCode = NULL; // $ticket->getRequestFromCode();
$_requestFromValue = ($ticket->getSource() === 'Visual Inspection') ? 'Staff' : NULL;
$_requestFromName = NULL;
$_requestFromEmail = NULL;
$_requestFromTenant = NULL;
$_requestFromServiceProvider = NULL;
$_requestFromStaff = NULL;
$_serviceRelatedName = NULL;
$_internalDepartment = NULL;
$_externalDepartment = NULL;
$_requestFromPhone = NULL;
$_assetCriticality = NULL;
$_assignedTo = NULL;
$closedDate = NULL;
$ticketType = ($ticket->getHelpTopic() === 'Services Related') ? 'service_related' : 'other';
$ticketType = ($ticket->getHelpTopic() === 'Technical') ? 'technical' : $ticketType;
$ticketImages = NULL;
$codeArray = array("public", "shopper", "visitor");
$userLevelName = NULL; // Need to get the logged user's level.
$pendingSLAExtension = FALSE; // This will determine if we have pending SLA Externsion requests for a particular ticket.

$userInfos = ExternalLogin::fetchUserInfo($_COOKIE['accessToken']);
$userInfo = json_decode($userInfos);
$userInfoResult = ($userInfo) ? $userInfo->result : NULL;
if ($userInfoResult !== NULL && $userInfoResult->userLevel !== NULL)
{
    $userLevelName = $userInfoResult->userLevel->name;
}
$ticketHistory = null;

if ($ticketObject !== NULL)
{
    $result= $ticketObject->result;
    // This will determine if we have pending SLA Externsion requests for a particular ticket.
    $pendingSLAExtension = $result->ticket->awaitingSLAExtensionRequest;
    $ticketHistory = $result->listTicketHistory;
    $ticketAssignee = $result->ticketAssignment->user;
    $_assignedTo = ($ticketAssignee !== NULL && $ticketAssignee !== '') ? $ticketAssignee->otherName.' '.$ticketAssignee->surname : NULL;
    $_assignedTo = ($_assignedTo !== NULL) ? $_assignedTo." (".$ticketAssignee->userLevel->name.")": $_assignedTo;
    
    // Fetch the imageURLs from the object returned by API
    if ($result !== NULL && $result->listUrlsInsEntryImages !== NULL)
    {
        $ticketImages = $result->listUrlsInsEntryImages;
    }

    // Set SLA
    $sla = ($result !== NULL && $result->ticketAssignment !== NULL) ? 'SLA-'.$result->ticketAssignment->slaNumHours : $sla;

    if ($result !== NULL && $result->ticket !== NULL)
    {
        $ticketObj = $result->ticket;
        // Closed Date
        $closedDate = ($ticketObj->closedDate !== NULL) ? $ticketObj->closedDate : $closedDate;

        // Determine if Technical/Service Related or any other type of ticket.
        $ticketType = 'other';
        $ticketType = ($ticketObj->servicesRelated !== NULL) ? 'service_related' : $ticketType;
        $ticketType = ($ticketObj->asset !== NULL) ? 'technical' : $ticketType;

        // User Types
        $_requestFromCode = ($ticketObj->userTypes !== NULL) ? $ticketObj->userTypes->userTypeKey : $_requestFromCode;
        // If the request is from Public, Shopper or Visitor (for which data are stored in ost_ticket table), get data from ost_ticket table
        if (in_array($_requestFromCode, $codeArray))
        {
            $_requestFromName = ($ticketObj->userTypeInfo !== NULL) ? $ticketObj->userTypeInfo->userTypeInfoName : $_requestFromName; // $ticket->getRequestFromName();
            $_requestFromEmail = ($ticketObj->userTypeInfo !== NULL) ? $ticketObj->userTypeInfo->userTypeInfoEmail : $_requestFromEmail; // $ticket->getRequestFromEmailAddressList();
            $_requestFromPhone = ($ticketObj->userTypeInfo !== NULL) ? $ticketObj->userTypeInfo->userTypeInfoPhone : $_requestFromPhone; // $ticket->getRequestFromPhoneList();
        }

        if ($_requestFromCode === 'staff')
        {
            $_requestFromStaff = ($ticketObj->staff !== NULL) ? $ticketObj->staff->surname.' '.$ticketObj->staff->otherName : $_requestFromStaff;
            $_requestFromEmail = ($ticketObj->staff !== NULL) ? $ticketObj->staff->userName : $_requestFromEmail;
            $_requestFromPhone = ($ticketObj->staff !== NULL) ? $ticketObj->staff->phoneNumber : $_requestFromPhone;
        }
        else if ($_requestFromCode === 'tenant')
        {
            $_requestFromTenant = ($ticketObj->ticketTenant !== NULL) ? $ticketObj->ticketTenant->tenantName : $_requestFromTenant;
            $_requestFromEmail = ($ticketObj->ticketTenant !== NULL) ? $ticketObj->ticketTenant->tenantEmailAddress : $_requestFromEmail;
            $_requestFromPhone = ($ticketObj->ticketTenant !== NULL) ? $ticketObj->ticketTenant->tenantPhone : $_requestFromPhone;
        }
        else if ($_requestFromCode === 'service_provider')
        {
            $_requestFromServiceProvider = ($ticketObj->ticketServiceProvider !== NULL) ? $ticketObj->ticketServiceProvider->serviceProviderName : $_requestFromTenant;
            $_requestFromEmail = ($ticketObj->ticketServiceProvider !== NULL) ? $ticketObj->ticketServiceProvider->serviceProviderEmailAddress : $_requestFromEmail;
            $_requestFromPhone = ($ticketObj->ticketServiceProvider !== NULL) ? $ticketObj->ticketServiceProvider->serviceProviderFixedPhone : $_requestFromPhone;
        }

        // START: Get Ticket Details
        // Location
        if ($ticketObj->location !== NULL)
        {
            $_location = $ticketObj->location->zone->name.' / '.$ticketObj->location->locationName;
        }
        // Sub Company
        if ($ticketObj->subCompany !== NULL)
        {
            $_subCompany = $ticketObj->subCompany->name;
            $_subCompanyId = $ticketObj->subCompany->subCompanyId;
        }
        // Staff Name
        if ($ticket->getSource() === 'Visual Inspection')
        {
            if ($ticketObj->user !== NULL)
            {
                $_requestFromStaff = $ticketObj->user->otherName.' '.$ticketObj->user->surname;
                $_requestFromEmail = $ticketObj->user->userName;
                $_requestFromPhone = $ticketObj->user->phoneNumber;
            }
        }
        else 
        {
            if ($ticketObj->user !== NULL)
            {
                if ($ticketObj->user->userLevel !== NULL)
                {
                    if ($ticketObj->user->userLevel->name !== 'L0')
                    {
                        //$_requestFromStaff = $ticketObj->user->otherName.' '.$ticketObj->user->surname;
                        //$_requestFromEmail = $ticketObj->user->userName;
                        //$_requestFromPhone = $ticketObj->user->phoneNumber;
                    }
                    else
                    {
                        // TODO: Need to make sure that User Type is saved on EC3 and then the data returned by Willy's api.
                        // Otherwise,we will have to make additional api request to get the Staff/Service Provider/Tenants from EC3.
                        // And, we will have to query osticket to get Public/Shopper/Visitor.
                        //$_requestFromStaff = $ticketObj->user->otherName.' '.$ticketObj->user->surname;
                        //$_requestFromEmail = $ticketObj->user->userName;
                        //$_requestFromPhone = $ticketObj->user->phoneNumber;
                    }
                }
            }
        }
        // END: Get Ticket Details   

        if ($ticketType === 'other')
        {
            
        }

        if ($ticketType === 'service_related')
        {
            $_location = $ticketObj->zone->name;
            $_requestFromValue = $ticket->getRequestFromValue();
            if ($ticketObj->servicesRelated !== NULL)
            {
                $_serviceRelatedName = $ticketObj->servicesRelated->serviceRelatedName;
                $_internalDepartment = $ticketObj->servicesRelated->ticketInternalDepartment->internalDepartmentName;
                $_externalDepartment = $ticketObj->servicesRelated->ticketExternalService->externalServiceName;
            }
        }

        if ($ticketType === 'technical')
        {
            $_requestFromValue = $ticket->getRequestFromValue();
            if ($ticketObj->asset !== NULL)
            {
                if ($ticketObj->asset->assetSubType !== NULL)
                {
                    $_assetTypeName = $ticketObj->asset->assetSubType->assetTypeName;
                    $_subAssetName = $ticketObj->asset->assetSubType->assetSubTypeName;
                    $_assetName = $ticketObj->asset->assetName;
                    
                }
                if ($ticketObj->asset->criticality !== NULL)
                {
                    $_assetCriticality = $ticketObj->asset->criticality->name;
                }
            }
        }
                 
    }
}

// Functions defined in upload/include/class.ticket.php
// $_subCompany = ($_subCompany === NULL) ? $ticket->getSubCompany() : $_subCompany;
// $_location = ($_location === NULL)  ? $ticket->getLocationName() : $_location;
// $_assetTypeName = $ticket->getAssetType();
// $_assetName = $ticket->getAssetName();
// $_subAssetName = $ticket->getSubAssetType();
// $_requestFromCode = $ticket->getRequestFromCode();
// $_requestFromValue = $ticket->getRequestFromValue();



// $_requestFromEmail = $ticket->getRequestFromEmailAddressList();
// $_requestFromTenant = $ticket->getRequestFromTenant();
// $_requestFromServiceProvider = $ticket->getRequestFromServiceProvider();
// $_requestFromStaff = $ticket->getRequestFromStaff();

//Useful warnings and errors the user might want to know!
if ($ticket->isClosed() && !$ticket->isReopenable())
    $warn = sprintf(
            __('Current ticket status (%s) does not allow the end user to reply.'),
            $ticket->getStatus());
elseif ($ticket->isAssigned()
        && (($staff && $staff->getId()!=$thisstaff->getId())
            || ($team && !$team->hasMember($thisstaff))
        ))
    $warn.= sprintf('&nbsp;&nbsp;<span class="Icon assignedTicket">%s</span>',
            sprintf(__('Ticket is assigned to %s'),
                implode('/', $ticket->getAssignees())
                ));

if (!$errors['err']) {

    if ($lock && $lock->getStaffId()!=$thisstaff->getId())
        $errors['err'] = sprintf(__('%s is currently locked by %s'),
                __('This ticket'),
                $lock->getStaffName());
    elseif (($emailBanned=Banlist::isBanned($ticket->getEmail())))
        $errors['err'] = __('Email is in banlist! Must be removed before any reply/response');
    elseif (!Validator::is_valid_email($ticket->getEmail()))
        $errors['err'] = __('EndUser email address is not valid! Consider updating it before responding');
}

$unbannable=($emailBanned) ? BanList::includes($ticket->getEmail()) : false;

if($ticket->isOverdue())
    $warn.='&nbsp;&nbsp;<span class="Icon overdueTicket">'.__('Marked overdue!').'</span>';
?>

<!-- Convert combo -->
<link type="text/css" rel="stylesheet" href="/helpdesk-dev/upload/css/select2.min.css?7b1eee8">

<?php if ($userLevelName === 'L2' && $pendingSLAExtension === false && $ticket->getStatus()->display() !== 'Closed') { ?>
    <!--  START: Modal To request for SLA Extension -->
    <style>
        /* The Modal (background) */
    .modal {
    z-index: 1000; /* Sit on top */
    }

        /* The Close Button */
        .close2 {
            position: absolute;
            top: 0px;
            right: 0px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }

        .close2:hover,
        .close2:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }
    </style>
    <!-- The Modal -->
    <div id="myModal2" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
        <span class="close2">&times;</span>
        <h3>External Intervention Request</h3>
        </div>
        <form action="../include/staff/extendslarequest.php?id=<?php echo $ticket->getId(); ?>&accessToken=<?php echo $_COOKIE['accessToken']; ?>" method="POST">
            <input type="hidden" name="userId" value="<?php echo ($userInfoResult !== NULL && $userInfoResult !== '') ? $userInfoResult->userId : 0; ?>" />
            <input type="hidden" name="ec3_ticket_id" value="<?php echo $ec3_ticket_id; ?>" />
            <input type="hidden" name="accesstoken" value="<?php echo $_COOKIE['accessToken']; ?>" />
            <div class="modal-body">
                <span><small><i>All fields marked with an asterisk are mandatory.</i></small></span>
                <table>
                    <tr>
                        <td style="padding-top: 20px;">External Service Provider <span style="color:red;">*</span>:</td>
                        <td style="padding-top: 20px; width:200px;" id="serviceProviderContainer">
                            <?php 
                                // Get the list of External Service Providers
                                $headers = array(
                                    'Content-Type: application/json',
                                    sprintf('Authorization: Bearer %s', $_COOKIE['accessToken'])
                                );
                                // Passing the sub_company_id to which the ticket is associated
                                $curl = curl_init(EC3_CURL_URL.':'.EC3_CURL_URL_PORT.'/api/osticket/ticketServiceProvidersBySubCompany?subCompanyId='.$_subCompanyId);
                                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                                $spResult = json_decode(curl_exec($curl));
                                $serviceProviders = ($spResult) ? $spResult->result : array();
                            ?>
                            
                                <?php 
                                    if (count($serviceProviders) > 0) {
                                        echo "<select name='serviceProvidersId' id='serviceProviders' required>";
                                            foreach ($serviceProviders as $row) {
                                                echo "<option value='".$row->ticketServiceProviderId."' data-email='".$row->serviceProviderEmailAddress."' data-phone='".$row->serviceProviderFixedPhone."'>".$row->serviceProviderName."</option>";
                                            }
                                        echo "</select>";
                                    } 
                                    else
                                    {
                                        echo "<b>No service providers found.</b>";
                                    }
                                ?>
                            
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="padding-top: 10px;">Additional Hours Requested <span style="color:red;">*</span>:</td>
                        <td style="padding-top: 10px;">
                        <input required type="number" id="extentionHour" name="extentionHour" min="1" max="24">
                        </td>
                    </tr>
                </table>
            
            </div>
            <div class="modal-footer">
                <button type="submit" name="saveExternalIntervention">Submit Request</button>
            </div>
        </form>
    </div>
    </div>
    <!--  END: Modal To request for SLA Extension -->
<?php } ?>

<div>
    <div class="sticky bar">
       <div class="content">
        <?php 
            if ($action !== NULL)
            {
                if ($action === 'slaextension')
                {
                    if ($code !== NULL)
                    {
                        if ($code === 0)
                            echo '<div class="success-banner">Successfully submitted SLA extension request.</div>';
                        else
                            echo '<div class="error-banner">Error occured on submitting SLA extension request.</div>';
                    }
                }
            }
        ?>

        <div class="pull-right flush-right">
            <?php
            if ($thisstaff->hasPerm(Email::PERM_BANLIST)
                    || $role->hasPerm(Ticket::PERM_EDIT)
                    || ($dept && $dept->isManager($thisstaff))) { ?>
            <span style="display:none;" class="action-button pull-right" data-placement="bottom" data-dropdown="#action-dropdown-more" data-toggle="tooltip" title="<?php echo __('More');?>">
                <i class="icon-caret-down pull-right"></i>
                <span ><i class="icon-cog"></i></span>
            </span>
            <?php
            }

            if ($role->hasPerm(Ticket::PERM_EDIT)) { ?>
                <span class="action-button pull-right hidden"><a data-placement="bottom" data-toggle="tooltip" title="<?php echo __('Edit'); ?>" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=edit"><i class="icon-edit"></i></a></span>
            <?php
            } ?>
            <span class="action-button pull-right" data-placement="bottom" data-dropdown="#action-dropdown-print" data-toggle="tooltip" title="<?php echo __('Print'); ?>">
                <i class="icon-caret-down pull-right"></i>
                <a id="ticket-print" aria-label="<?php echo __('Print'); ?>" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print"><i class="icon-print"></i></a>
            </span>
            <div id="action-dropdown-print" class="action-dropdown anchor-right">
              <ul>
                 <li><a class="no-pjax" target="_blank" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print&notes=0"><i
                 class="icon-file-alt"></i> <?php echo __('Ticket Thread'); ?></a>
                 <li><a class="no-pjax" target="_blank" href="tickets.php?id=<?php echo $ticket->getId(); ?>&a=print&notes=1"><i
                 class="icon-file-text-alt"></i> <?php echo __('Thread + Internal Notes'); ?></a>
              </ul>
            </div>
            <?php
            // Transfer
            if ($role->hasPerm(Ticket::PERM_TRANSFER)) {?>
            <span class="action-button pull-right" style="display:none;">
            <a class="ticket-action" id="ticket-transfer" data-placement="bottom" data-toggle="tooltip" title="<?php echo __('Transfer'); ?>"
                data-redirect="tickets.php"
                href="#tickets/<?php echo $ticket->getId(); ?>/transfer"><i class="icon-share"></i></a>
            </span>
            <?php
            } ?>

            <?php
            // Assign
            if ($ticket->isOpen() && $role->hasPerm(Ticket::PERM_ASSIGN)) {?>
            <span style="display:none;" class="action-button pull-right"
                data-dropdown="#action-dropdown-assign"
                data-placement="bottom"
                data-toggle="tooltip"
                title=" <?php echo $ticket->isAssigned() ? __('Assign') : __('Reassign'); ?>"
                >
                <i class="icon-caret-down pull-right"></i>
                <a class="ticket-action" id="ticket-assign"
                    data-redirect="tickets.php"
                    href="#tickets/<?php echo $ticket->getId(); ?>/assign"><i class="icon-user"></i></a>
            </span>
            <div id="action-dropdown-assign" class="action-dropdown anchor-right">
              <ul>
                <?php
                // Agent can claim team assigned ticket
                if (!$ticket->getStaff()
                        && (!$dept->assignMembersOnly()
                            || $dept->isMember($thisstaff))
                        ) { ?>
                 <li><a class="no-pjax ticket-action"
                    data-redirect="tickets.php?id=<?php echo
                    $ticket->getId(); ?>"
                    href="#tickets/<?php echo $ticket->getId(); ?>/claim"><i
                    class="icon-chevron-sign-down"></i> <?php echo __('Claim'); ?></a>
                <?php
                } ?>
                 <li><a class="no-pjax ticket-action"
                    data-redirect="tickets.php"
                    href="#tickets/<?php echo $ticket->getId(); ?>/assign/agents"><i
                    class="icon-user"></i> <?php echo __('Agent'); ?></a>
                 <li><a class="no-pjax ticket-action"
                    data-redirect="tickets.php"
                    href="#tickets/<?php echo $ticket->getId(); ?>/assign/teams"><i
                    class="icon-group"></i> <?php echo __('Team'); ?></a>
              </ul>
            </div>
            <?php
            } ?>
            <div id="action-dropdown-more" class="action-dropdown anchor-right">
              <ul>
                <?php
                 if ($role->hasPerm(Ticket::PERM_EDIT)) { ?>
                    <li><a class="change-user" href="#tickets/<?php
                    echo $ticket->getId(); ?>/change-user"><i class="icon-user"></i> <?php
                    echo __('Change Owner'); ?></a></li>
                <?php
                 }

                 if ($ticket->isAssigned() && $canRelease) { ?>
                        <li><a href="#tickets/<?php echo $ticket->getId();
                            ?>/release" class="ticket-action"
                             data-redirect="tickets.php?id=<?php echo $ticket->getId(); ?>" >
                               <i class="icon-unlock"></i> <?php echo __('Release (unassign) Ticket'); ?></a></li>
                 <?php
                 }
                 if($ticket->isOpen() && $isManager) {
                    if(!$ticket->isOverdue()) { ?>
                        <li><a class="confirm-action" id="ticket-overdue" href="#overdue"><i class="icon-bell"></i> <?php
                            echo __('Mark as Overdue'); ?></a></li>
                    <?php
                    }
                 }
                 if($ticket->isOpen() && $canAnswer) {
                    if($ticket->isAnswered()) { ?>
                    <li><a href="#tickets/<?php echo $ticket->getId();
                        ?>/mark/unanswered" class="ticket-action"
                            data-redirect="tickets.php?id=<?php echo $ticket->getId(); ?>">
                            <i class="icon-circle-arrow-left"></i> <?php
                            echo __('Mark as Unanswered'); ?></a></li>
                    <?php
                    } else { ?>
                    <li><a href="#tickets/<?php echo $ticket->getId();
                        ?>/mark/answered" class="ticket-action"
                            data-redirect="tickets.php?id=<?php echo $ticket->getId(); ?>">
                            <i class="icon-circle-arrow-right"></i> <?php
                            echo __('Mark as Answered'); ?></a></li>
                    <?php
                    }
                } ?>

                <?php
                if ($role->hasPerm(Ticket::PERM_REFER)) { ?>
                <li><a href="#tickets/<?php echo $ticket->getId();
                    ?>/referrals" class="ticket-action"
                     data-redirect="tickets.php?id=<?php echo $ticket->getId(); ?>" >
                       <i class="icon-exchange"></i> <?php echo __('Manage Referrals'); ?></a></li>
                <?php
                } ?>
                <?php
                if ($role->hasPerm(Ticket::PERM_EDIT)) { ?>
                <li><a href="#ajax.php/tickets/<?php echo $ticket->getId();
                    ?>/forms/manage" onclick="javascript:
                    $.dialog($(this).attr('href').substr(1), 201);
                    return false"
                    ><i class="icon-paste"></i> <?php echo __('Manage Forms'); ?></a></li>
                <?php
                }

                if ($role->hasPerm(Ticket::PERM_REPLY)) {
                    ?>
                <li>

                    <?php
                    $recipients = __(' Manage Collaborators');

                    echo sprintf('<a class="collaborators manage-collaborators"
                            href="#thread/%d/collaborators"><i class="icon-group"></i>%s</a>',
                            $ticket->getThreadId(),
                            $recipients);
                   ?>
                </li>
                <?php
                } ?>


<?php           if ($thisstaff->hasPerm(Email::PERM_BANLIST)
                    && $role->hasPerm(Ticket::PERM_REPLY)) {
                     if(!$emailBanned) {?>
                        <li><a class="confirm-action" id="ticket-banemail"
                            href="#banemail"><i class="icon-ban-circle"></i> <?php echo sprintf(
                                Format::htmlchars(__('Ban Email <%s>')),
                                $ticket->getEmail()); ?></a></li>
                <?php
                     } elseif($unbannable) { ?>
                        <li><a  class="confirm-action" id="ticket-banemail"
                            href="#unbanemail"><i class="icon-undo"></i> <?php echo sprintf(
                                Format::htmlchars(__('Unban Email <%s>')),
                                $ticket->getEmail()); ?></a></li>
                    <?php
                     }
                  }
                  if ($role->hasPerm(Ticket::PERM_DELETE)) {
                     ?>
                    <li class="danger"><a class="ticket-action" href="#tickets/<?php
                    echo $ticket->getId(); ?>/status/delete"
                    data-redirect="tickets.php"><i class="icon-trash"></i> <?php
                    echo __('Delete Ticket'); ?></a></li>
                <?php
                 }
                ?>
              </ul>
            </div>
                <?php
                if ($role->hasPerm(Ticket::PERM_REPLY)) { ?>
                <a href="#post-reply" class="post-response action-button"
                data-placement="bottom" data-toggle="tooltip"
                title="<?php echo __('Post Reply'); ?>"><i class="icon-mail-reply"></i></a>
                <?php
                } ?>
                <a href="#post-note" id="post-note" class="post-response action-button"
                data-placement="bottom" data-toggle="tooltip"
                title="<?php echo __('Post Internal Note'); ?>"><i class="icon-file-text"></i></a>
                <?php // Status change options
                echo TicketStatus::status_options();
                ?>
           </div>
        <div class="flush-left">
             <h2><a href="tickets.php?id=<?php echo $ticket->getId(); ?>&accessToken=<?php echo $_COOKIE['accessToken']; ?>" 
             title="<?php echo __('Reload'); ?>"><i class="icon-refresh"></i>
             <?php echo sprintf(__('Ticket #%s'), $ticket->getNumber()); ?></a>
            </h2>
        </div>
    </div>
  </div>
</div>
<div class="clear tixTitle has_bottom_border">
    <h3>
    <?php $subject_field = TicketForm::getInstance()->getField('subject');
        echo $subject_field->display($ticket->getSubject()); ?>
    </h3>
</div>
<table class="ticket_info" cellspacing="0" cellpadding="0" width="940" border="0">
    <tr>
        <td width="50%">
            <table border="0" cellspacing="" cellpadding="4" width="100%">
                <tr>
                    <th width="100"><?php echo __('Status');?>:</th>
                    <?php
                        // MKO
                        $labelClassStatus = (strtolower($ticket->getStatus()) === 'open') ? "danger" : "success";
                        if ($role->hasPerm(Ticket::PERM_CLOSE)) { ?>
                    <td>
                        <?php
                            $ticketStatus = $ticket->getStatus();
                            $dataDropdown = "#action-dropdown-statuses";

                            if ( strtolower(trim($ticketStatus)) === 'closed')
                            {
                                /*
                                *   If status is close, we need to check if closedDate has exceeded 3 months. If yes, then cannot reopen ticket.
                                */
                                // $closedDate
                                if ($closedDate !== NULL)
                                {
                                    $reopenDateLimit = date('Y-m-d H:i:s', strtotime("+3 months", strtotime(str_replace('/', '-', $closedDate))));
                                    $currentDate = date('Y-m-d H:i:s');
                                    if (strtotime($currentDate) >= strtotime($reopenDateLimit))
                                        $dataDropdown = '';
                                }
                                else
                                {
                                    $dataDropdown = '';
                                }
                            }
                        ?>
                        <span class="bbb labels labels-<?php echo $labelClassStatus; ?>">
                            <a class="tickets-action" data-dropdown="<?php echo $dataDropdown; ?>" data-placement="<?php echo ($dataDropdown !== '') ? 'bottom': ''; ?>" data-toggle="<?php echo ($dataDropdown !== '') ? 'tooltip' : ''; ?>" title="<?php echo ($dataDropdown !== '') ?  __('Change Status') : ''; ?>"
                            data-redirect="tickets.php?id=<?php echo $ticket->getId(); ?>"
                            href="#statuses">
                                <?php 
                                    echo $ticket->getStatus(); 
                                ?>
                            </a>
                        </span>
                    </td>
                      <?php } else { ?>
                          <td><span class="aaaa labels labels-<?php echo $labelClassStatus; ?>"><?php echo ($S = $ticket->getStatus()) ? $S->display() : ''; ?></span></td>
                      <?php } ?>
                </tr>
                <tr>
                    <th><?php echo __('Priority');?>:</th>
                      <?php
                         if ($role->hasPerm(Ticket::PERM_EDIT)) {?>
                           <td>
                                <!-- <a class="ticket-action" id="inline-update" data-placement="bottom" data-toggle="tooltip" title="<?php // echo __('Update'); ?>"
                                data-redirect="tickets.php?id=<?php // echo $ticket->getId(); ?>"
                                href="#tickets/<?php // echo $ticket->getId(); ?>/field/priority/edit"> -->
                                    <?php echo $ticket->getPriority(); ?>
                                <!-- </a> -->
                           </td>
                      <?php } else { ?>
                           <td><?php echo $ticket->getPriority(); ?></td>
                      <?php } ?>
                </tr>
                <tr>
                    <th><?php echo __('Department');?>:</th>
                    <?php
                    if ($role->hasPerm(Ticket::PERM_TRANSFER)) {?>
                        <td>
                            <!-- <a class="ticket-action" id="ticket-transfer" data-placement="bottom" data-toggle="tooltip" title="<?php // echo __('Transfer'); ?>"
                            data-redirect="tickets.php?id=<?php // echo $ticket->getId(); ?>"
                            href="#tickets/<?php // echo $ticket->getId(); ?>/transfer"> -->
                                <?php echo Format::htmlchars($ticket->getDeptName()); ?>
                            <!-- </a> -->
                        </td>
                    <?php
                  }else {?>
                    <td><?php echo Format::htmlchars($ticket->getDeptName()); ?></td>
                  <?php } ?>
                </tr>
                <tr>
                    <th><?php echo __('Create Date');?>:</th>
                    <td><?php $date = str_replace('/', '-', $ticket->getCreateDate());
                    $newDate = date("d-m-Y h:i:s A", strtotime($date)); echo $newDate;
                    ?></td>
                </tr>

            </table>
        </td>
        <td width="50%" style="vertical-align:top">
            <table border="0" cellspacing="" cellpadding="4" width="100%">
                <tr>
                    <th width="100"><?php echo __('Created by'); ?>:</th>
                    <td>
                        <!-- <a href="#tickets/<?php // echo $ticket->getId(); ?>/user"
                        onclick="javascript:
                            $.userLookup('ajax.php/tickets/<?php // echo $ticket->getId(); ?>/user',
                                    function (user) {
                                        $('#user-'+user.id+'-name').text(user.name);
                                        $('#user-'+user.id+'-email').text(user.email);
                                        $('#user-'+user.id+'-phone').text(user.phone);
                                        $('select#emailreply option[value=1]').text(user.name+' <'+user.email+'>');
                                    });
                            return false;
                            "> --> 
                            <i class="icon-user"></i> 
                            <span id="user-<?php echo $ticket->getOwnerId(); ?>-name">
                                <?php echo Format::htmlchars($ticket->getName()); ?>
                            </span>
                        <!-- </a> -->
                        <?php
                        $displayOtherCreatedByInfo = false;
                        if ($user && $displayOtherCreatedByInfo) { ?>
                            <a href="tickets.php?<?php echo Http::build_query(array(
                                'status'=>'open', 'a'=>'search', 'uid'=> $user->getId()
                            )); ?>" title="<?php echo __('Related Tickets'); ?>"
                            data-dropdown="#action-dropdown-stats">
                            (<b><?php echo $user->getNumTickets(); ?></b>)
                            </a>
                            <div id="action-dropdown-stats" class="action-dropdown anchor-right">
                                <ul>
                                    <?php
                                    if(($open=$user->getNumOpenTickets()))
                                        echo sprintf('<li><a href="tickets.php?a=search&status=open&uid=%s"><i class="icon-folder-open-alt icon-fixed-width"></i> %s</a></li>',
                                                $user->getId(), sprintf(_N('%d Open Ticket', '%d Open Tickets', $open), $open));

                                    if(($closed=$user->getNumClosedTickets()))
                                        echo sprintf('<li><a href="tickets.php?a=search&status=closed&uid=%d"><i
                                                class="icon-folder-close-alt icon-fixed-width"></i> %s</a></li>',
                                                $user->getId(), sprintf(_N('%d Closed Ticket', '%d Closed Tickets', $closed), $closed));
                                    ?>
                                    <li><a href="tickets.php?a=search&uid=<?php echo $ticket->getOwnerId(); ?>"><i class="icon-double-angle-right icon-fixed-width"></i> <?php echo __('All Tickets'); ?></a></li>
                                <?php   if ($thisstaff->hasPerm(User::PERM_DIRECTORY)) { ?>
                                    <li><a href="users.php?id=<?php echo
                                    $user->getId(); ?>"><i class="icon-user
                                    icon-fixed-width"></i> <?php echo __('Manage User'); ?></a></li>
                                <?php   } ?>
                                </ul>
                            </div>
                            <?php
                            if ($role->hasPerm(Ticket::PERM_EDIT)) {
                            $numCollaborators = $ticket->getThread()->getNumCollaborators();
                             if ($ticket->getThread()->getNumCollaborators())
                                $recipients = sprintf(__('%d'),
                                        $numCollaborators);
                            else
                              $recipients = 0;

                             echo sprintf('<span><a class="manage-collaborators preview"
                                    href="#thread/%d/collaborators"><span id="t%d-recipients"><i class="icon-group"></i> (%s)</span></a></span>',
                                    $ticket->getThreadId(),
                                    $ticket->getThreadId(),
                                    $recipients);
                             }?>
<?php                   } # end if ($user) ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __('Email'); ?>:</th>
                    <td>
                        <span id="user-<?php echo $ticket->getOwnerId(); ?>-email"><?php echo $ticket->getEmail(); ?></span>
                    </td>
                </tr>
<?php   if ($user->getOrganization()) { ?>
                <tr style="display:none;">
                    <th><?php echo __('Organization'); ?>:</th>
                    <td><i class="icon-building"></i>
                    <?php echo Format::htmlchars($user->getOrganization()->getName()); ?>
                        <a href="tickets.php?<?php echo Http::build_query(array(
                            'status'=>'open', 'a'=>'search', 'orgid'=> $user->getOrgId()
                        )); ?>" title="<?php echo __('Related Tickets'); ?>"
                        data-dropdown="#action-dropdown-org-stats">
                        (<b><?php echo $user->getNumOrganizationTickets(); ?></b>)
                        </a>
                            <div id="action-dropdown-org-stats" class="action-dropdown anchor-right">
                                <ul>
<?php   if ($open = $user->getNumOpenOrganizationTickets()) { ?>
                                    <li><a href="tickets.php?<?php echo Http::build_query(array(
                                        'a' => 'search', 'status' => 'open', 'orgid' => $user->getOrgId()
                                    )); ?>"><i class="icon-folder-open-alt icon-fixed-width"></i>
                                    <?php echo sprintf(_N('%d Open Ticket', '%d Open Tickets', $open), $open); ?>
                                    </a></li>
<?php   }
        if ($closed = $user->getNumClosedOrganizationTickets()) { ?>
                                    <li><a href="tickets.php?<?php echo Http::build_query(array(
                                        'a' => 'search', 'status' => 'closed', 'orgid' => $user->getOrgId()
                                    )); ?>"><i class="icon-folder-close-alt icon-fixed-width"></i>
                                    <?php echo sprintf(_N('%d Closed Ticket', '%d Closed Tickets', $closed), $closed); ?>
                                    </a></li>
                                    <li><a href="tickets.php?<?php echo Http::build_query(array(
                                        'a' => 'search', 'orgid' => $user->getOrgId()
                                    )); ?>"><i class="icon-double-angle-right icon-fixed-width"></i> <?php echo __('All Tickets'); ?></a></li>
<?php   }
        if ($thisstaff->hasPerm(User::PERM_DIRECTORY)) { ?>
                                    <li><a href="orgs.php?id=<?php echo $user->getOrgId(); ?>"><i
                                        class="icon-building icon-fixed-width"></i> <?php
                                        echo __('Manage Organization'); ?></a></li>
<?php   } ?>
                                </ul>
                            </div>
                        </td>
                    </tr>
<?php   } # end if (user->org) ?>
                <tr>
                  <th><?php echo __('Source'); ?>:</th>
                  <td>
                    <?php if ($role->hasPerm(Ticket::PERM_EDIT)) { ?>
                        <!-- <a class="ticket-action" id="inline-update" data-placement="bottom" data-toggle="tooltip" title="<?php // echo __('Update'); ?>"
                        data-redirect="tickets.php?id=<?php // echo $ticket->getId(); ?>" 
                        href="#tickets/<?php // echo $ticket->getId(); ?>/field/source/edit"> -->
                            <?php echo Format::htmlchars($ticket->getSource()); ?>
                        <!-- </a> -->
                    <?php
                    } else {
                        echo Format::htmlchars($ticket->getSource());
                    }

                    if (!strcasecmp($ticket->getSource(), 'Web') && $ticket->getIP())
                        echo '&nbsp;&nbsp; <span class="faded">('.Format::htmlchars($ticket->getIP()).')</span>';
                    ?>
                 </td>
                </tr>
    
            </table>
        </td>
    </tr>
</table>
<br>
<table class="ticket_info" cellspacing="0" cellpadding="0" width="940" border="0">
    <tr>
        <td width="50%">
            <table cellspacing="0" cellpadding="4" width="100%" border="0">
                <?php
                if($ticket->isOpen()) { ?>
                <tr>
                    <th width="100"><?php echo __('Assigned To');?>:</th>
                    
                    <td> 
                       <?php 
                            if($_assignedTo !== NULL)
                            {
                                 echo Format::htmlchars($_assignedTo);
                            }
                            else
                            {
                                echo '<span class="faded">&mdash; '.__('Unassigned').' &mdash;</span>';
                            }
                        ?>
                    </td>
                </tr>
                <?php
                } else { ?>
                <tr>
                    <th width="100"><?php echo __('Closed By');?>:</th>
                    <td>
                        <?php
                        if(($staff = $ticket->getStaff()))
                            echo Format::htmlchars($staff->getName());
                        else
                            echo '<span class="faded">&mdash; '.__('Unknown').' &mdash;</span>';
                        ?>
                    </td>
                </tr>
                <?php
                } ?>
                <tr>
                    <th style="vertical-align:top;"><?php echo __('SLA Plan');?>:</th>
                    <td>
                        <?php 
                            $labelClass = "labels labels-success";
                            if ($pendingSLAExtension === true) 
                            { 
                                $labelClass = "labels labels-warning";
                            }

                            // if ($role->hasPerm(Ticket::PERM_EDIT)) { 
                                if ($sla !== NULL) {
                        ?>
                            <!-- <a class="ticket-action ticket-action-sla" id="inline-update" data-placement="bottom" data-toggle="tooltip" title="<?php // echo __('Update'); ?>"
                            data-redirect="tickets.php?id=<?php // echo $ticket->getId(); ?>" style="display:none;" href="#tickets/<?php // echo $ticket->getId(); ?>/field/sla/edit"> -->
                                <?php // echo $sla?Format::htmlchars($sla->getName()):'<span class="faded">&mdash; '.__('None').' &mdash;</span>'; ?>
                            <!-- </a> -->
                                    <span class="<?php echo $labelClass; ?> "><?php echo Format::htmlchars($sla); ?></span>
                                <?php } else { ?>
                                    <span class="faded"><?php echo __('None'); ?></span>
                                <?php } ?>
                            <!-- <span class="<?php // echo $labelClass; ?> "><?php // echo $sla?Format::htmlchars($sla->getName()):'<span class="faded">&mdash; '.__('None').' &mdash;</span>'; ?></span> -->

                            <?php if ($userLevelName === 'L2' && $pendingSLAExtension === false && $ticket->getStatus()->display() !== 'Closed') { ?>
                                <br/><br/>
                                <button id="myBtn">External Intervention</button>
                                <!-- <label for=""><input type="checkbox" id="chkVendorIntervention" name="chkVendorIntervention" />External Intervention</label> -->
                            <?php } ?>
                        
                        <?php // } else { 
                            
                            // if ($sla) {    
                        ?>
                                <!-- <span class="<?php // echo $labelClass; ?> "><?php // echo Format::htmlchars($sla->getName()); ?></span> -->
                            <?php // } else { ?>
                                <!-- <span class="faded"><?php // echo __('None'); ?></span> -->
                            <?php // } ?>
                        <?php // } ?>
                    </td>
                </tr>
                <?php
                if($ticket->isOpen()){ ?>
                    <tr>
                        <th><?php echo __('Due Date');?>:</th>
                        <?php if ($role->hasPerm(Ticket::PERM_EDIT)) { ?>
                            <td>
                                <!-- <a class="ticket-action" id="inline-update" data-placement="bottom" data-toggle="tooltip" title="<?php // echo __('Update'); ?>"
                                data-redirect="tickets.php?id=<?php // echo $ticket->getId(); ?>" href="#tickets/<?php // echo $ticket->getId(); ?>/field/duedate/edit"> -->
                                <?php 
                                    $date = str_replace('/', '-', $ticket->getEstDueDate());
                                    $newDate = date("d-m-Y h:i:s A", strtotime($date)); 
                                    echo $newDate; 
                                ?>
                                <!-- </a> -->
                            <td>
                        <?php } else { ?>
                            <td>
                                <?php 
                                    $date = str_replace('/', '-', $ticket->getEstDueDate());
                                    $newDate = date("d-m-Y h:i:s A", strtotime($date)); 
                                    echo $newDate;
                                ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <th><?php echo __('Close Date');?>:</th>
                        <td>
                            <?php 
                                if ($closedDate !== NULL && $closedDate !== '')
                                {
                                    $date = str_replace('/', '-', $closedDate);
                                    $newDate = date("d-m-Y h:i:s A", strtotime($date)); 
                                    echo $newDate; 
                                }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </td>
        <td width="50%">
            <table cellspacing="0" cellpadding="4" width="100%" border="0">
                <tr>
                    <th width="100"><?php echo __('Incident Category');?>:</th>
                    <?php if ($role->hasPerm(Ticket::PERM_EDIT)) { ?>
                        <td>
                            <!-- <a class="ticket-action" id="inline-update" data-placement="bottom"
                                data-toggle="tooltip" title="<?php // echo __('Update'); ?>"
                                data-redirect="tickets.php?id=<?php // echo $ticket->getId(); ?>"
                                href="#tickets/<?php // echo $ticket->getId(); ?>/field/topic/edit"> -->
                                <?php echo $ticket->getHelpTopic() ?: __('None'); ?>
                            <!-- </a> -->
                        </td>
                    <?php } else { ?>
                            <td><?php echo Format::htmlchars($ticket->getHelpTopic()); ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <th nowrap><?php echo __('Last Message');?>:</th>
                    <td>
                        <?php 
							$date = str_replace('/', '-', $ticket->getLastMsgDate());
							if ($date !== "" && $date !== NULL)
							{
								$newDate = date("d-m-Y h:i:s A", strtotime($date)); 
								echo $newDate; 
							}
						?>
                    </td>
                </tr>
                <tr>
                    <th nowrap><?php echo __('Last Responses');?>:</th>
                    <td>
                        <?php 
                            $date = str_replace('/', '-', $ticket->getLastRespDate());
							if ($date !== "" && $date !== NULL)
							{
								$newDate = ($date !== '' && $date !== NULL) ? date("d-m-Y h:i:s A", strtotime($date)) : '';
								echo $newDate;
							}
                        ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<?php
foreach (DynamicFormEntry::forTicket($ticket->getId()) as $form) {
    // START: MKO TEST CODE: This is done temporarily to not display the second duplication of Ticket DEtails in View Ticket
    if ($count === 1)
    {
        $count++;
        continue; 
    }
    // END: MKO TEST CODE

    //Find fields to exclude if disabled by help topic
    $disabled = Ticket::getMissingRequiredFields($ticket, true);

    // Skip core fields shown earlier in the ticket view
    // TODO: Rewrite getAnswers() so that one could write
    //       ->getAnswers()->filter(not(array('field__name__in'=>
    //           array('email', ...))));
    $answers = $form->getAnswers()->exclude(Q::any(array(
        'field__flags__hasbit' => DynamicFormField::FLAG_EXT_STORED,
        'field__name__in' => array('subject', 'priority'),
        'field__id__in' => $disabled,
    )));
    $displayed = array();
    foreach($answers as $a) {
        if (!$a->getField()->isVisibleToStaff())
            continue;
        $displayed[] = $a;
    }
    if (count($displayed) == 0)
        continue;
    ?>
    <table class="ticket_info custom-data" cellspacing="0" cellpadding="0" width="940" border="0">
    <thead>
        <th colspan="2"><?php echo Format::htmlchars($form->getTitle()); ?></th>
    </thead>
    <tbody>
    <!-- Start : Mall/Location: Displays for all ticket types -->
    <tr>
        <td width="50%">
            <table>
                <tr>
                    <th width="100">Mall:</th>
                    <td><?php echo $_subCompany; ?></td>
                </tr>
            </table>
        </td>
        <td width="50%">
            <table>
                <tr>
                    <th width="100">
                        <?php if ($ticketType === 'service_related') { ?>
                            Zone:
                        <?php } else { ?>
                            Zone/Location:
                        <?php } ?>
                    </th>
                    <td><?php echo $_location; ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <!-- End : Mall/Location: Displays for all ticket types -->

    <!-- Start : Services Related Name: Displays only for Service Related tickets -->
    <?php if ($ticketType === 'service_related') { ?>
        <tr>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Service Related:</th>
                        <td><?php echo $_serviceRelatedName; ?></td>
                    </tr>
                </table>
            </td>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Internal Department:</th>
                        <td><?php echo $_internalDepartment; ?></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">External Department:</th>
                        <td><?php echo $_externalDepartment; ?></td>
                    </tr>
                </table>
            </td>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Request from:</th>
                        <td><?php echo $_requestFromValue; ?></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <!-- Public / Shopper / Visitor -->
            <?php
                if (in_array($_requestFromCode, $codeArray)):
            ?>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Name:</th>
                        <td><?php echo $_requestFromName; ?></td>
                    </tr>
                </table>
            </td>
            <?php endif; ?>

            <!-- Tenant -->
            <?php if ($_requestFromCode === 'tenant') : ?>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Tenant:</th>
                        <td><?php echo $_requestFromTenant; ?></td>
                    </tr>
                </table>
            </td>
            <?php endif; ?>

            <!-- Service provider -->
            <?php if ($_requestFromCode === 'service_provider') : ?>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Service provider:</th>
                        <td><?php echo $_requestFromServiceProvider; ?></td>
                    </tr>
                </table>
            </td>
            <?php endif; ?>

            <!-- Staff -->
            <?php if ($_requestFromCode === 'staff') : ?>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Staff:</th>
                        <td><?php echo $_requestFromStaff; ?></td>
                    </tr>
                </table>
            </td>
            <?php endif; ?>
			
			<!-- Email -->
			<td width="50%">
                <table>
                    <tr>
                        <th width="100">Email:</th>
                        <td><?php echo $_requestFromEmail; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Phone:</th>
                        <td><?php echo $_requestFromPhone; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    <?php } ?>
    <!-- End : Services Related Name -->

    <!-- Start : Services Related Name: Displays only for Asset tickets (Techical) -->
    <?php if ($ticketType === 'technical') { ?>
        <tr id='assetDetails'>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Asset Type:</th>
                        <td><?php echo $_assetTypeName; ?></td>
                    </tr>
                </table>
            </td>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Sub-Asset type:</th>
                        <td><?php echo $_subAssetName; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id='assetDetails'>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Asset Name:</th>
                        <td><?php echo $_assetName; ?></td>
                    </tr>
                </table>
            </td>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Asset Criticality:</th>
                        <td><?php echo $_assetCriticality; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    <?php } ?>
    <!-- End : Services Related Name: Displays only for Asset tickets (Techical) -->

    <!-- START: For any other Incident Category show these fields -->
    <?php if ($ticketType === 'other') { ?>
        <tr>
            <!-- Public / Shopper / Visitor -->
            <?php
                if (in_array($_requestFromCode, $codeArray)):
            ?>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Name:</th>
                        <td><?php echo $_requestFromName; ?></td>
                    </tr>
                </table>
            </td>
            <?php endif; ?>

            <!-- Tenant -->
            <?php if ($_requestFromCode === 'tenant') : ?>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Tenant:</th>
                        <td><?php echo $_requestFromTenant; ?></td>
                    </tr>
                </table>
            </td>
            <?php endif; ?>

            <!-- Service provider -->
            <?php if ($_requestFromCode === 'service_provider') : ?>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Service provider:</th>
                        <td><?php echo $_requestFromServiceProvider; ?></td>
                    </tr>
                </table>
            </td>
            <?php endif; ?>

            <!-- Staff -->
            <?php if ($_requestFromCode === 'staff') : ?>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Staff:</th>
                        <td><?php echo $_requestFromStaff; ?></td>
                    </tr>
                </table>
            </td>
            <?php endif; ?>
			
			<!-- Email -->
			<td width="50%">
                <table>
                    <tr>
                        <th width="100">Email:</th>
                        <td><?php echo $_requestFromEmail; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="50%">
                <table>
                    <tr>
                        <th width="100">Phone:</th>
                        <td><?php echo $_requestFromPhone; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    <?php } ?>
    <!-- END: For any other Incident Category show these fields -->

<?php
    foreach ($displayed as $a) {
        $id =  $a->getLocal('id');
        $label = $a->getLocal('label');
        $v = $a->display() ?: '<span class="faded">&mdash;' . __('Empty') .  '&mdash; </span>';
        $field = $a->getField();
        $isFile = ($field instanceof FileUploadField);
?>
        <?php if ($label === 'Request From' && $ticketType === 'technical') : ?>
            <tr>
                <td width="50%">
                    <table>
                        <tr>
                            <th width="100">Request from:</th>
                            <td><?php echo $_requestFromValue; ?></td>
                        </tr>
                    </table>
                </td>
               
                <!-- Tenant -->
                <?php if ($_requestFromCode === 'tenant') : ?>
                    <td width="50%">
                        <table>
                            <tr>
                                <th width="100">Tenant:</th>
                                <td><?php echo $_requestFromTenant; ?></td>
                            </tr>
                        </table>
                    </td>
                <?php endif; ?>

                <!-- Service provider -->
                <?php if ($_requestFromCode === 'service_provider') : ?>
                    <td width="50%">
                        <table>
                            <tr>
                                <th width="100">Service provider:</th>
                                <td><?php echo $_requestFromServiceProvider; ?></td>
                            </tr>
                        </table>
                    </td>
                <?php endif; ?>

                <!-- Staff -->
                <?php if ($_requestFromCode === 'staff') : ?>
                    <td width="50%">
                        <table>
                            <tr>
                                <th width="100">Staff:</th>
                                <td><?php echo $_requestFromStaff; ?></td>
                            </tr>
                        </table>
                    </td>
                <?php endif; ?>
            </tr>

            <tr>
                <!-- Email -->
                <td width="50%">
                    <table>
                        <tr>
                            <th width="100">Email:</th>
                            <td><?php echo $_requestFromEmail; ?></td>
                        </tr>
                    </table>
                </td>
                <td width="50%">
                    <table>
                        <tr>
                            <th width="100">Phone:</th>
                            <td><?php echo $_requestFromPhone; ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        <?php endif; ?>

        <?php if ($label !== 'Request From') { ?>
            <?php if ($label === 'Cost associated' && $ticketType === 'technical') { ?>
                <tr>
                    <td width="50%">
                        <table>
                            <tr>
                                <th width="200"><?php echo Format::htmlchars($label); ?>:</th>
                                <td>
                                    <?php if ($role->hasPerm(Ticket::PERM_EDIT)
                                            && $field->isEditableToStaff()) {
                                            $isEmpty = strpos($v, '&mdash;');
                                            if ($isFile && !$isEmpty)
                                                echo $v.'<br>'; ?>
                                    <a class="ticket-action" id="inline-update" data-placement="bottom" data-toggle="tooltip" title="<?php echo __('Update'); ?>"
                                        data-redirect="tickets.php?id=<?php echo $ticket->getId(); ?>"
                                        href="#tickets/<?php echo $ticket->getId(); ?>/field/<?php echo $id; ?>/edit" style="margin-left:-80px;">
                                        <?php
                                            if (is_string($v) && $isFile && !$isEmpty) {
                                            echo "<i class=\"icon-edit\"></i>";
                                            } elseif (strlen($v) > 200) {
                                            echo Format::truncate($v, 200);
                                            echo "<br><i class=\"icon-edit\"></i>";
                                            }
                                            else
                                            echo $v;
                                        ?>
                                    </a>
                                    <?php
                                    } else {
                                        echo $v;
                                    } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
<?php } ?>
        
    </tbody>
    </table>
<?php } ?>

<!-- START : Gallery Image -->

<?php
// Get the list of imageURLs from 
if ($ticketImages !== NULL && count($ticketImages) > 0)
{
?>
    <style>
    body {font-family: Arial, Helvetica, sans-serif;}

    .img-group {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    .img-group:hover {opacity: 0.7;}

    /* The Modal (background) */
    .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1000; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content (image) */
    .modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    }

    /* Caption of Modal Image */
    #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
    }

    /* Add Animation */
    .modal-content, #caption {  
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
    }

    @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
    }

    @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
    }

    /* The Close Button */
    .close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
    }

    .close:hover,
    .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
    }
    </style>

    <?php foreach($ticketImages as $ticketImageURL) { ?>
        <img src="<?php echo $ticketImageURL; ?>" onclick="javascript:enlargeImage(this);" alt="" style="width:50%;max-width:200px">
    <?php } ?>

    <!-- The Modal -->
    <div id="myModal" class="modal">
    <span class="close">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
    </div>
    <br/>
    <br/>
    <br/>
<?php
}
?>
<!-- START : Gallery Image -->



<div class="clear"></div>

<?php
$tcount = $ticket->getThreadEntries($types)->count();
?>
<ul  class="tabs clean threads" id="ticket_tabs" >
    <li class="active"><a id="ticket-thread-tab" href="#ticket_thread" ><?php
        echo sprintf(__('Ticket Thread (%d)'), $tcount); ?></a></li>
    <li><a id="ticket-tasks-tab" href="#tasks"
            data-url="<?php
        echo sprintf('#tickets/%d/tasks?fromTab=1', $ticket->getId()); ?>"><?php
        echo __('Tasks');
        if ($ticket->getNumTasks())
            echo sprintf('&nbsp;(<span id="ticket-tasks-count">%d</span>)', $ticket->getNumTasks());
        ?></a></li>
    <li class=""><a id="ticket-history-tab" href="#ticket_history"><?php
        echo __('ASCE Ticket History'); ?></a></li>
</ul>

<div id="ticket_tabs_container">
<div id="ticket_history" class="tab_content" style="display:none;">
    <?php if ($ticketHistory !== NULL && $ticketHistory !== '' && count($ticketHistory) > 0) { 
    ?>
    <div id="ticket_thread_history" class="tab_content">
        <div id="ticketHistory">

    <?php
        foreach ($ticketHistory as $row) {    
    ?>
            <div id="thread-items">
                <div class="thread-event action">
                    <span class="type-icon">
                        <i class="faded icon-magic"></i>
                    </span>
                    <span class="faded description">
                        <?php echo $row->messages; ?></span>
                </div>
            </div>
            
    <?php } ?>
            </div>
        </div>
    <?php } else {  echo "No Ticket history to display.";  } ?>
</div>

<div id="ticket_thread" class="tab_content">

<?php
    // Render ticket thread
    $ticket->getThread()->render(
            array('M', 'R', 'N'),
            array(
                'html-id'   => 'ticketThread',
                'mode'      => Thread::MODE_STAFF,
                'sort'      => $thisstaff->thread_view_order
                )
            );
?>
<div class="clear"></div>
<?php
if ($errors['err'] && isset($_POST['a'])) {
    // Reflect errors back to the tab.
    $errors[$_POST['a']] = $errors['err'];
} elseif($msg) { ?>
    <div id="msg_notice"><?php echo $msg; ?></div>
<?php
} elseif($warn) { ?>
    <div id="msg_warning"><?php echo $warn; ?></div>
<?php
} ?>

<div class="sticky bar stop actions" id="response_options"
>
    <ul class="tabs" id="response-tabs">
        <?php
        if ($role->hasPerm(Ticket::PERM_REPLY)) { ?>
        <li class="active <?php
            echo isset($errors['reply']) ? 'error' : ''; ?>"><a
            href="#reply" id="post-reply-tab"><?php echo __('Post Reply');?></a></li>
        <?php
        } ?>
        <li><a href="#note" <?php
            echo isset($errors['postnote']) ?  'class="error"' : ''; ?>
            id="post-note-tab"><?php echo __('Post Internal Note');?></a></li>
    </ul>
    <?php
    if ($role->hasPerm(Ticket::PERM_REPLY)) {
        $replyTo = $_POST['reply-to'] ?: 'all';
        $emailReply = ($replyTo != 'none');
        ?>
    <form id="reply" class="tab_content spellcheck exclusive save"
        data-lock-object-id="ticket/<?php echo $ticket->getId(); ?>"
        data-lock-id="<?php echo $mylock ? $mylock->getId() : ''; ?>"
        action="tickets.php?id=<?php
        echo $ticket->getId(); ?>&accessToken=<?php echo $_GET['accessToken']; ?>#reply" name="reply" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="msgId" value="<?php echo $msgId; ?>">
        <input type="hidden" name="a" value="reply">
        <input type="hidden" name="lockCode" value="<?php echo $mylock ? $mylock->getCode() : ''; ?>">
        <table style="width:100%" border="0" cellspacing="0" cellpadding="3">
            <?php
            if ($errors['reply']) {?>
            <tr><td width="120">&nbsp;</td><td class="error"><?php echo $errors['reply']; ?>&nbsp;</td></tr>
            <?php
            }?>
           <tbody id="to_sec">
           <tr>
               <td width="120">
                   <label><strong><?php echo __('From'); ?>:</strong></label>
               </td>
               <td>
                   <select id="from_email_id" name="from_email_id">
                     <?php
                     // Department email (default).
                     echo sprintf('<option value="%s" selected="selected">%s</option>',
                             $dept->getEmail()->getId(),
                             Format::htmlchars($dept->getEmail()->getAddress()));
                     // Optional SMTP addreses user can send email via
                     if (($emails = Email::getAddresses(array('smtp' =>
                                 true), false)) && count($emails)) {
                         echo '<option value=""
                             disabled="disabled">&nbsp;</option>';
                         $emailId = $_POST['from_email_id'] ?: 0;
                         foreach ($emails as $e) {
                             if ($dept->getEmail()->getId() == $e->getId())
                                 continue;
                             echo sprintf('<option value="%s" %s>%s</option>',
                                     $e->getId(),
                                      $e->getId() == $emailId ?
                                      'selected="selected"' : '',
                                      Format::htmlchars($e->getAddress()));
                         }
                     }
                     ?>
                   </select>
               </td>
           </tr>
            </tbody>
            <tbody id="recipients">
             <tr id="user-row">
                <td width="120">
                    <label><strong><?php echo __('Recipients'); ?>:</strong></label>
                </td>
                <td><a href="#tickets/<?php echo $ticket->getId(); ?>/user"
                    onclick="javascript:
                        $.userLookup('ajax.php/tickets/<?php echo $ticket->getId(); ?>/user',
                                function (user) {
                                    window.location = 'tickets.php?id='<?php $ticket->getId(); ?>
                                });
                        return false;
                        "><span ><?php
                            echo Format::htmlchars($ticket->getOwner()->getEmail()->getAddress());
                    ?></span></a>
                </td>
              </tr>
               <tr><td>&nbsp;</td>
                   <td>
                   <div style="margin-bottom:2px;">
                    <?php
                         echo sprintf('<span><a id="show_ccs">
                                 <i id="arrow-icon" class="icon-caret-right"></i>&nbsp;%s </a>
                                 &nbsp;
                                 <a class="manage-collaborators
                                 collaborators preview noclick %s"
                                  href="#thread/%d/collaborators">
                                 (%s)</a></span>',
                                 __('Collaborators'),
                                 $ticket->getNumCollaborators()
                                  ? '' : 'hidden',
                                 $ticket->getThreadId(),
                                 sprintf(__('%s of %s'),
                                         sprintf('<span
                                             class="collabselection__count">%d</span>',
                                             $ticket->getNumActiveCollaborators()),
                                         sprintf('<span
                                             class="collabselection__total">%d</span>',
                                              $ticket->getNumCollaborators())
                                         )
                                     );
                    ?>
                   </div>
                   <div id="ccs" class="hidden">
                     <div>
                        <span style="margin: 10px 5px 1px 0;" class="faded pull-left"><?php echo __('Select or Add New Collaborators'); ?>&nbsp;</span>
                        <?php
                        if ($role->hasPerm(Ticket::PERM_REPLY)) { ?>
                        <span class="action-button pull-left" style="margin: 2px  0 5px 20px;"
                            data-dropdown="#action-dropdown-collaborators"
                            data-placement="bottom"
                            data-toggle="tooltip"
                            title="<?php echo __('Manage Collaborators'); ?>"
                            >
                            <i class="icon-caret-down pull-right"></i>
                            <a class="ticket-action" id="collabs-button"
                                data-redirect="tickets.php?id=<?php echo
                                $ticket->getId(); ?>"
                                href="#thread/<?php echo
                                $ticket->getThreadId(); ?>/collaborators">
                                <i class="icon-group"></i></a>
                         </span>
                         <?php
                        }  ?>
                         <span class="error">&nbsp;&nbsp;<?php echo $errors['ccs']; ?></span>
                        </div>
                        <?php
                        if ($role->hasPerm(Ticket::PERM_REPLY)) { ?>
                        <div id="action-dropdown-collaborators" class="action-dropdown anchor-right">
                          <ul>
                             <li><a class="manage-collaborators"
                                href="#thread/<?php echo
                                $ticket->getThreadId(); ?>/add-collaborator/addcc"><i
                                class="icon-plus"></i> <?php echo __('Add New'); ?></a>
                             <li><a class="manage-collaborators"
                                href="#thread/<?php echo
                                $ticket->getThreadId(); ?>/collaborators"><i
                                class="icon-cog"></i> <?php echo __('Manage Collaborators'); ?></a>
                          </ul>
                        </div>
                        <?php
                        } ?>
                     <div class="clear">
                      <select id="collabselection" name="ccs[]" multiple="multiple"
                          data-placeholder="<?php
                            echo __('Select Active Collaborators'); ?>">
                          <?php
                          $collabs = $ticket->getCollaborators();
                          foreach ($collabs as $c) {
                              echo sprintf('<option value="%s" %s class="%s">%s</option>',
                                      $c->getUserId(),
                                      $c->isActive() ?
                                      'selected="selected"' : '',
                                      $c->isActive() ?
                                      'active' : 'disabled',
                                      $c->getName());
                          }
                          ?>
                      </select>
                     </div>
                 </div>
                 </td>
             </tr>
             <tr>
                <td width="120">
                    <label><?php echo __('Reply To'); ?>:</label>
                </td>
                <td>
                    <?php
                    // Supported Reply Types
                    $replyTypes = array(
                            'all'   =>  __('All Active Recipients'),
                            'user'  =>  sprintf('%s (%s)',
                                __('Ticket Owner'),
                                Format::htmlchars($ticket->getOwner()->getEmail())),
                            'none'  =>  sprintf('&mdash; %s  &mdash;',
                                __('Do Not Email Reply'))
                            );

                    $replyTo = $_POST['reply-to'] ?: 'all';
                    $emailReply = ($replyTo != 'none');
                    ?>
                    <select id="reply-to" name="reply-to">
                        <?php
                        foreach ($replyTypes as $k => $v) {
                            echo sprintf('<option value="%s" %s>%s</option>',
                                    $k,
                                    ($k == $replyTo) ?
                                    'selected="selected"' : '',
                                    $v);
                        }
                        ?>
                    </select>
                    <i class="help-tip icon-question-sign" href="#reply_types"></i>
                </td>
             </tr>
            </tbody>
            <tbody id="resp_sec">
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Response');?>:</strong></label>
                </td>
                <td>
                <?php
                if ($errors['response'])
                    echo sprintf('<div class="error">%s</div>',
                            $errors['response']);

                // if ($cfg->isCannedResponseEnabled()) { ?>
                  <!-- <div>
                    <select id="cannedResp" name="cannedResp">
                        <option value="0" selected="selected"><?php // echo __('Select a canned response');?></option>
                        <option value='original'><?php // echo __('Original Message'); ?></option>
                        <option value='lastmessage'><?php // echo __('Last Message'); ?></option>
                        <?php
                        /*if(($cannedResponses=Canned::responsesByDeptId($ticket->getDeptId()))) {
                            echo '<option value="0" disabled="disabled">
                                ------------- '.__('Premade Replies').' ------------- </option>';
                            foreach($cannedResponses as $id =>$title)
                                echo sprintf('<option value="%d">%s</option>',$id,$title);
                        }*/
                        ?>
                    </select>
                    </div> -->
                <?php //} # endif (canned-resonse-enabled)
                    $signature = '';
                    switch ($thisstaff->getDefaultSignatureType()) {
                    case 'dept':
                        if ($dept && $dept->canAppendSignature())
                           $signature = $dept->getSignature();
                       break;
                    case 'mine':
                        $signature = $thisstaff->getSignature();
                        break;
                    } ?>
                  <div>
                    <input type="hidden" name="draft_id" value=""/>
                    <textarea name="response" id="response" cols="50"
                        data-signature-field="signature" data-dept-id="<?php echo $dept->getId(); ?>"
                        data-signature="<?php
                            echo Format::htmlchars(Format::viewableImages($signature)); ?>"
                        placeholder="<?php echo __(
                        'Start writing your response here. Use canned responses from the drop-down above'
                        ); ?>"
                        rows="9" wrap="soft"
                        class="<?php if ($cfg->isRichTextEnabled()) echo 'richtext';
                            ?> draft draft-delete" <?php
    list($draft, $attrs) = Draft::getDraftAndDataAttrs('ticket.response', $ticket->getId(), $info['response']);
    echo $attrs; ?>><?php echo $_POST ? $info['response'] : $draft;
                    ?></textarea>
                </div>
                <div id="reply_form_attachments" class="attachments">
                <?php
                    print $response_form->getField('attachments')->render();
                ?>
                </div>
                </td>
            </tr>
            <tr>
                <td width="120">
                    <label for="signature" class="left"><?php echo __('Signature');?>:</label>
                </td>
                <td>
                    <?php
                    $info['signature']=$info['signature']?$info['signature']:$thisstaff->getDefaultSignatureType();
                    ?>
                    <label><input type="radio" name="signature" value="none" checked="checked"> <?php echo __('None');?></label>
                    <?php
                    if($thisstaff->getSignature()) {?>
                    <label><input type="radio" name="signature" value="mine"
                        <?php echo ($info['signature']=='mine')?'checked="checked"':''; ?>> <?php echo __('My Signature');?></label>
                    <?php
                    } ?>
                    <?php
                    if($dept && $dept->canAppendSignature()) { ?>
                    <label><input type="radio" name="signature" value="dept"
                        <?php echo ($info['signature']=='dept')?'checked="checked"':''; ?>>
                        <?php echo sprintf(__('Department Signature (%s)'), Format::htmlchars($dept->getName())); ?></label>
                    <?php
                    } ?>
                </td>
            </tr>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Ticket Status');?>:</strong></label>
                </td>
                <td>
                    <?php
                    $outstanding = false;
                    if ($role->hasPerm(Ticket::PERM_CLOSE)
                            && is_string($warning=$ticket->isCloseable())) {
                        $outstanding =  true;
                        echo sprintf('<div class="warning-banner">%s</div>', $warning);
                    } ?>
                    <select name="reply_status_id">
                    <?php
                    $statusId = $info['reply_status_id'] ?: $ticket->getStatusId();
                    $states = array('open');
                    if ($role->hasPerm(Ticket::PERM_CLOSE) && !$outstanding)
                        $states = array_merge($states, array('closed'));

                    foreach (TicketStatusList::getStatuses(
                                array('states' => $states)) as $s) {
                        if (!$s->isEnabled()) continue;
                        $selected = ($statusId == $s->getId());
                        echo sprintf('<option value="%d" %s>%s%s</option>',
                                $s->getId(),
                                $selected
                                 ? 'selected="selected"' : '',
                                __($s->getName()),
                                $selected
                                ? (' ('.__('current').')') : ''
                                );
                    }
                    ?>
                    </select>
                </td>
            </tr>
         </tbody>
        </table>
        <p  style="text-align:center;">
            <input class="save pending" type="submit" value="<?php echo __('Post Reply');?>">
            <input class="" type="reset" value="<?php echo __('Reset');?>">
        </p>
    </form>
    <?php
    } ?>
    <form id="note" class="hidden tab_content spellcheck exclusive save"
        data-lock-object-id="ticket/<?php echo $ticket->getId(); ?>"
        data-lock-id="<?php echo $mylock ? $mylock->getId() : ''; ?>"
        action="tickets.php?id=<?php echo $ticket->getId(); ?>#note"
        name="note" method="post" enctype="multipart/form-data">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="locktime" value="<?php echo $cfg->getLockTime() * 60; ?>">
        <input type="hidden" name="a" value="postnote">
        <input type="hidden" name="lockCode" value="<?php echo $mylock ? $mylock->getCode() : ''; ?>">
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <?php
            if($errors['postnote']) {?>
            <tr>
                <td width="120">&nbsp;</td>
                <td class="error"><?php echo $errors['postnote']; ?></td>
            </tr>
            <?php
            } ?>
            <tr>
                <td width="120" style="vertical-align:top">
                    <label><strong><?php echo __('Internal Note'); ?>:</strong><span class='error'>&nbsp;*</span></label>
                </td>
                <td>
                    <div>
                        <div class="faded" style="padding-left:0.15em"><?php
                        echo __('Note title - summary of the note (optional)'); ?></div>
                        <input type="text" name="title" id="title" size="60" value="<?php echo $info['title']; ?>" >
                        <br/>
                        <span class="error">&nbsp;<?php echo $errors['title']; ?></span>
                    </div>
                    <br/>
                    <div class="error"><?php echo $errors['note']; ?></div>
                    <textarea name="note" id="internal_note" cols="80"
                        placeholder="<?php echo __('Note details'); ?>"
                        rows="9" wrap="soft"
                        class="<?php if ($cfg->isRichTextEnabled()) echo 'richtext';
                            ?> draft draft-delete" <?php
    list($draft, $attrs) = Draft::getDraftAndDataAttrs('ticket.note', $ticket->getId(), $info['note']);
    echo $attrs; ?>><?php echo $_POST ? $info['note'] : $draft;
                        ?></textarea>
                <div class="attachments">
                <?php
                    print $note_form->getField('attachments')->render();
                ?>
                </div>
                </td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td width="120">
                    <label><?php echo __('Ticket Status');?>:</label>
                </td>
                <td>
                    <div class="faded"></div>
                    <select name="note_status_id">
                        <?php
                        $statusId = $info['note_status_id'] ?: $ticket->getStatusId();
                        $states = array('open');
                        if ($ticket->isCloseable() === true
                                && $role->hasPerm(Ticket::PERM_CLOSE))
                            $states = array_merge($states, array('closed'));
                        foreach (TicketStatusList::getStatuses(
                                    array('states' => $states)) as $s) {
                            if (!$s->isEnabled()) continue;
                            $selected = $statusId == $s->getId();
                            echo sprintf('<option value="%d" %s>%s%s</option>',
                                    $s->getId(),
                                    $selected ? 'selected="selected"' : '',
                                    __($s->getName()),
                                    $selected ? (' ('.__('current').')') : ''
                                    );
                        }
                        ?>
                    </select>
                    &nbsp;<span class='error'>*&nbsp;<?php echo $errors['note_status_id']; ?></span>
                </td>
            </tr>
        </table>

       <p style="text-align:center;">
           <input class="save pending" type="submit" value="<?php echo __('Post Note');?>">
           <input class="" type="reset" value="<?php echo __('Reset');?>">
       </p>
   </form>
 </div>
 </div>
</div>
<div style="display:none;" class="dialog" id="print-options">
    <h3><?php echo __('Ticket Print Options');?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <form action="tickets.php?id=<?php echo $ticket->getId(); ?>"
        method="post" id="print-form" name="print-form" target="_blank">
        <?php csrf_token(); ?>
        <input type="hidden" name="a" value="print">
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <fieldset class="notes">
            <label class="fixed-size" for="notes"><?php echo __('Print Notes');?>:</label>
            <label class="inline checkbox">
            <input type="checkbox" id="notes" name="notes" value="1"> <?php echo __('Print <b>Internal</b> Notes/Comments');?>
            </label>
        </fieldset>
        <fieldset>
            <label class="fixed-size" for="psize"><?php echo __('Paper Size');?>:</label>
            <select id="psize" name="psize">
                <option value="">&mdash; <?php echo __('Select Print Paper Size');?> &mdash;</option>
                <?php
                  $psize =$_SESSION['PAPER_SIZE']?$_SESSION['PAPER_SIZE']:$thisstaff->getDefaultPaperSize();
                  foreach(Export::$paper_sizes as $v) {
                      echo sprintf('<option value="%s" %s>%s</option>',
                                $v,($psize==$v)?'selected="selected"':'', __($v));
                  }
                ?>
            </select>
        </fieldset>
        <hr style="margin-top:3em"/>
        <p class="full-width">
            <span class="buttons pull-left">
                <input type="reset" value="<?php echo __('Reset');?>">
                <input type="button" value="<?php echo __('Cancel');?>" class="close">
            </span>
            <span class="buttons pull-right">
                <input type="submit" value="<?php echo __('Print');?>">
            </span>
         </p>
    </form>
    <div class="clear"></div>
</div>
<div style="display:none;" class="dialog" id="confirm-action">
    <h3><?php echo __('Please Confirm');?></h3>
    <a class="close" href=""><i class="icon-remove-circle"></i></a>
    <hr/>
    <p class="confirm-action" style="display:none;" id="claim-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>claim</b> (self assign) %s?'), __('this ticket'));?>
    </p>
    <p class="confirm-action" style="display:none;" id="answered-confirm">
        <?php echo __('Are you sure you want to flag the ticket as <b>answered</b>?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="unanswered-confirm">
        <?php echo __('Are you sure you want to flag the ticket as <b>unanswered</b>?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="overdue-confirm">
        <?php echo __('Are you sure you want to flag the ticket as <font color="red"><b>overdue</b></font>?');?>
    </p>
    <p class="confirm-action" style="display:none;" id="banemail-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>ban</b> %s?'), $ticket->getEmail());?> <br><br>
        <?php echo __('New tickets from the email address will be automatically rejected.');?>
    </p>
    <p class="confirm-action" style="display:none;" id="unbanemail-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>remove</b> %s from ban list?'), $ticket->getEmail()); ?>
    </p>
    <p class="confirm-action" style="display:none;" id="release-confirm">
        <?php echo sprintf(__('Are you sure you want to <b>unassign</b> ticket from <b>%s</b>?'), $ticket->getAssigned()); ?>
    </p>
    <p class="confirm-action" style="display:none;" id="changeuser-confirm">
        <span id="msg_warning" style="display:block;vertical-align:top">
        <?php echo sprintf(Format::htmlchars(__('%s <%s> will longer have access to the ticket')),
            '<b>'.Format::htmlchars($ticket->getName()).'</b>', Format::htmlchars($ticket->getEmail())); ?>
        </span>
        <?php echo sprintf(__('Are you sure you want to <b>change</b> ticket owner to %s?'),
            '<b><span id="newuser">this guy</span></b>'); ?>
    </p>
    <p class="confirm-action" style="display:none;" id="delete-confirm">
        <font color="red"><strong><?php echo sprintf(
            __('Are you sure you want to DELETE %s?'), __('this ticket'));?></strong></font>
        <br><br><?php echo __('Deleted data CANNOT be recovered, including any associated attachments.');?>
    </p>
    <div><?php echo __('Please confirm to continue.');?></div>
    <form action="tickets.php?id=<?php echo $ticket->getId(); ?>" method="post" id="confirm-form" name="confirm-form">
        <?php csrf_token(); ?>
        <input type="hidden" name="id" value="<?php echo $ticket->getId(); ?>">
        <input type="hidden" name="a" value="process">
        <input type="hidden" name="do" id="action" value="">
        <hr style="margin-top:1em"/>
        <p class="full-width">
            <span class="buttons pull-left">
                <input type="button" value="<?php echo __('Cancel');?>" class="close">
            </span>
            <span class="buttons pull-right">
                <input type="submit" value="<?php echo __('OK');?>">
            </span>
         </p>
    </form>
    <div class="clear"></div>
</div>
<script type="text/javascript">
<?php
if ($ticketImages !== NULL && count($ticketImages) > 0)
{
?>
    var span = document.getElementsByClassName("close")[0];
    var modal = document.getElementById("myModal");
    function enlargeImage(imgElem)
    {
        var modalImg = document.getElementById("img01");
        modal.style.display = "block";
        modalImg.src = imgElem.src;
        captionText.innerHTML = ""; 
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() { 
        modal.style.display = "none";
    }  
<?php
}
?>


$(function() {
    $('select#serviceProviders').select2({width: '100%'});
    
    $(document).on('click', 'a.change-user', function(e) {
        e.preventDefault();
        var tid = <?php echo $ticket->getOwnerId(); ?>;
        var cid = <?php echo $ticket->getOwnerId(); ?>;
        var url = 'ajax.php/'+$(this).attr('href').substr(1);
        $.userLookup(url, function(user) {
            if(cid!=user.id
                    && $('.dialog#confirm-action #changeuser-confirm').length) {
                $('#newuser').html(user.name +' &lt;'+user.email+'&gt;');
                $('.dialog#confirm-action #action').val('changeuser');
                $('#confirm-form').append('<input type=hidden name=user_id value='+user.id+' />');
                $('#overlay').show();
                $('.dialog#confirm-action .confirm-action').hide();
                $('.dialog#confirm-action p#changeuser-confirm')
                .show()
                .parent('div').show().trigger('click');
            }
        });
    });

    $(document).on('click', 'a.manage-collaborators', function(e) {
        e.preventDefault();
        var url = 'ajax.php/'+$(this).attr('href').substr(1);
        $.dialog(url, 201, function (xhr) {
           var resp = $.parseJSON(xhr.responseText);
           if (resp.user && !resp.users)
              resp.users.push(resp.user);
            // TODO: Process resp.users
           $('.tip_box').remove();
        }, {
            onshow: function() { $('#user-search').focus(); }
        });
        return false;
     });

    // Post Reply or Note action buttons.
    $('a.post-response').click(function (e) {
        var $r = $('ul.tabs > li > a'+$(this).attr('href')+'-tab');
        if ($r.length) {
            // Make sure ticket thread tab is visiable.
            var $t = $('ul#ticket_tabs > li > a#ticket-thread-tab');
            if ($t.length && !$t.hasClass('active'))
                $t.trigger('click');
            // Make the target response tab active.
            if (!$r.hasClass('active'))
                $r.trigger('click');

            // Scroll to the response section.
            var $stop = $(document).height();
            var $s = $('div#response_options');
            if ($s.length)
                $stop = $s.offset().top-125

            $('html, body').animate({scrollTop: $stop}, 'fast');
        }

        return false;
    });

  $('#show_ccs').click(function() {
    var show = $('#arrow-icon');
    var collabs = $('a#managecollabs');
    $('#ccs').slideToggle('fast', function(){
        if ($(this).is(":hidden")) {
            collabs.hide();
            show.removeClass('icon-caret-down').addClass('icon-caret-right');
        } else {
            collabs.show();
            show.removeClass('icon-caret-right').addClass('icon-caret-down');
        }
    });
    return false;
   });

  $('.collaborators.noclick').click(function() {
    $('#show_ccs').trigger('click');
   });

  $('#collabselection').select2({
    width: '350px',
    allowClear: true,
    sorter: function(data) {
        return data.filter(function (item) {
                return !item.selected;
                });
    },
    templateResult: function(e) {
        var $e = $(
        '<span><i class="icon-user"></i> ' + e.text + '</span>'
        );
        return $e;
    }
   }).on("select2:unselecting", function(e) {
        if (!confirm(__("Are you sure you want to DISABLE the collaborator?")))
            e.preventDefault();
   }).on("select2:selecting", function(e) {
        if (!confirm(__("Are you sure you want to ENABLE the collaborator?")))
             e.preventDefault();
   }).on('change', function(e) {
    var id = e.currentTarget.id;
    var count = $('li.select2-selection__choice').length;
    var total = $('#' + id +' option').length;
    $('.' + id + '__count').html(count);
    $('.' + id + '__total').html(total);
    $('.' + id + '__total').parent().toggle((total));
   }).on('select2:opening select2:closing', function(e) {
    $(this).parent().find('.select2-search__field').prop('disabled', true);
   });
});

$('#chkVendorIntervention').change(function() {
    $('.sla-read-only').toggle();
    $('.ticket-action-sla').toggle();
});


<?php if ( $userLevelName === 'L2'  && $pendingSLAExtension === false && $ticket->getStatus()->display() !== 'Closed') { ?>

// Get the modal
var myModal2 = document.getElementById("myModal2");
myModal2.style.display = "none";

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close2")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    myModal2.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    myModal2.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == myModal2) {
    myModal2.style.display = "none";
  }
}
<?php } ?>
</script>


<script type="text/javascript" src="/helpdesk-dev/upload/js/select2.full.min.js?7b1eee8"></script>