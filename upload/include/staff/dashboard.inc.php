<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<?php
$report = new OverviewReport($_POST['start'], '-7 days');
$plots = $report->getPlotData();

/* Start: Build charts's X-Axis dates */
$startDate = (!empty($_POST) && $_POST['start'] !== '') ? $_POST['start'] : date('Y-m-d', strtotime('6 days ago', strtotime(date('Y-m-d'))));
$endDate = (!empty($_POST) && $_POST['end'] !== '') ? $_POST['end'] : date('Y-m-d');
//$startDateTime = new DateTime($startDate);
//$enddateTime = new DateTime($endDate);
//$difference = $startDateTime->diff($enddateTime);
$dateCount = floor(strtotime($endDate)/(60*60*24)) - floor(strtotime($startDate)/(60*60*24));
// $period = isset($_POST['period']) ? $_POST['period'] : 'lastSevenDays';
// $dateCount = $difference->d; // By default, display dates for last seven days from current date
// $dateCount = ($period === 'lastFourteenDays') ? 13 : $dateCount;
// $dateCount = ($period === 'oneMonth') ? 30 : $dateCount;

for ($i=($dateCount); $i>=0; $i--)
{
  $chartXAxisDates[] = date('d-m-Y', strtotime('+'.$i.' days', strtotime($startDate)));
}
$chartXAxisDates = array_reverse($chartXAxisDates);
/* End: Build charts's X-Axis dates */


// Fetch all the mall user has access to
$mallsData = $thisstaff->getMallsDetails();
$staffSubCompanyId = $thisstaff->getSubCompanyId();

if (!empty($_POST) && $_POST['mall_id'] !== '' && (int)$_POST['mall_id'] !== 0)
{
  $subCompanyIds = array($_POST['mall_id']);
}
else
{
  $subCompanyIds = explode(',', $staffSubCompanyId);
}

/* Start: Fetch plots */
$plotData = [];
if (count($chartXAxisDates))
  $plotData = $report->getHighchartPlotData($chartXAxisDates, $subCompanyIds);
/* End: Fetch plots */
?>

<form method="post" action="dashboard.php">
  <div id="basic_search">
    <div style="min-height:25px;">
      <table>
        <tr>
          <td>
            <!--<p><?php //echo __('Select the starting time and period for the system activity graph');?></p>-->
            <?php echo csrf_token(); ?>
            <label>
                <?php echo __( 'Start date'); ?>:
                <input type="text" class="dp input-medium search-query" id="startDate"
                    name="start" placeholder="<?php echo __('Start Date');?>"
                    value="<?php
                        echo Format::htmlchars(date('d-m-Y', strtotime($startDate)));
                    ?>" />
            </label>
          </td>

          <td style="padding-left:10px;">
            <label>
                <?php echo __( 'End date'); ?>:
                <input type="text" class="dp input-medium search-query" id="endDate"
                    name="end" placeholder="<?php echo __('End Date');?>"
                    value="<?php
                        echo Format::htmlchars(date('d-m-Y', strtotime($endDate)));
                    ?>" />
            </label>
          </td>

          <!--
            <td style="padding-left:10px;">
            <label>
                <?php // echo __( 'Period');?>:
                <select name="period" id="period">
                    <option value="lastSevenDays">
                        <?php // echo __( 'Last 7 days');?>
                    </option>
                    <option value="lastFourteenDays">
                        <?php // echo __( 'Last 14 days');?>
                    </option>
                    <option value="oneMonth">
                        <?php // echo __( 'One Month');?>
                    </option>
                </select>
            </label>
          </td>
          -->

          <td style="padding-left:10px;">
            <?php if (count($mallsData) > 1) { ?>
              <label>
                <?php echo __( 'Mall'); ?>:
                <select name="mall_id">
                      <option value="0">
                          <?php echo __( 'All Malls');?>
                      </option>

                      <?php
                          foreach($mallsData as $mall) {
                            $selected = '';
                            if (!empty($_POST) && $_POST['mall_id'] !== '')
                            {
                              $selected = ( (int)$_POST['mall_id'] == (int)$mall['sub_company_id'] ) ? 'selected' : '';
                            }
                      ?>
                            <option value="<?php echo $mall['sub_company_id']; ?>"  <?php echo $selected; ?> >
                                <?php echo __( ucwords(strtolower($mall['name'])) );?>
                            </option>
                      <?php 
                          }
                      ?>
                  </select>
              </label>
            <?php } ?>

            <button class="green button action-button muted" type="submit">
                <?php echo __( 'Refresh');?>
            </button>

            <i class="help-tip icon-question-sign" href="#report_timeframe"></i>
          </td>
             
        </tr>
      </table>
    </div>
  </div>
<div class="clear"></div>
<div style="margin-bottom:20px; padding-top:5px;">
    <div class="pull-left flush-left">
        <h2><?php echo __('Ticket Activity');
            ?>&nbsp;<i class="help-tip icon-question-sign" href="#ticket_activity"></i></h2>
    </div>
</div>
<div class="clear"></div>


<!-- Create a graph and fetch some data to create pretty dashboard -->
<div style="position:relative"> 
  <div id='mychart' style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>



<hr/>
<h2><?php echo __('Statistics'); ?>&nbsp;<i class="help-tip icon-question-sign" href="#statistics"></i></h2>
<!-- <p><?php // echo __('Statistics of tickets organized by department, help topic, and agent.');?></p> -->
<p><b><?php echo __('Range: '); ?></b>
  <?php
  /*
  $range = array();
  foreach ($report->getDateRange() as $date)
  {
    $date = str_ireplace('FROM_UNIXTIME(', '',$date);
    $date = str_ireplace(')', '',$date);
    $date = new DateTime('@'.$date);
    $date->setTimeZone(new DateTimeZone($cfg->getTimezone()));
    $timezone = $date->format('e');
    $range[] = $date->format('F j, Y');
  }
  // echo __($range[0] . ' - ' . $range[1] .  ' (' . Format::timezone($timezone) . ')');
  echo __(date_format(date_create($range[0]),"d-m-Y") . ' - ' . date_format(date_create($range[1]),"d-m-Y"));
 */

echo __( date('d-m-Y', strtotime($startDate)) .' - '. date('d-m-Y', strtotime($endDate)));
  
?>

<ul class="clean tabs">
<?php
$first = true;
$groups = $report->enumTabularGroups();
foreach ($groups as $g=>$desc) { ?>
    <li class="<?php echo $first ? 'active' : ''; ?>"><a href="#<?php echo Format::slugify($g); ?>"
        ><?php echo Format::htmlchars($desc); ?></a></li>
<?php
    $first = false;
} ?>
</ul>

<?php
$first = true;
foreach ($groups as $g=>$desc) {
    $data = $report->getTabularData($g, date('Y-m-d', strtotime($startDate)) ,date('Y-m-d', strtotime($endDate)) , $subCompanyIds);
?>
    
    <div class="tab_content <?php echo (!$first) ? 'hidden' : ''; ?>" id="<?php echo Format::slugify($g); ?>">
    <table class="dashboard-stats table"><tbody><tr>
<?php
    foreach ($data['columns'] as $j=>$c) {
  ?>
        <th <?php if ($j === 0) echo 'width="30%" class="flush-left"'; ?>><?php echo Format::htmlchars($c);
        switch ($c) {
          case 'Opened':
            ?>
              <i class="help-tip icon-question-sign" href="#opened"></i>
            <?php
            break;
          case 'Assigned':
            ?>
              <i class="help-tip icon-question-sign" href="#assigned"></i>
            <?php
            break;
            case 'Overdue':
              ?>
                <i class="help-tip icon-question-sign" href="#overdue"></i>
              <?php
              break;
            case 'Closed':
              ?>
                <i class="help-tip icon-question-sign" href="#closed"></i>
              <?php
              break;
            case 'Reopened':
              ?>
                <i class="help-tip icon-question-sign" href="#reopened"></i>
              <?php
              break;
            case 'Deleted':
              ?>
                <i class="help-tip icon-question-sign" href="#deleted"></i>
              <?php
              break;
            case 'Service Time':
              ?>
                <i class="help-tip icon-question-sign" href="#service_time"></i>
              <?php
              break;
            case 'Response Time':
              ?>
                <i class="help-tip icon-question-sign" href="#response_time"></i>
              <?php
              break;
        }
        ?></th>
  <?php
      } 
  ?>
    </tr></tbody>
    <tbody>
<?php
    foreach ($data['data'] as $i=>$row) {
        echo '<tr>';
        foreach ($row as $j=>$td) {
            if ($j === 0) { ?>
                <th class="flush-left"><?php echo Format::htmlchars($td); ?></th>
<?php       }
            else { ?>
                <td><?php echo Format::htmlchars($td);
                if ($td) { // TODO Add head map
                }
                echo '</td>';
            }
        }
        echo '</tr>';
    }
    $first = false; ?>
    </tbody></table>
    <div style="margin-top: 5px; display:none;"><button type="submit" class="link button" name="export"
        value="<?php echo Format::htmlchars($g); ?>">
        <i class="icon-download"></i>
        <?php echo __('Export'); ?></a></div>
    </div>
<?php
}
?>

</form>
<script>
    // $.drawPlots(<?php // echo JsonDataEncoder::encode($report->getPlotData()); ?>);
    // Set Selected Period For Dashboard Stats and Export
    <?php if ($report && $report->end) { ?>
        $("div#basic_search select#period option").each(function(){
            // Remove default selection
            if ($(this)[0].selected)
                $(this).removeAttr('selected');
            // Set the selected period by the option's value (periods equal
            // option's values)
            if ($(this).val() == "<?php echo $period; ?>")
                $(this).attr("selected","selected");
        });
    <?php } ?>

    var chartXAxisDates = <?php echo json_encode($chartXAxisDates); ?>;
    var seriesData = <?php echo json_encode($plotData); ?>;
    Highcharts.chart('mychart', {
    chart: {
        type: 'column'
    },
    exporting: {
      buttons: {
        contextButton: {
          menuItems: [
            'printChart',
            'separator',
            'downloadPNG',
            'downloadJPEG',
            'downloadPDF',
            'downloadSVG',
            'separator',
            'downloadCSV',
            'downloadXLS'
            // ,'viewData'
          ]
        }
      }
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: chartXAxisDates,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'No. of tickets'
        },
        tickInterval: 1
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
            // colorByPoint: true
        }
    },
    colors: [
        '#5555BB',
        '#5588BB',
        '#BB5555',
        '#88BB55',
        '#BB8855',
        '#8093A5'
    ],
    series: seriesData,
    credits: {
      enabled: false
    },
    lang: {
      contextButtonTitle: 'Download or print chart'
  }
});
</script>
