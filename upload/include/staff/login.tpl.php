<?php
include_once(INCLUDE_DIR.'staff/login.header.php');
$info = ($_POST && $errors)?Format::htmlchars($_POST):array();
?>
<h3 id="loading"><i class="icon-spinner icon-spin" aria-hidden="true"></i>&nbsp;Please wait...</h3>
<div id="brickwall" style="display:none;"></div>
<div id="loginBox" style="display:none;">
    <div id="blur">
        <div id="background"></div>
    </div>
    <h1 id="logo"><a href="index.php">
        <span class="valign-helper"></span>
        <img src="logo.php?login" alt="osTicket :: <?php echo __('Staff Control Panel');?>" />
    </a></h1>
    <h3 id="login-message"><?php echo Format::htmlchars($msg); ?></h3>
	<h3 id="loading" style="display:none;"><i class="icon-spinner icon-spin" aria-hidden="true"></i>&nbsp;Please wait...</h3>
    <div class="banner"><small><?php echo ($content) ? Format::display($content->getLocalBody()) : ''; ?></small></div>
    <form action="login.php" method="post" id="login" onsubmit="attemptLoginAjax(event)">
        <?php csrf_token(); ?>
        <input type="hidden" name="do" value="scplogin">
        <fieldset>
        <input type="text" name="userid" id="name" value="<?php echo $userInfoResult->userName; ?>" placeholder="<?php echo __('Email or Username'); ?>" autofocus autocorrect="off" autocapitalize="off">
        <input type="password" name="passwd" id="pass" placeholder="<?php echo __('Password'); ?>" autocorrect="off" autocapitalize="off" value="<?php echo $defaultPassword; ?>">
		<i class="glyphicon icon-eye-open form-control-feedback"></i>
            <h3 style="display:inline"><a id="reset-link" class="<?php
                if (!$show_reset || !$cfg->allowPasswordReset()) echo 'hidden';
                ?>" href="pwreset.php"><?php echo __('Forgot My Password'); ?></a></h3>
				<p id="textCaps">Caps lock is ON.</p>
            <button class="submit button pull-right" type="submit"
                name="submit"><i class="icon-signin"></i>
                <?php echo __('Log In'); ?>
            </button>
        </fieldset>
    </form>
<?php
$ext_bks = array();
foreach (StaffAuthenticationBackend::allRegistered() as $bk)
    if ($bk instanceof ExternalAuthentication)
        $ext_bks[] = $bk;

if (count($ext_bks)) { ?>
<div class="or">
    <hr/>
</div><?php
    foreach ($ext_bks as $bk) { ?>
<div class="external-auth"><?php $bk->renderExternalLink(); ?></div><?php
    }
} ?>

    <div id="company">
        <div class="content">
            <?php echo __('Copyright'); ?> &copy; <?php echo date('Y'); ?> <?php echo Format::htmlchars($ost->company) ?: date('Y'); ?>. <?php echo __('All rights reserved.'); ?>
        </div>
    </div>
</div>
<!-- 
        <div id="poweredBy"><?php // echo __('Powered by Ascencia Limited'); ?>
    <a href="http://www.osticket.com" target="_blank">
        <img alt="osTicket" src="images/osticket-grey.png" class="osticket-logo">
    </a>
</div>
-->
    <script>
    document.addEventListener('DOMContentLoaded', function() {
        if (undefined === window.getComputedStyle(document.documentElement).backgroundBlendMode) {
            document.getElementById('loginBox').style.backgroundColor = 'white';
        }
    });

    $(document).ready(function(){
        $('#login').submit();
    });

    function attemptLoginAjax(e) {
		$("#login-message").hide();
        $("#loading").show();
        var objectifyForm = function(formArray) { //serialize data function
            var returnArray = {};
            for (var i = 0; i < formArray.length; i++) {
                returnArray[formArray[i]['name']] = formArray[i]['value'];
            }
            return returnArray;
        };
        if ($.fn.effect) {
            // For some reason, JQuery-UI shake does not considere an element's
            // padding when shaking. Looks like it might be fixed in 1.12.
            // Thanks, https://stackoverflow.com/a/22302374
            var oldEffect = $.fn.effect;
            $.fn.effect = function (effectName) {
                if (effectName === "shake") {
                    var old = $.effects.createWrapper;
                    $.effects.createWrapper = function (element) {
                        var result;
                        var oldCSS = $.fn.css;

                        $.fn.css = function (size) {
                            var _element = this;
                            var hasOwn = Object.prototype.hasOwnProperty;
                            return _element === element && hasOwn.call(size, "width") && hasOwn.call(size, "height") && _element || oldCSS.apply(this, arguments);
                        };

                        result = old.apply(this, arguments);

                        $.fn.css = oldCSS;
                        return result;
                    };
                }
                return oldEffect.apply(this, arguments);
            };
        }
        var form = $(e.target),
            data = objectifyForm(form.serializeArray())
        data.ajax = 1;
        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: data,
            cache: false,
            success: function(json) {
                if (!typeof(json) === 'object' || !json.status)
                    return;
                switch (json.status) {
                case 401:
                    if (json && json.redirect)
                        document.location.href = json.redirect;
                    if (json && json.message)
                        $('#login-message').text(json.message)
                    if (json && json.show_reset)
                        $('#reset-link').show()
                    if ($.fn.effect) {
                        $('#loginBox').effect('shake')
                    }
                    // Clear the password field
                    $('#pass').val('').focus();
                    break
                case 302:
                    if (json && json.redirect)
                    {
                        document.location.href = json.redirect;
                    }
                    break
                }
				$("#loading").hide();
                $("#login-message").show();
            },
        });
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        return false;
    }
    </script>

    <script>

        var text = document.getElementById("textCaps");

        document.addEventListener("keydown", function(event) {

        if (typeof event.getModifierState === "function" && event.getModifierState("CapsLock")) {
            text.style.display = "block";
          } else {
            text.style.display = "none"
          }

        });

        $('#pass + .glyphicon').on('click', function() {
              $(this).toggleClass('icon-eye-close').toggleClass('icon-eye-open'); // toggle our classes for the eye icon
              $('#pass').togglePassword(); // activate the hideShowPassword plugin
            });

    </script>

    <!--[if IE]>
    <style>
        #loginBox:after { background-color: white !important; }
    </style>
    <![endif]-->
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery-ui-1.12.1.custom.min.js?7b1eee8"></script>

    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/hideShowPassword.min.js?7b1eee8"></script>
     <style type="text/css">
         
          #pass + .glyphicon {
           cursor: pointer;
           pointer-events: all;
         }

         .form-control-feedback {
            float: right;
            margin-right: 6px;
            margin-top: -34px;
            position: relative;
            z-index: 2;
        }

        #textCaps {display:none;color:green}

     </style>

</body>
</html>
