<?php
#Get real path for root dir ---linux and windows
$here = dirname(__FILE__);
$here = ($h = realpath($here)) ? $h : $here;
$root_dir = str_replace('\\', '/', $here.'/');
$root_dir = str_replace('staff/', '', $root_dir);
unset($here); unset($h);

// Define constants
define('INCLUDE_DIR', $root_dir);

include_once(INCLUDE_DIR.'ost-config.php');

if (isset($_POST))
{
    $serviceProvidersId = $_POST['serviceProvidersId'];
    $extendByHour = $_POST['extentionHour'];
    $userId = $_POST['userId'];
    $ec3_ticket_id = $_POST['ec3_ticket_id'];
    $accessToken = $_POST['accesstoken'];

    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
    $url = "https://";   
    else  
        $url = "http://";   
    // Append the host(domain name, ip) to the URL.   
    $url.= $_SERVER['HTTP_HOST'];   
    // Append the requested resource location to the URL   
    $url.= $_SERVER['REQUEST_URI'];  
    $url = str_replace('include/staff/extendslarequest.php', 'scp/tickets.php', $url);

    $data = array(                    
        'ticketId' => $ec3_ticket_id,
        'userId' => $userId,
        'serviceProviderId' => $serviceProvidersId,
        'numOfHoursOfExtension' => $extendByHour
    );

    $headers = array(
        'Content-Type: application/json',
        'Authorization: Bearer ' . $accessToken,
        'Accept: application/json'
    );
    $payload = json_encode($data);

    $curl = curl_init(EC3_CURL_URL.':'.EC3_CURL_URL_PORT.'/api/helpdesk/createSLAExtensionRequest');
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLINFO_HEADER_OUT, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
    $response = curl_exec($curl);
    $results = json_decode($response);
    $code = ($results !== NULL) ? (int)$results->code : 3000; // Code = 0 means success. Any other number means an error occured.

    // Redirect to the ticket view
    ob_start();
    header('Location: '.$url.'&action=slaextension&code='.$code);
    ob_end_flush();
    die();
   // var_dump($serviceProvidersId, $extendByHour, $userId, $ec3_ticket_id);die();


}