<?php
// Calling convention (assumed global scope):
// $tickets - <QuerySet> with all columns and annotations necessary to
//      render the full page


// Impose visibility constraints
// ------------------------------------------------------------
if (!$queue->ignoreVisibilityConstraints($thisstaff))
    $tickets->filter($thisstaff->getTicketsVisibility());

// Make sure the cdata materialized view is available
TicketForm::ensureDynamicDataView();

// Identify columns of output
$columns = $queue->getColumns();

// Figure out REFRESH url — which might not be accurate after posting a
// response
list($path,) = explode('?', $_SERVER['REQUEST_URI'], 2);
$args = array();
parse_str($_SERVER['QUERY_STRING'], $args);

// Remove commands from query
unset($args['id']);
if ($args['a'] !== 'search') unset($args['a']);

$refresh_url = $path . '?' . http_build_query($args);

// Establish the selected or default sorting mechanism
if (isset($_GET['sort']) && is_numeric($_GET['sort'])) {
    $sort = $_SESSION['sort'][$queue->getId()] = array(
        'col' => (int) $_GET['sort'],
        'dir' => (int) $_GET['dir'],
    );
}
elseif (isset($_GET['sort'])
    // Drop the leading `qs-`
    && (strpos($_GET['sort'], 'qs-') === 0)
    && ($sort_id = substr($_GET['sort'], 3))
    && is_numeric($sort_id)
    && ($sort = QueueSort::lookup($sort_id))
) {
    $sort = $_SESSION['sort'][$queue->getId()] = array(
        'queuesort' => $sort,
        'dir' => (int) $_GET['dir'],
    );
}
elseif (isset($_SESSION['sort'][$queue->getId()])) {
    $sort = $_SESSION['sort'][$queue->getId()];
}
elseif ($queue_sort = $queue->getDefaultSort()) {
    $sort = $_SESSION['sort'][$queue->getId()] = array(
        'queuesort' => $queue_sort,
        'dir' => (int) $_GET['dir'] ?: 0,
    );
}

// Handle current sorting preferences

$sorted = false;
foreach ($columns as $C) {
    // Sort by this column ?
    if (isset($sort['col']) && $sort['col'] == $C->id) {
        $tickets = $C->applySort($tickets, $sort['dir']);
        $sorted = true;
    }
}
if (!$sorted && isset($sort['queuesort'])) {
    // Apply queue sort-dropdown selected preference
    $sort['queuesort']->applySort($tickets, $sort['dir']);
}

// Apply pagination

$page = ($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
$pageNav = new Pagenate(PHP_INT_MAX, $page, PAGE_LIMIT);
$tickets = $pageNav->paginateSimple($tickets);

if (isset($tickets->extra['tables'])) {
    // Creative twist here. Create a new query copying the query criteria, sort, limit,
    // and offset. Then join this new query to the $tickets query and clear the
    // criteria, sort, limit, and offset from the outer query.
    $criteria = clone $tickets;
    $criteria->limit(500);
    $criteria->annotations = $criteria->related = $criteria->aggregated =
        $criteria->annotations = $criteria->ordering = [];
    $tickets->constraints = $tickets->extra = [];
    $tickets = $tickets->filter(['ticket_id__in' =>
            $criteria->values_flat('ticket_id')]);
    # Index hint should be used on the $criteria query only
    $tickets->clearOption(QuerySet::OPT_INDEX_HINT);
    $tickets->distinct('ticket_id');
}

$count = $queue->getCount($thisstaff) ?: (PAGE_LIMIT*3);
$pageNav->setTotal($count, true);
$pageNav->setURL('tickets.php', $args);

// Defined functions to fetch Asset Type, Asset Name and Associated Costs
function getAssetType($ticket_id)
{
    if ($ticket_id)
    {
        $sql = 'SELECT asset_type_name from ost_ticket WHERE ticket_id='.db_input($ticket_id);
        $results=db_query($sql);
        while($ht = db_fetch_array($results))
        {
            return $ht['asset_type_name'];
        }
    }
    return;
}

// Defined functions to fetch Asset Type, Asset Name and Associated Costs
function getAssetName($ticket_id)
{
    if ($ticket_id)
    {
        $sql = 'SELECT asset_name from ost_ticket WHERE ticket_id='.db_input($ticket_id);
        $results=db_query($sql);
        while($ht = db_fetch_array($results))
        {
            return $ht['asset_name'];
        }
    }
    return;
}

// Defined functions to fetch Asset Type, Asset Name and Associated Costs
function getAssociatedCost($ticket_id)
{
    if ($ticket_id)
    {
        $sql = 'SELECT associated_cost from ost_ticket WHERE ticket_id='.db_input($ticket_id);
        $results=db_query($sql);
        while($ht = db_fetch_array($results))
        {
            return $ht['associated_cost'];
        }
    }
    return;
}

// Defined functions to fetch sub-company id associated to ticket
function getSubCompanyId($ticket_id)
{
    if ($ticket_id)
    {
        $sql = 'SELECT ar_sub_company_id from ost_ticket WHERE ticket_id='.db_input($ticket_id);
        $results=db_query($sql);
        while($ht = db_fetch_array($results))
        {
            return $ht['ar_sub_company_id'];
        }
    }
    return;
}

// Defined functions to fetch assigned staff_id
function getAssignedStaffId($ticket_id)
{
    if ($ticket_id)
    {
        $sql = 'SELECT staff_id from ost_ticket WHERE ticket_id='.db_input($ticket_id);
        $results=db_query($sql);
        if($row = db_fetch_array($results))
        {
            return $row['staff_id'];
        }
    }
    return null;
}

// Defined functions to fetch sub-company ids associated to ticket
function getUserAssociatedSubCompanyId($staffId)
{
    if ($staffId)
    {
        $sql = 'SELECT ar_sub_company_id from ost_staff WHERE staff_id='.db_input($staffId);
        $results=db_query($sql);
        if($row = db_fetch_array($results))
        {
            $subCompanies = $row['ar_sub_company_id'];
            return explode(",", $subCompanies);
        }
    }
    return null;
}

// Defined functions to fetch staff role id
function getUserRoleId($staffId)
{   
    if ($staffId)
    {
        $sql = 'SELECT role_id from ost_staff WHERE staff_id='.db_input($staffId);
        $results=db_query($sql);
        while($ht = db_fetch_array($results))
        {
            return $ht['role_id'];
        }
    }
    return;
}

$user_associated_sub_companies = getUserAssociatedSubCompanyId($thisstaff->getId());
$user_role_id = getUserRoleId($thisstaff->getId());

?>

<!-- SEARCH FORM START -->
<div id='basic_search'>
  <div class="pull-right" style="height:25px">
    <span class="valign-helper"></span>
    <?php
    require 'queue-quickfilter.tmpl.php';
    if ($queue->getSortOptions())
        require 'queue-sort.tmpl.php';
    ?>
  </div>
    <form action="tickets.php" method="get" onsubmit="javascript:
  $.pjax({
    url:$(this).attr('action') + '?' + $(this).serialize(),
    container:'#pjax-container',
    timeout: 2000
  });
return false;">
    <input type="hidden" name="a" value="search">
    <input type="hidden" name="search-type" value=""/>
    <div class="attached input">
      <input type="text" class="basic-search" data-url="ajax.php/tickets/lookup" name="query"
        autofocus size="30" value="<?php echo Format::htmlchars($_REQUEST['query'], true); ?>"
        autocomplete="off" autocorrect="off" autocapitalize="off">
      <button type="submit" class="attached button"><i class="icon-search"></i>
      </button>
    </div>
    <a href="#" onclick="javascript:
        $.dialog('ajax.php/tickets/search', 201);"
        >[<?php echo __('advanced'); ?>]</a>
        <i class="help-tip icon-question-sign" href="#advanced"></i>
    </form>
</div>
<!-- SEARCH FORM END -->

<div class="clear"></div>
<div style="margin-bottom:20px; padding-top:5px;">
    <div class="sticky bar opaque">
        <div class="content">
            <div class="pull-left flush-left">
                <h2><a href="<?php echo $refresh_url; ?>"
                    title="<?php echo __('Refresh'); ?>"><i class="icon-refresh"></i> <?php echo
                    $queue->getName(); ?></a>
                    <?php
                    if (($crit=$queue->getSupplementalCriteria()))
                        echo sprintf('<i class="icon-filter"
                                data-placement="bottom" data-toggle="tooltip"
                                title="%s"></i>&nbsp;',
                                Format::htmlchars($queue->describeCriteria($crit)));
                    ?>
                </h2>
            </div>
            <!--
            <div class="configureQ">
                <i class="icon-cog"></i>
                <div class="noclick-dropdown anchor-left">
                    <ul>
                        <li>
                            <a class="no-pjax" href="#"
                              data-dialog="ajax.php/tickets/search/<?php //echo urlencode($queue->getId()); ?>"><i
                            class="icon-fixed-width icon-pencil"></i>
                            <?php // echo __('Edit'); ?></a>
                        </li>
                        <li>
                            <a class="no-pjax" href="#"
                              data-dialog="ajax.php/tickets/search/create?pid=<?php // echo $queue->getId(); ?>"><i
                            class="icon-fixed-width icon-plus-sign"></i>
                            <?php // echo __('Add Sub Queue'); ?></a>
                        </li>
<?php

// if ($queue->id > 0 && $queue->isOwner($thisstaff)) { ?>
                        <li class="danger">
                            <a class="no-pjax confirm-action" href="#"
                                data-dialog="ajax.php/queue/<?php // echo $queue->id; ?>/delete"><i
                            class="icon-fixed-width icon-trash"></i>
                            <?php // echo __('Delete'); ?></a>
                        </li>
<?php //} ?>
                    </ul>
                </div>
            </div>
            -->

          <div class="pull-right flush-right">
            <?php
            // TODO: Respect queue root and corresponding actions
            if ($count) {
                // Ticket::agentActions($thisstaff, array('status' => $status));
            }?>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>

<form action="?" method="POST" name='tickets' id="tickets">
<?php csrf_token(); ?>
 <input type="hidden" name="a" value="mass_process" >
 <input type="hidden" name="do" id="action" value="" >

<table class="list queue tickets" border="0" cellspacing="1" cellpadding="2" width="100%">
  <thead>
    <tr>
<?php
$canManageTickets = $thisstaff->canManageTickets();
if ($canManageTickets) { ?>
        <th style="width:12px"></th>
<?php
}

foreach ($columns as $C) {
    $heading = Format::htmlchars($C->getLocalHeading());
    if ($C->isSortable()) {
        $args = $_GET;
        $dir = $sort['col'] != $C->id ?: ($sort['dir'] ? 'desc' : 'asc');
        $args['dir'] = $sort['col'] != $C->id ?: (int) !$sort['dir'];
        $args['sort'] = $C->id;
        $heading = sprintf('<a href="?%s" class="%s">%s</a>',
            Http::build_query($args), $dir, $heading);
    }
    echo sprintf('<th width="%s" data-id="%d">%s</th>',
        $C->getWidth(), $C->id, $heading);
}
//echo sprintf('<th  data-id="">Asset Type</th>');
//echo sprintf('<th  data-id="">Asset Name</th>');
//echo sprintf('<th  data-id="">Associated Cost</th>');
?>
    </tr>
  </thead>
  <tbody>
<?php

/*
    Call API to get all the Malls (ID => NAME) from ASCE.
    Then, loop over the tickets. Foreach of the tickets, get the ar_sub_company_id and try to get the corresponding Mall info
    from the resultset returned by the API. If mathch found, display the Mall's name under Mall column in the tickets grid.
*/
$accessToken = $_COOKIE['accessToken'];
$userCompanyId = 0;
$arraySubCompanies = NULL;
if ($accessToken !== '' && $accessToken !== NULL)
{
    // Get the company_id
    $userInfos = ExternalLogin::fetchUserInfo($accessToken);
    $userInfo = json_decode($userInfos);
    $userInfoResult = $userInfo->result;
    if ($userInfoResult !== null && $userInfo->code === 0)
    {
        $userCompanyId = ($userInfoResult->company !== NULL) ? $userInfoResult->company->companyId : $userCompanyId;
    }

    if ($userCompanyId !== 0  &&  $userCompanyId !== NULL)
    {
        $result = SubCompanyService::listSubCompanies($userCompanyId, $accessToken);
        $listSubCompanies = ($result !== NULL) ? $result->listSubCompanies: NULL;
        if ($listSubCompanies !== NULL)
        {
            foreach($listSubCompanies as $listSubCompany)
            {
                $arraySubCompanies[$listSubCompany->subCompanyId] = $listSubCompany->name;
            }
        }
    }
}

foreach ($tickets as $T) {
    // Perform this check to filter ticker per sub-company associated only if not Role=1
    // if ($user_role_id > 1)
    if (!$thisstaff->isAdmin())
    {
        // Fetch the sub_company ticket is associated with from ost_ticket table
        $ticket_sub_company_id = getSubCompanyId($T['ticket_id']);
        if(!in_array($ticket_sub_company_id, $user_associated_sub_companies))
            continue;
    }

    // If viewing Un-Assigned tickets, then filter based on Tasks.
    // If a ticket has a task, then it should not appear as part of Un-Assigned tickets.
    if ($queue->getId() === 18)
    {
        if (Ticket::hasTasks($T['ticket_id']))
            continue;
    }

    // If viewing Work-In-Progress tickets, then filter based on Tasks.
    // If a ticket has not a task, then it should not appear as part of Un-Assigned tickets.
    if ($queue->getId() === 17)
    {
        if (!Ticket::hasTasks($T['ticket_id']))
            continue;
    }

    if($thisstaff->isAccessLimited()) {
        $assignedStaffId = getAssignedStaffId($T['ticket_id']);
        $hideTicket = isset($assignedStaffId) && $thisstaff->getId() != $assignedStaffId;
        if($thisstaff->isAdmin()) {
            $hideTicket = isset($assignedStaffId) && $assignedStaffId != 0 && $thisstaff->getId() != $assignedStaffId;
        }
        if($hideTicket) {
            continue;
        }
    }

    echo '<tr>';
    if ($canManageTickets) { ?>
        <td><input type="checkbox" class="ckb" name="tids[]"
            value="<?php echo $T['ticket_id']; ?>" /></td>
<?php
    }
    foreach ($columns as $C) {
        $primaryKey = $C->getPrimary();
        list($contents, $styles) = $C->render($T);
        
        if ($primaryKey === 'cdata__subject')
        {
            preg_match("/href=\"(.*?)\"/i", $contents, $hrefSubject);
            $newHrefSubject =  str_replace($hrefSubject[1], $hrefSubject[1]."&accessToken=".$_COOKIE['accessToken'], $hrefSubject[1]);
            $contents =  str_replace($hrefSubject[1], $newHrefSubject, $contents);
        }
        
        if ($primaryKey === 'number')
        {
            $iconClass = 'webTicket';
            $source = Ticket::getTicketSource($T['ticket_id']);
            $ticketNumber = Ticket::getTicketNumber($T['ticket_id']);
            if ($source === 'Email')
            {
                $iconClass = 'emailTicket';
            }
            if ($source === 'Phone')
            {
                $iconClass = 'phoneTicket';
            }
            if ($source === 'API')
            {
                $iconClass = 'apiTicket';
            }
            if ($source === 'Other' || $source === 'VI')
            {
                $iconClass = 'otherTicket';
            }

            preg_match("/href=\"(.*?)\"/i", $contents, $href);
            preg_match("/data-preview=\"(.*?)\"/i", $contents, $dataPreview);

            $newHref = $href[1].'&accessToken='.$_COOKIE['accessToken'];
            $html = '<span class="Icon '.$iconClass.'"></span>';
            $html .= '<a style="display: inline" class="preview" data-preview="'.$dataPreview[1].'" href="'.$newHref.'">'.$ticketNumber.'</a>';
            $contents = $html;
        }

        if ($primaryKey === 'created' || $primaryKey === 'closed')
        {
            $contents = date_format(date_create($contents),"d-m-Y");
        }

        if ($primaryKey === 'ar_sub_company_name')
        {
            $sub_company_id = getSubCompanyId($T['ticket_id']);
            if ($sub_company_id !== NULL && $sub_company_id !== 0)
            {
                $contents = ($arraySubCompanies !== NULL && array_key_exists((int)$sub_company_id, $arraySubCompanies)) ? $arraySubCompanies[(int)$sub_company_id] : '';
            }
            // $contents = Ticket::getSubCompanyName($T['ticket_id']);
        }

        if ($primaryKey === 'request_from_list_item_id')
        {
            $contents = Ticket::getRequestFrom($T['ticket_id']);
        }

        if ($style = $styles ? 'style="'.$styles.'"' : '') 
        {
            echo "<td $style><div $style>$contents</div></td>";
        }
        else {
            echo "<td>$contents</td>";
        }
    }
    $_assetTypeName = getAssetType($T['ticket_id']);
    $_assetName = getAssetName($T['ticket_id']);
    $_associatedCost = number_format(getAssociatedCost($T['ticket_id']),2);
	//echo "<td>$_assetTypeName</td>";
	//echo "<td>$_assetName</td>";
    //echo "<td style='text-align:right;'>$_associatedCost</td>";
    echo '</tr>';
}

?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="<?php echo count($columns)+1; ?>">
        <?php if ($count && $canManageTickets) {
        echo __('Select');?>:&nbsp;
        <a id="selectAll" href="#ckb"><?php echo __('All');?></a>&nbsp;&nbsp;
        <a id="selectNone" href="#ckb"><?php echo __('None');?></a>&nbsp;&nbsp;
        <a id="selectToggle" href="#ckb"><?php echo __('Toggle');?></a>&nbsp;&nbsp;
        <?php }else{
            echo '<i>';
            echo $ferror?Format::htmlchars($ferror):__('Query returned 0 results.');
            echo '</i>';
        } ?>
      </td>
    </tr>
  </tfoot>
</table>

<?php
    if ($count > 0) { //if we actually had any tickets returned.
?>  <div>
      <span class="faded pull-right"><?php echo $pageNav->showing(); ?></span>
<?php
        echo __('Page').':'.$pageNav->getPageLinks().'&nbsp;';
        ?>
        <a href="#tickets/export/<?php echo $queue->getId(); ?>" id="queue-export" class="no-pjax"
            ><?php echo __('Export'); ?></a>
        <i class="help-tip icon-question-sign" href="#export"></i>
    </div>
<?php
    } ?>
</form>
<script type="text/javascript">
$(function() {
    $(document).on('click', 'a#queue-export', function(e) {
        e.preventDefault();
        var url = 'ajax.php/'+$(this).attr('href').substr(1)
        $.dialog(url, 201, function (xhr) {
            window.location.href = '?a=export&queue=<?php echo $queue->getId(); ?>';
            return false;
         });
        return false;
    });
});
</script>
