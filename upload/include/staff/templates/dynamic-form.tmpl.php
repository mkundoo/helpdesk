<?php
global $thisstaff;

$isCreate = (isset($options['mode']) && $options['mode'] == 'create');

if (isset($options['entry']) && $options['mode'] == 'edit'
    && $_POST
    && ($_POST['forms'] && !in_array($options['entry']->getId(), $_POST['forms']))
)
    return;
	
$myPDO = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME, DBUSER, DBPASS);
$myPDO->query("SET NAMES utf8");
$request_from_list_query = $myPDO->query("SELECT id,code,value FROM custom_request_from_list_items WHERE active = 1 ORDER BY value ASC");
$request_from_tenant_query = $myPDO->query("SELECT id,value,email_addresses FROM custom_tenants WHERE active = 1 AND sub_company_id IN (".$thisstaff->getSubCompanyId().") ORDER BY value ASC");
$request_from_service_provider_query = $myPDO->query("SELECT id,value,email_addresses FROM custom_service_providers WHERE active = 1 AND sub_company_id IN (".$thisstaff->getSubCompanyId().") ORDER BY value ASC");
$request_from_staff_query = $myPDO->query("SELECT id,value,email_addresses FROM custom_staffs WHERE active = 1 AND sub_company_id IN (".$thisstaff->getSubCompanyId().") ORDER BY value ASC");

if (isset($options['entry']) && $options['mode'] == 'edit') { ?>

<tbody>
<?php } ?>
    <tr><td style="width:<?php echo $options['width'] ?: 150;?>px;"></td><td></td></tr>
<?php
// Keep up with the entry id in a hidden field to decide what to add and
// delete when the parent form is submitted
if (isset($options['entry']) && $options['mode'] == 'edit') { ?>
    <input type="hidden" name="forms[]" value="<?php
        echo $options['entry']->getId(); ?>" />
<?php } ?>
<?php if ($form->getTitle()) { ?>
    <tr><th colspan="2">
        <em>
<?php if ($options['mode'] == 'edit') { ?>
        <div class="pull-right">
    <?php if ($options['entry']
                && $options['entry']->getDynamicForm()->get('type') == 'G') { ?>
            <a href="#" title="Delete Entry" onclick="javascript:
                $(this).closest('tbody').remove();
                return false;"><i class="icon-trash"></i></a>&nbsp;
    <?php } ?>
            <i class="icon-sort" title="Drag to Sort"></i>
        </div>
<?php } ?>
        <strong><?php echo Format::htmlchars($form->getTitle()); ?></strong>:
        <div><?php echo Format::display($form->getInstructions()); ?></div>
        </em>
    </th></tr>
    <?php
    }
    foreach ($form->getFields() as $field) {
        try {
            if (!$field->isEnabled())
                continue;
        }
        catch (Exception $e) {
            // Not connected to a DynamicFormField
        }
        ?>
		<?php if ($field->getLocal('label') == 'Request From'): ?>
		<tr>
			<td class="multi-line <?php if ($field->isRequiredForStaff() || $field->isRequiredForClose()) echo 'required';
			?>" style="min-width:120px;" <?php if ($options['width'])
				echo "width=\"{$options['width']}\""; ?>>
			<?php echo Format::htmlchars($field->getLocal('label')); ?>:</td>
			<td>
				<div style="positive:relative">
					<select name="selRequestFrom" id="selRequestFrom" data-placeholder="Select">
						<option value='0'>Please select</option>
						<?php foreach($request_from_list_query as $row) { ?>
							<option value='<?php echo $row['code']; ?>'><?php echo $row['value']; ?></option>
						<?php } ?>
					</select>
				</div>
			</td>
		</tr>
		<tr id="trRequestFromName" style="display:none;">
			<td class="multi-line <?php if ($field->isRequiredForStaff() || $field->isRequiredForClose()) echo 'required';
			?>" style="min-width:120px;" <?php if ($options['width'])
				echo "width=\"{$options['width']}\""; ?>>Name:</td>
			<td>
				<div style="positive:relative">
					<input type="text" id="txtRequestFromName" size="16" maxlength="100" placeholder="" name="txtRequestFromName">
				</div>
			</td>
		</tr>
		<tr id="trRequestFromTenant" style="display:none;">
			<td class="multi-line <?php if ($field->isRequiredForStaff() || $field->isRequiredForClose()) echo 'required';
			?>" style="min-width:120px;" <?php if ($options['width'])
				echo "width=\"{$options['width']}\""; ?>>Tenant:</td>
			<td>
				<div style="positive:relative">
					<select id="selTenant" class="ostField" name="selTenant">
						<?php foreach($request_from_tenant_query as $row) { ?>
							<option value='<?php echo $row['id']; ?>' data-email-addresses='<?php echo $row['email_addresses']; ?>'><?php echo $row['value']; ?></option>
						<?php } ?>
					</select>
				</div>
			</td>
		</tr>
		<tr id="trRequestFromServiceProvider" style="display:none;">
			<td class="multi-line <?php if ($field->isRequiredForStaff() || $field->isRequiredForClose()) echo 'required';
			?>" style="min-width:120px;" <?php if ($options['width'])
				echo "width=\"{$options['width']}\""; ?>>ServiceProvider:</td>
			<td>
				<div style="positive:relative">
					<select id="selServiceProvider" class="ostField" name="selServiceProvider">
						<?php foreach($request_from_service_provider_query as $row) { ?>
							<option value='<?php echo $row['id']; ?>' data-email-addresses='<?php echo $row['email_addresses']; ?>'><?php echo $row['value']; ?></option>
						<?php } ?>
					</select>
				</div>
			</td>
		</tr>
		<tr id="trRequestFromStaff" style="display:none;">
			<td class="multi-line <?php if ($field->isRequiredForStaff() || $field->isRequiredForClose()) echo 'required';
			?>" style="min-width:120px;" <?php if ($options['width'])
				echo "width=\"{$options['width']}\""; ?>>Staff:</td>
			<td>
				<div style="positive:relative">
					<select id="selStaff" class="ostField" name="selStaff">
						<?php foreach($request_from_staff_query as $row) { ?>
							<option value='<?php echo $row['id']; ?>' data-email-addresses='<?php echo $row['email_addresses']; ?>'><?php echo $row['value']; ?></option>
						<?php } ?>
					</select>
				</div>
			</td>
		</tr>
		<tr id="trRequestFromEmail" style="display:none;">
			<td class="multi-line <?php if ($field->isRequiredForStaff() || $field->isRequiredForClose()) echo 'required';
			?>" style="min-width:120px;" <?php if ($options['width'])
				echo "width=\"{$options['width']}\""; ?>>Email:</td>
			<td>
				<div style="positive:relative">
					<input type="text" id="txtRequestFromEmailAdd" size="16" maxlength="128" placeholder="" name="txtRequestFromEmailAdd">
					<span id="emailAddresses"></span>
				</div>
			</td>
		</tr>
		<?php endif; ?>
		<?php if ($field->getLocal('label') !== 'Request From'): ?>
        <tr><?php if ($field->isBlockLevel()) { ?>
                <td colspan="2">
                <?php
            }
            else { ?>
                <td class="multi-line <?php if ($field->isRequiredForStaff() || $field->isRequiredForClose()) echo 'required';
                ?>" style="min-width:120px;" <?php if ($options['width'])
                    echo "width=\"{$options['width']}\""; ?>>
                <?php echo Format::htmlchars($field->getLocal('label')); ?>:</td>
                <td><div style="position:relative"><?php
            }

            if ($field->isEditableToStaff() || $isCreate) {
                $field->render($options); ?>
                <?php if (!$field->isBlockLevel() && $field->isRequiredForStaff()) { ?>
                    <span class="error">*</span>
                <?php
                }
                if ($field->isStorable() && ($a = $field->getAnswer()) && $a->isDeleted()) {
                    ?><a class="action-button float-right danger overlay" title="Delete this data"
                        href="#delete-answer"
                        onclick="javascript:if (confirm('<?php echo __('You sure?'); ?>'))
                            $.ajax({
                                url: 'ajax.php/form/answer/'
                                    +$(this).data('entryId') + '/' + $(this).data('fieldId'),
                                type: 'delete',
                                success: $.proxy(function() {
                                    $(this).closest('tr').fadeOut();
                                }, this)
                            });
                        return false;"
                        data-field-id="<?php echo $field->getAnswer()->get('field_id');
                    ?>" data-entry-id="<?php echo $field->getAnswer()->get('entry_id');
                    ?>"> <i class="icon-trash"></i> </a></div><?php
                }
                if ($a && !$a->getValue() && $field->isRequiredForClose()) {
    ?><i class="icon-warning-sign help-tip warning"
        data-title="<?php echo __('Required to close ticket'); ?>"
        data-content="<?php echo __('Data is required in this field in order to close the related ticket'); ?>"
    /></i><?php
                }
                if ($field->get('hint') && !$field->isBlockLevel()) { ?>
                    <br /><em style="color:gray;display:inline-block"><?php
                        echo Format::viewableImages($field->getLocal('hint')); ?></em>
                <?php
                }
                foreach ($field->errors() as $e) { ?>
                    <div class="error"><?php echo Format::htmlchars($e); ?></div>
                <?php }
            } else {
                $val = '';
                if ($field->value)
                    $val = $field->display($field->value);
                elseif (($a= $field->getAnswer()))
                    $val = $a->display();

                echo $val;
            }?>
            </div></td>
        </tr>
		<?php endif; ?>
    <?php }
if (isset($options['entry']) && $options['mode'] == 'edit') { ?>
</tbody>
<?php } ?>
