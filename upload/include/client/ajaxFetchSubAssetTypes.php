<?php
    define('INCLUDE_DIR', true); // Required for below line..
    require_once('../../services/class.sub_asset_type.php');

	$subCompanyId = (isset($_GET['subCompanyId']) && $_GET['subCompanyId'] !== '') ? $_GET['subCompanyId'] : 0;
	$locationId = (isset($_GET['locationId']) && $_GET['locationId'] !== '') ? $_GET['locationId'] : 0;
    $assetTypeId = (isset($_GET['assetTypeId']) && $_GET['assetTypeId'] !== '') ? $_GET['assetTypeId'] : 0;
    $accessToken = (isset($_GET['accessToken']) && $_GET['accessToken'] !== '') ? $_GET['accessToken'] : '';
    $html = '';

    // Fetch data from DB
    if($assetTypeId !== 0 && $accessToken !== '')
    {
        $arrayData = SubAssetTypeService::listSubAssetTypes($assetTypeId, $locationId, $accessToken);
        if(count($arrayData))
        {
            $html = '<option value="0">Please select</option>';
            foreach($arrayData as $row) 
            {
                $html .= "<option value='".$row->assetSubTypeId."'>".$row->assetSubTypeName."</option>";
            }
        }

        die( json_encode(array(
                'msg' => 'Successfully pulled Sub Asset Types',
                'success' => true,
                'html' => $html
            ))
        );
    }

    // At this point there is an error
    die( json_encode(array(
        'msg' => 'An error occured',
        'success' => false,
        'html' => ''
        ))
    );
?>