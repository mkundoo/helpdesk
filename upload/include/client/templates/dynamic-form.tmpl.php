<?php
// Return if no visible fields
global $thisclient;
if (!$form->hasAnyVisibleFields($thisclient))
    return;

$isCreate = (isset($options['mode']) && $options['mode'] == 'create');
$userLevel = isset($_SESSION['userLevel']) ? $_SESSION['userLevel'] : 'L0';

// $myPDO = new PDO('pgsql:host='.DBHOST_EC3.';dbname='.DBNAME_EC3, DBUSER_EC3, DBPASS_EC3);
// $subCompanies = $myPDO->query("SELECT sub_company_id,name FROM sub_companies WHERE sub_company_is_deleted = false ORDER BY name asc");

$myPDO = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME, DBUSER, DBPASS);
// $request_from_list_query = $myPDO->query("SELECT id,code,value FROM custom_request_from_list_items WHERE active = 1 ORDER BY value ASC");
$sla_query =  $myPDO->query("SELECT id,grace_period FROM ost_sla WHERE grace_period = 24 OR grace_period = 48");
?>
	<!-- START: Header Title -->
    <tr>
		<td colspan="3">
            <!-- <hr />
			<div class="form-header" style="margin-bottom:0.5em">
				<h3><?php // echo Format::htmlchars($form->getTitle()); ?></h3>
				<div><?php // echo Format::display($form->getInstructions()); ?></div>
			</div> -->
			<select id="slaId" name="slaId" style="display:none;">
				<?php foreach($sla_query as $row) { ?>
					<option value="<?=$row['id']?>"><?=$row['grace_period']?></option>
				<?php } ?>
			</select>
		</td>
	</tr>
	<!-- END: Header Title -->

    
    <tr class='subCompanyField'>
        <!-- Start: Malls -->
        <td style="padding-top:8px;">
            <label for="subCompanyId">
                <span class="required">Mall<span class="error">*</span></span>
                <br/>
                <select id="subCompany" class="ostField" name="subCompanyId" onchange="javascript:fetchSubCompanyLocationsZones(this.value);fetchRequestFromAssociatedDropdowns();">
                    <option value='0'>Loading...</option>
                </select>
                <input type="hidden" id="subCompanyName" name="subCompanyName" value="" />
            </label>
        </td>
         <!-- End: Malls -->

         <!-- Start: Locations -->
        <td style="padding-top:8px;" id='locationId'>
            <label for="locationId" >
                <span class="required">Location<span class="error">*</span></span>
                <br/>
                <select id="locationId" class="ostField" name="locationId" onchange="javascript:setLocationName(this.value);fetchAssetType(this.value);" disabled></select>
				<input type="hidden" id="locationName" name="locationName" />
            </label>
        </td>
        <!-- End: Locations -->

        <!-- Start: Zones -->
        <td style="padding-top:8px;" id='zoneId' style="display:none;">
            <label for="zoneId">
                <span class="required">Zone<span class="error">*</span></span>
                <br/>
                <select id="zoneId" class="ostField" name="zoneId" onchange="javascript:fetchServicesRelatedItems(this.value);" disabled></select>
            </label>
        </td>
        <!-- End: Zones -->

        <td style="padding-top:8px;" class="tdPriority" id="nonServiceRelatedPriority">
					
        </td>

        <!-- Start: Services Related Item -->
        <td style="padding-top:8px;display:none;" class="serviceRelatedField">
            <label for="servicesRelatedItemId">
                <span class="required">Service Related Items<span class="error">*</span></span>
                <br/>
                <select id="servicesRelatedItem" class="ostField"  name="servicesRelatedItemId" onChange="javascript:setservicesRelatedItemName(this.value);" disabled>
                    
                </select>
                <input type="hidden" id="servicesRelatedItemName" name="servicesRelatedItemName" value="" />
            </label>
        </td>
        <!-- End: Services Related Item -->

        <?php foreach ($form->getFields() as $field) {
            try {
                if (!$field->isEnabled())
                {
                    $count++;
                    continue;
                }
            }
            catch (Exception $e) {
                // Not connected to a DynamicFormField
            }

            if ($isCreate) 
            {
                if (!$field->isVisibleToUsers() && !$field->isRequiredForUsers())
                {	
                    $count++;
                    continue;
                }
            }
            elseif (!$field->isVisibleToUsers()) 
            {
                $count++;
                continue;
            }
            
        ?>
            
        <?php } ?>
    </tr>
	
    <?php
    // Form fields, each with corresponding errors follows. Fields marked
    // 'private' are not included in the output for clients
	
    foreach ($form->getFields() as $field) {
        try {
            if (!$field->isEnabled())
			{
                continue;
			}
        }
        catch (Exception $e) {
            // Not connected to a DynamicFormField
        }

        if ($isCreate) 
		{
            if (!$field->isVisibleToUsers() && !$field->isRequiredForUsers())
			{	
                continue;
			}
        }
		elseif (!$field->isVisibleToUsers()) 
		{
            continue;
        }
        
        /*if ($field->getLocal('label') === 'Mall' || $field->getLocal('label') === 'Location')
        {
            continue;
        }*/
	?>
        <!-- START: Fields display for Common Assets option only. Fields define here and then unwanted fields are removed via Jquery in open.inc.php -->
        <?php // if ($field->getLocal('label') === 'Request From'): ?>
            
            <!-- <tr class="commonAssetsFields" style="display:none;"> -->
                <!-- Start: Common Assets -->
                <!--<td>
                    <label for="commonAssetId">
                        <span class="required">Common Asset</span>
                        <br/>
                        <select id="commonAssetId" class="ostField"  name="commonAssetId" onChange="javascript:setCommonAssetName(this.value);" >
                            <option value='0'>Loading...</option>
                        </select>
                        <input type="hidden" id="commonAssetName" name="commonAssetName" value="" />
                    </label>
                </td>-->
                <!-- End: Common Assets -->

                <!-- START:  Priority field -->
                <!-- <td style="padding-top:8px;" class="tdPriority"></td> -->
                <!-- END:  Priority field -->
            <!-- <tr> -->
        <?php // endif; ?>
        <!-- END: Fields display for Common Assets option only -->

        <?php if ($field->getLocal('label') === 'Request From'): ?>
            <!-- <tr style="display:none;font-weight: bold;" id="intDeptExtServiceError" class="error"></tr> -->
			<tr colspan="2">    
                <!-- Start: Internal Departements -->
                <!--
                <td style="padding-top:8px;display:none;" class="serviceRelatedField">
                    <label for="internalDepartmentId">
                        <span class="required">Internal Department</span>
                        <br/>
                        <select id="internalDepartment" class="ostField"  name="internalDepartmentId" onChange="javascript:setInternalDepartmentName(this.value);" >
                            <option value='0'>Loading...</option>
                        </select>
                        <input type="hidden" id="internalDepartmentName" name="internalDepartmentName" value="" />
                    </label>
                </td>
                -->
                <!-- End: Internal Departement -->

                <td style="padding-top:8px;" class="assetsFields">
                    <label for="assetTypeId">
                        <span class="required">Asset Type<span class="error">*</span></span>
                        <br/>
                        <select id="assetType" name="assetTypeId" onchange="javascript:fetchAssetSubType(this.value);" disabled></select>
                        <input type="hidden" id="assetTypeName" name="assetTypeName" />
                    </label>
                </td>
           
                <td class="assetsFields" style="padding-top:8px;">
					<label for="subAssetTypeId">
						<span class="required">Sub-Asset Type<span class="error">*</span></span>
						<br/>
						<select id="subAssetType" name="subAssetTypeId" onchange="javascript:fetchAssetName(this.value);" disabled></select>
						<input type="hidden" id="subAssetTypeName" name="subAssetTypeName" value="" />
					</label>
				</td>

                <td class="assetsFields" style="padding-top:8px;">
					<label for="assetNameId">
						<span class="required">Asset Name<span class="error">*</span></span>
						<br/>
						<select id="assetNameId" name="assetNameId" onchange="javascript:setAssetName(this);" disabled></select>
						<input type="hidden" id="assetName" name="assetName" value="" />
					</label>
				</td>

                <!-- Start: External Services is visible only for Service Related Category -->
                <!-- 
                <td style="padding-top:8px;display:none;" class="serviceRelatedField">
                    <label for="externalServiceId">
                        <span class="required">External Service</span>
                        <br/>
                        <select id="externalService"  name="externalServiceId" class="ostField" onChange="javascript:setExternalServiceName(this.value);">
                            <option value='0'>Loading...</option>
                        </select>
                        <input type="hidden" id="externalServiceName" name="externalServiceName" value="" />
                    </label>
                </td>
                -->
                <!-- End: External Services is visible only for Service Related Category -->

				
			</tr>
		<?php endif; ?>
        
		<?php if ($field->getLocal('label') === 'Request From'): ?>
            <?php // if ($userLevel === 'L0'): ?>
                <tr class="requestFromFields">
                    <td style="padding-top:8px;">
                        <label for="sourceOfTicket">
                            <span class="required">Source of Ticket<span class="error">*</span></span>
                            <br/>
                            <select id="sourceOfTicket"  name="sourceOfTicket" class="ostField" onChange="javascript:setSourceOfTicketName(this.value);">
                                <option value='0'>Loading...</option>
                            </select>
                            <input type="hidden" id="sourceOfTicketId" name="sourceOfTicketId" value="" />
                        </label>
                    </td>

                    <td style="padding-top:8px;">
                        <label for="selRequestFrom">
                            <span class="required">User Type<span class="error">*</span></span>
                            <br/>
                            <select id="selRequestFrom" class="ostField" name="selRequestFrom">
                                <option value='0' data-code="none">Please select</option>
                            </select>
                        </label>
                    </td>

                    <td style="padding-top:8px;" class="tdPriority" id="serviceRelatedPriority">
					
                    </td>
                </tr>

                <tr class="requestFromFields">
                    <td style="padding-top:8px;">
                        <label for="txtRequestFromName" style="display:none;">
                            <span class="required">Name<span class="error">*</span></span>
                            <br/>
                            <input type="text" id="txtRequestFromName" size="16" maxlength="100" placeholder="" name="txtRequestFromName">
                        </label>
                        <label for="selTenant" style="display:none;">
                            <span class="required">Tenant<span class="error">*</span></span>
                            <br/>
                            <select id="selTenant" class="ostField" name="selTenant">
                                <option value='0'>Please select</option>
                            </select>
                        </label>
                        <label for="selServiceProvider" style="display:none;">
                            <span class="required">Service Provider<span class="error">*</span></span>
                            <br/>
                            <select id="selServiceProvider" class="ostField" name="selServiceProvider">
                                <option value='0'>Please select</option>
                            </select>
                        </label>
                        <label for="selStaff" style="display:none;">
                            <span class="required">Staff<span class="error">*</span></span>
                            <br/>
                            <select id="selStaff" class="ostField" name="selStaff">
                                <option value='0'>Please select</option>
                            </select>
                        </label>
                    </td>

                    <td style="padding-top:8px;">
                        <label for="txtRequestFromEmailAdd" style="display:none;">
                            <span class="required">Email address<span class="error">*</span></span>
                            <br/>
                            <input type="text" id="txtRequestFromEmailAdd" size="16" maxlength="128" placeholder="" name="txtRequestFromEmailAdd">
                            <span id="spanNoEmailAddress">None</span>
                            <span id="spanLoadingEmailAddress" style="display:none;">Loading email address(es)...</span>
                            <ul id="ulEmailAdd"></ul>
                        </label>
                    </td>

                    <td style="padding-top:8px;">
                        <label for="txtPhoneNumber" style="display:none;">
                            <span class="required">Phone No.<span class="error">*</span></span>
                            <br/>
                            <input type="text" id="txtPhoneNumber" size="8" maxlength="8" placeholder="" name="txtPhoneNumber">
                            <span id="spanNoPhoneNumber">None</span>
                            <span id="spanLoadingPhoneNumber" style="display:none;">Loading phone number...</span>
                            <ul id="ulPhoneNumberAdd"></ul>
                        </label>
                    </td>
                </tr>
            <?php // endif; ?>

            <!-- For L1 and above, display Source of Ticket field just before Issue Summary field -->
            <?php // if ($userLevel !== 'L0'): ?>
                <!-- 
                <tr id="sourceTicketL1AndAbove">
                    <td style="padding-top:8px;">
                        <label for="sourceOfTicket">
                            <span class="required">Source of Ticket<span class="error">*</span></span>
                            <br/>
                            <select id="sourceOfTicket"  name="sourceOfTicket" class="ostField" onChange="javascript:setSourceOfTicketName(this.value);">
                                <option value='0'>Loading...</option>
                            </select>
                            <input type="hidden" id="sourceOfTicket" name="sourceOfTicket" value="" />
                        </label>
                    </td>

                    <td style="padding-top:8px;" class="tdPriority" id="serviceRelatedPriority">
					
                    </td>
                </tr>
                -->
            <?php // endif; ?>
		<?php endif; ?>
		
        <?php if ($field->getLocal('label') !== 'Request From'): ?>
        <tr class="<?php echo $field->getFormName(); ?>">
            <td colspan="2" style="padding-top:10px;">
            <?php if (!$field->isBlockLevel()) { ?>
                <label for="<?php echo $field->getFormName(); ?>"><span class="<?php
                    if ($field->isRequiredForUsers()) echo 'required'; ?>">
                <?php echo Format::htmlchars($field->getLocal('label')); ?>
            <?php if ($field->isRequiredForUsers() &&
                    ($field->isEditableToUsers() || $isCreate)) { ?>
                <span class="error">*</span>
            <?php }
            ?></span><?php
                if ($field->get('hint')) { ?>
                    <br /><em style="color:gray;display:inline-block"><?php
                        echo Format::viewableImages($field->getLocal('hint')); ?></em>
                <?php
                } ?>
            <br/>
            <?php
            }
            if ($field->isEditableToUsers() || $isCreate) {
                $field->render(array('client'=>true));
                ?></label><?php
                foreach ($field->errors() as $e) { ?>
                    <div class="error"><?php echo $e; ?></div>
                <?php }
                $field->renderExtras(array('client'=>true));
            } else {
                $val = '';
                if ($field->value)
                    $val = $field->display($field->value);
                elseif (($a=$field->getAnswer()))
                    $val = $a->display();

                echo sprintf('%s </label>', $val);
            }
            ?>
            </td>
        </tr>
        <?php endif; ?>
		
<?php
    }
?>