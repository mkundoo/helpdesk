<?php
	define('INCLUDE_DIR', true); // Required for below line..
	require_once('../../services/class.common_asset.php');

	$companyId = (isset($_GET['companyId']) && $_GET['companyId'] !== '') ? $_GET['companyId'] : 0;
	$accessToken = (isset($_GET['accessToken']) && $_GET['accessToken'] !== '') ? $_GET['accessToken'] : '';
    $html = '';

	if($companyId !== 0 && $accessToken !== '')
	{
		$arrayData = CommonAssetService::listCommonAssetByCompnay($companyId, $accessToken);
		if(count($arrayData))
		{
			$html = '<option value="0">Please select</option>';
			foreach($arrayData as $row) 
			{
				$html .= "<option value='".$row->commonAssetId."'>".$row->commonAssetName."</option>";
			}
		}
	}

	die( json_encode(array(
			'msg' => 'Successfully pulled Staffs',
			'success' => true,
			'html' => $html
		))
	);
?>