<?php
    define('INCLUDE_DIR', true); // Required for below line..
    require_once('../../services/class.asset.php');

    $assetSubTypeId = (isset($_GET['assetSubTypeId']) && $_GET['assetSubTypeId'] !== '') ? $_GET['assetSubTypeId'] : 0;
	$subCompanyId = (isset($_GET['subCompanyId']) && $_GET['subCompanyId'] !== '') ? $_GET['subCompanyId'] : 0;
	$locationId = (isset($_GET['locationId']) && $_GET['locationId'] !== '') ? $_GET['locationId'] : 0;
    $accessToken = (isset($_GET['accessToken']) && $_GET['accessToken'] !== '') ? $_GET['accessToken'] : '';
    $html = '';

    // Fetch data from DB
    if($assetSubTypeId !== 0 && $accessToken !== '')
    {
        $arrayData = AssetService::listAssets($assetSubTypeId, $locationId, $accessToken);
        if(count($arrayData))
        {
            $html = '<option value="0" data-criticality="medium">Please select</option>';
            foreach($arrayData as $row) 
            {
                $criticalityObject = $row->criticality;
                $html .= "<option value='".$row->assetId."' data-criticality='".strtolower(trim($criticalityObject->name))."'>".$row->assetName."</option>";
            }
        }

        die( json_encode(array(
                'msg' => 'Successfully pulled Assets',
                'success' => true,
                'html' => $html
            ))
        );
    }

    // At this point there is an error
    die( json_encode(array(
        'msg' => 'An error occured',
        'success' => false,
        'html' => ''
        ))
    );
?>