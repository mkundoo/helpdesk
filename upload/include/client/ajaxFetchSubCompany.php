<?php
	define('INCLUDE_DIR', true); // Required for below line..
	require_once('../../services/class.sub_company.php');

	$companyId = (isset($_GET['companyId']) && $_GET['companyId'] !== '') ? $_GET['companyId'] : 0;
	$accessToken = (isset($_GET['accessToken']) && $_GET['accessToken'] !== '') ? $_GET['accessToken'] : '';
    $html = '';

	if($companyId !== 0 && $accessToken !== '')
	{
		$arrayData = SubCompanyService::listSubCompanies($companyId, $accessToken);
		if($arrayData->listSubCompanies && count($arrayData->listSubCompanies))
		{
			$html = '<option value="0">Please select</option>';
			foreach($arrayData->listSubCompanies as $row) 
			{
				$html .= "<option value='".$row->subCompanyId."'>".$row->name."</option>";
			}
		}
	}

	die( json_encode(array(
			'msg' => 'Successfully pulled Staffs',
			'success' => true,
			'html' => $html
		))
	);
?>