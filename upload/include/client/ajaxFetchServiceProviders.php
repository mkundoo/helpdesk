<?php
	define('INCLUDE_DIR', true); // Required for below line..
	require_once('../../services/class.service_provider.php');

	$subCompanyId = (isset($_GET['subCompanyId']) && $_GET['subCompanyId'] !== '') ? $_GET['subCompanyId'] : 0;
	$accessToken = (isset($_GET['accessToken']) && $_GET['accessToken'] !== '') ? $_GET['accessToken'] : '';
	$html = '';
	
	if ( $subCompanyId !== 0 && $accessToken !== '')
	{
		$arrayData = ServiceProvider::listServiceProvidersBySubCompanyId($subCompanyId, $accessToken);
		if(count($arrayData))
		{
			$html = '<option value="0">Please select</option>';
			foreach($arrayData as $row) 
			{
				$html .= "<option value='".$row->ticketServiceProviderId."' data-email='".$row->serviceProviderEmailAddress."' data-phone='".$row->serviceProviderFixedPhone."'>".$row->serviceProviderName."</option>";
			}
		}
	}

	die( json_encode(array(
			'msg' => 'Successfully pulled Service Providers',
			'success' => true,
			'html' => $html
		))
	);
?>