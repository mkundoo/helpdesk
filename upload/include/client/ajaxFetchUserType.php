<?php
	define('INCLUDE_DIR', true); // Required for below line..
	require_once('../../services/class.user_type.php');

	$accessToken = (isset($_GET['accessToken']) && $_GET['accessToken'] !== '') ? $_GET['accessToken'] : '';
	$companyId = (isset($_GET['companyId']) && $_GET['companyId'] !== '') ? $_GET['companyId'] : 0;
    $html = '';

	if($companyId !== 0 && $accessToken !== '')
	{
		$arrayData = UserTypeService::listUserTypes($companyId, $accessToken);
		if(count($arrayData))
		{
			$html = '<option value="0">Please select</option>';
			foreach($arrayData as $row) 
			{
				$html .= "<option value='".$row->userTypeId."-".$row->userTypeKey."' data-code='".$row->userTypeKey."'>".$row->userTypeName."</option>";
			}
		}
	}

	die( json_encode(array(
			'msg' => 'Successfully pulled User Types',
			'success' => true,
			'html' => $html
		))
	);
?>