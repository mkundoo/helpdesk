<?php
    define('INCLUDE_DIR', true); // Required for below line..
    require_once('../../services/class.zone.php');

    $subCompanyId = (isset($_GET['subCompanyId']) && $_GET['subCompanyId'] !== '') ? $_GET['subCompanyId'] : 0;
    $accessToken = (isset($_GET['accessToken']) && $_GET['accessToken'] !== '') ? $_GET['accessToken'] : '';
    $html = '';

    // Fetch data from DB
    if($subCompanyId !== 0 && $accessToken !== '')
    {
        $arrayData = ZoneService::listZonesBySubCompanyId($subCompanyId, $accessToken);
        if(count($arrayData))
        {
            $html = '<option value="0">Please select</option>';
            foreach($arrayData as $row) 
            {
				$html .= "<option value='".$row->zoneId."'>".$row->name."</option>";
            }
        }

        die( json_encode(array(
                'msg' => 'Successfully pulled Zones',
                'success' => true,
                'html' => $html
            ))
        );
    }

    // At this point there is an error
    die( json_encode(array(
        'msg' => 'An error occured',
        'success' => false,
        'html' => ''
        ))
    );
?>