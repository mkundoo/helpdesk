<?php
if(!defined('OSTCLIENTINC')) die('Access Denied!');
require_once(INCLUDE_DIR.'class.external_login.php');
$userInfos = ExternalLogin::fetchUserInfo($accessToken);
$userInfo = json_decode($userInfos);
$userInfoResult = $userInfo->result;
$userError = true;
$userCompanyId = 0;
$userId = 0;
$canSelectStaffOsticket = 'true';
if ($userInfoResult !== null && $userInfo->code === 0)
{
	$userCompanyId = $userInfoResult->company->companyId;
	$_SESSION['userLevel'] = $userInfoResult->userLevel->name;
	$userId = $userInfoResult->userId;
	$canSelectStaffOsticket = ($userInfoResult->canSelectStaffOsticket === false) ? 'false' : $canSelectStaffOsticket;
	
	$userError = false;
}

if (!$userError)
{
	$info=array();
	if($thisclient && $thisclient->isValid()) {
		$info=array('name'=>$thisclient->getName(),
					'email'=>$thisclient->getEmail(),
					'phone'=>$thisclient->getPhoneNumber());
	}

	$info=($_POST && $errors)?Format::htmlchars($_POST):$info;

	$form = null;
	if (!$info['topicId']) {
		if (array_key_exists('topicId',$_GET) && preg_match('/^\d+$/',$_GET['topicId']) && Topic::lookup($_GET['topicId']))
			$info['topicId'] = intval($_GET['topicId']);
		else
			$info['topicId'] = $cfg->getDefaultTopicId();
	}

	$forms = array();
	if ($info['topicId'] && ($topic=Topic::lookup($info['topicId']))) {
		foreach ($topic->getForms() as $F) {
			if (!$F->hasAnyVisibleFields())
				continue;
			if ($_POST) {
				$F = $F->instanciate();
				$F->isValidForClient();
			}
			$forms[] = $F->getForm();
		}
	}

?>

<style>
    .ostField, label input[type='text'], label select
    {
        width:200px;
    }
    #ticketForm > table td {
        width: 590px;
    }
    #ticketForm > table tr:nth-last-child(2) td input {
        width: 793px;
    }
    #ticketForm > table tr:nth-last-child(1) td div.redactor-box {
        width: 795px;
    }
    #ticketForm > table tr:nth-last-child(1) td div.filedrop {
        width: 797px;
    }

	label {
		cursor: default;
	}

	.error {
		font-size: 12px;
	}

	#spanNoEmailAddress {
		line-height: 25px;
	}

	#spanLoadingEmailAddress {
		line-height: 25px;
	}

	#ulEmailAdd {
		margin-top: 7px;
		padding: 0px;
	}

	#ulEmailAdd li {
		word-break: break-all;
		margin-left: -15px;
	}

	#ulPhoneNumberAdd {
		margin-top: 7px;
		padding: 0px;
	}

	#ulPhoneNumberAdd li {
		word-break: break-all;
		margin-left: -15px;
	}

	/*
	#technicalCategoryOptions td {
		padding-top: 15px;
	}
	*/
</style>

<!-- <h1><?php // echo __('Open a New Ticket');?></h1> -->
<!-- <p><?php // echo __('Please fill in the form below to open a new ticket.');?></p> -->
<form id="ticketForm" method="post" action="open.php" enctype="multipart/form-data">
  <?php csrf_token(); ?>
  <input type="hidden" name="a" value="open">
  <input type="hidden" name="userId" value="<?php echo $userId; ?>">
  <input type="hidden" name="accessToken" value="<?php echo $accessToken; ?>">
  <table width="800" cellpadding="1" cellspacing="0" border="0">
    <tbody>
<?php
        if (!$thisclient) {
            $uform = UserForm::getUserForm()->getForm($_POST);
            if ($_POST) $uform->isValid();
            $uform->render(array('staff' => false, 'mode' => 'create'));
        }
        else { ?>
            <tr><td colspan="3"><!-- <hr /> --></td></tr>
        	<!-- <tr><td><?php // echo __('Email'); ?>:</td><td><?php
            // echo $thisclient->getEmail(); ?></td></tr> -->

        	<tr style="display:none;">
				<td><?php echo __('Client'); ?>:</td>
				<td><?php echo Format::htmlchars($thisclient->getName()); ?></td>
			</tr>
        <?php } ?>
    </tbody>
    <tbody id="topicForm">
		<tr>
			<td colspan="3">
				<div class="form-header" style="margin-bottom:0.5em">
				<b><?php echo __('Incident Category'); ?></b>
				</div>
			</td>
		</tr>

		<tr>
			<td colspan="2">
				<select id="topicId" name="topicId" onchange="javascript:fetchDynamicFormFields();">
					<option value="" selected="selected">&mdash; <?php echo __('Select an Incident Category');?> &mdash;</option>
					<?php
					if($topics=Topic::getPublicHelpTopics()) {
						foreach($topics as $id =>$name) {
							echo sprintf('<option value="%d" %s>%s</option>',
									$id, ($info['topicId']==$id)?'selected="selected"':'', $name);
						}
					} else { ?>
						<option value="0" ><?php echo __('General Inquiry');?></option>
					<?php
					} ?>
				</select>
				<label class="error" for='topicId'>*&nbsp;<?php echo $errors['topicId']; ?></label>
			</td>
		</tr>
		
		<!--
			<tr id="technicalCategoryOptions" style="display:none;">
			<td><input type="radio" class="technicalCategoryOptions" name="technicalCategoryOptions" onChange="javascript:fetchDynamicFormFields();" value="mallSpecific"> Mall Specific Assets </td>
			<td><input type="radio" class="technicalCategoryOptions" name="technicalCategoryOptions" onChange="javascript:fetchDynamicFormFields();" value="commonAsset"> Common Assets </td>
		</tr>
		-->
    </tbody>
    <tbody id="dynamic-form">
        <?php
        $options = array('mode' => 'create');
        foreach ($forms as $form) {
            include(CLIENTINC_DIR . 'templates/dynamic-form.tmpl.php');
        } ?>
    </tbody>
    <tbody>
    <?php
    if($cfg && $cfg->isCaptchaEnabled() && (!$thisclient || !$thisclient->isValid())) {
        if($_POST && $errors && !$errors['captcha'])
            $errors['captcha']=__('Please re-enter the text again');
        ?>
    <tr class="captchaRow">
        <td class="required"><?php echo __('CAPTCHA Text');?>:</td>
        <td>
            <span class="captcha"><img src="captcha.php" border="0" align="left"></span>
            &nbsp;&nbsp;
            <input id="captcha" type="text" name="captcha" size="6" autocomplete="off">
            <em><?php echo __('Enter the text shown on the image.');?></em>
            <font class="error">*&nbsp;<?php echo $errors['captcha']; ?></font>
        </td>
    </tr>
    <?php
    } ?>
    <tr><td colspan=2>&nbsp;</td></tr>
    </tbody>
  </table>
<!-- <hr/> -->
  <p class="buttons" style="margin-top: -10px;">
        <input type="button" id="btnSubmit" name="btnSubmit" value="<?php echo __('Create Ticket');?>">
        <!-- 
			<input type="reset" name="reset" value="<?php // echo __('Reset');?>">
        	<input type="button" name="cancel" value="<?php // echo __('Cancel'); ?>" onclick="javascript:
				$('.richtext').each(function() {
					var redactor = $(this).data('redactor');
					if (redactor && redactor.opts.draftDelete)
						redactor.draft.deleteDraft();
				});
            	window.location.href='index.php';">
		-->
  </p>
</form>

<script>
	var accessToken = '<?php echo $accessToken; ?>';
	var userCompanyId = <?php echo $userCompanyId; ?>;
    $(document).ready(function(){
        var helpTopicText = $('#topicId option:selected').text();
        if (helpTopicText === 'Technical')
        {
            $('#topicId').trigger('change');
        }

		// Convert to dropdown without search box
		$('select#topicId').select2({
			minimumResultsForSearch: -1
		});
		
		$('#btnSubmit').on('click', function(e) {
			e.preventDefault();
			if(validateForm())
			{
				$('#ticketForm').trigger('submit');
			}
		});
    });
	
	/*
	function selTopicIdOnChange()
	{
		var helpTopicVal = $('#topicId option:selected').text();
		if (helpTopicVal === 'Technical')
		{
			$("#technicalCategoryOptions").show();
			$('#dynamic-form').empty();
		}
		else
		{
			$("#technicalCategoryOptions").hide();
			fetchDynamicFormFields();
		}
	}
	*/

	function fetchDynamicFormFields()
	{
		$('label[for=topicId]').html('*');
		var data = $(':input[name]', '#dynamic-form').serialize();
		$.ajax(
			'ajax.php/form/help-topic/' + $('#topicId').val(),
			{
				data: data,
				dataType: 'json',
				success: function(json) {
					$('#dynamic-form').empty().append(json.html);
					
					$('td.tdPriority').append($('label[for=priority]'));
					$('tr.priority').remove();
					
					// Convert to dropdown with search box
					$('select#subCompany, select#locationId, select#selTenant, select#selServiceProvider, select#selStaff, select#zoneId').select2();

					// Convert to dropdown without search box
					$('select#selRequestFrom, select#sourceOfTicket').select2({
						minimumResultsForSearch: -1
					});
					
					//  START: TEST
					var data = $('#assetsContainer').html();
					var helpTopicVal = $('#topicId option:selected').text();
					if (helpTopicVal === 'Technical')
					{
						// $('#topicForm').append(data);
						// $('#topicForm div#assetsContainer').css('display', 'block');

						// Remove non service related priority field
						$('td#serviceRelatedPriority').remove();

						// Convert to dropdown with search box
						$('select#assetType, select#subAssetType, select#assetNameId').select2();

						// Remove Zone combo
						$('tr.subCompanyField td#zoneId').remove();

						/*
						if ($("input[name='technicalCategoryOptions']:checked").val() === 'commonAsset')
						{
							// Populate Common Assets field
							fetchCommonAssets(userCompanyId);
							// Remove fields based on radio button selected
							$('tr.subCompanyField').remove();
							$('.assetsFields').remove();
							$('.commonAssetsFields').show();
						}
						else
						{
							$('.commonAssetsFields').remove();
						}
						*/
						
					} 
					else if (helpTopicVal === 'Services Related')
					{
						// Remove Location combo
						$('tr.subCompanyField td#locationId').remove();
						// Show Zone combo
						$('tr.subCompanyField td#zoneId').show();
						// Remove Assets fields
						$('.assetsFields').remove();

						// Remove service related priority field
						$('td#nonServiceRelatedPriority').remove();
					}
					else 
					{
						//$('#topicForm div#assetsContainer').css('display', 'none');
						$('.assetsFields').remove();
						// Remove Zone combo
						$('tr.subCompanyField td#zoneId').remove();

						// Remove non service related priority field
						$('td#serviceRelatedPriority').remove();

						// $('.commonAssetsFields').remove();
						// Uncheck radio buttons
						// $('input:radio[name=technicalCategoryOptions]:checked').prop('checked', false);
						
						// $('label[for=assetTypeId]').remove();
						// This field should be displayed only for Services Related category
					}

					if (helpTopicVal === 'Services Related')
					{
						// $('select#internalDepartment, select#externalService').select2();
						$('select#servicesRelatedItem').select2();
						$('.serviceRelatedField').show();
						// fetchInternalDepartment(userCompanyId);
						// fetchExternalServices(userCompanyId);
						
					}
					else
					{
						$('.serviceRelatedField').remove();
					}

					if (helpTopicVal === 'Technical' || helpTopicVal === 'Services Related' || helpTopicVal === 'Grievances')
					{
						$optionsTextToExclude = ['Low','Emergency'];
						$optionsTextToExclude.forEach(function(optionsText)
						{
							$("#_priority").find('option:contains(' + optionsText + ')').remove();
						});
					}
					else
					{
						$optionsTextToExclude = ['High','Emergency'];
						$optionsTextToExclude.forEach(function(optionsText)
						{
							$("#_priority").find('option:contains(' + optionsText + ')').remove();
						});
					}
					// END : TEST

					// Convert to dropdown without search box for Priority
					$('select#_priority').select2({
						minimumResultsForSearch: -1
					});
					
					$(document.head).append(json.media);
					// Populate Mall combo
					fetchSubCompany(userCompanyId);
					// Fetch Source Of Ticket
					fetchSourceOfTicket(userCompanyId);
					// Fetch Source Of Ticket
					fetchUserType(userCompanyId);
				}
			}
		);
	}

	function fetchUserType(userCompanyId)
	{
		$.ajax({
			type : 'GET',
			url : 'include/client/ajaxFetchUserType.php',
			dataType: 'json',
			data: { companyId : userCompanyId, accessToken : accessToken },
			success: function(json){
				if (json.success)
				{
					$("select#selRequestFrom option").remove();
					$('select#selRequestFrom').append(json.html);
				}
			},
			failure: function()
			{
				console.log('error fetch User Types');
			}
		});
	}

	function setSourceOfTicketName(sourceOfTicketId)
	{
		$('input#sourceOfTicketId').val(sourceOfTicketId);
	}

	function fetchSourceOfTicket(userCompanyId)
	{
		$.ajax({
			type : 'GET',
			url : 'include/client/ajaxFetchSourceOfTicket.php',
			dataType: 'json',
			data: { companyId : userCompanyId, accessToken : accessToken },
			success: function(json){
				if (json.success)
				{
					$("select#sourceOfTicket option").remove();
					$('select#sourceOfTicket').append(json.html);

					// For L1 and above, default value is Direct Entry by Staff
					<?php  if (isset($_SESSION['userLevel']) && $_SESSION['userLevel'] !== '' && $_SESSION['userLevel'] !== 'L0'): ?>
	
						if ($('select#sourceOfTicket option').length > 1)
						{
							var L1AboveDefaultVal = $("select#sourceOfTicket option:contains('Direct Entry By Staff')").val();
							$('select#sourceOfTicket').val(L1AboveDefaultVal);
							$('select#sourceOfTicket').select2().trigger('change');
							$('input#sourceOfTicketId').val($('select#sourceOfTicket option:selected').val());
							$("select#sourceOfTicket").prop("disabled", true);
							// $('tr#sourceTicketL1AndAbove').hide();
						}

					<?php endif; ?>
				}
			},
			failure: function()
			{
				console.log('error fetch Source of Tickets');
			}
		});
	}

	function fetchCommonAssets(userCompanyId)
	{
		$.ajax({
			type : 'GET',
			url : 'include/client/ajaxFetchCommonAssets.php',
			dataType: 'json',
			data: { companyId : userCompanyId, accessToken : accessToken },
			success: function(json){
				if (json.success)
				{
					$("select#commonAssetId option").remove();
					$('select#commonAssetId').append(json.html);
				}
			},
			failure: function()
			{
				console.log('error fetch Common Assets');
			}
		});
	}

	/*
	function fetchInternalDepartment(userCompanyId)
	{
		$.ajax({
			type : 'GET',
			url : 'include/client/ajaxFetchInternalDepartment.php',
			dataType: 'json',
			data: { companyId : userCompanyId, accessToken : accessToken },
			success: function(json){
				if (json.success)
				{
					$("select#internalDepartment option").remove();
					$('select#internalDepartment').append(json.html);
				}
			},
			failure: function()
			{
				console.log('error fetch Internal Departments');
			}
		});
	}
	*/

	/*
	function setInternalDepartmentName(internalDepartmentId)
	{
		if (internalDepartmentId && internalDepartmentId !== "0")
        {
			// Set Internal Department Name in hidden field
			$('input#internalDepartmentName').val($('select#internalDepartment option:selected').text());
		}
		else
		{
			$('input#internalDepartmentName').val('');
		}
	}
	*/

	function setCommonAssetName(commonAssetId)
	{
		if (commonAssetId && commonAssetId !== "0")
        {
			// Set Common Asset Name in hidden field
			$('input#commonAssetName').val($('select#commonAssetId option:selected').text());
		}
		else
		{
			$('input#commonAssetName').val('');
		}
	}

	/*
	function fetchExternalServices(userCompanyId)
	{
		$.ajax({
			type : 'GET',
			url : 'include/client/ajaxFetchExternalService.php',
			dataType: 'json',
			data: { companyId : userCompanyId, accessToken : accessToken },
			success: function(json){
				if (json.success)
				{
					$("select#externalService option").remove();
					$('select#externalService').append(json.html);
				}
			},
			failure: function()
			{
				console.log('error fetch External Services');
			}
		});
	}

	function setExternalServiceName(externalServiceId)
	{
		if (externalServiceId && externalServiceId !== "0")
        {
			// Set External Service Name in hidden field
			$('input#externalServiceName').val($('select#externalService option:selected').text());
		}
		else
		{
			$('input#externalServiceName').val('');
		}
	}
	*/

    function setAssetName(elem)
    {
		
		var selected = $(elem).find('option:selected');
		var selectedVal = $(selected).text();
		var code = selected.data('criticality');

        if (selectedVal !== "0")
            $('input#assetName').val(selectedVal);
        else
			$('input#assetName').val('');
			
		$("#_priority ").children().removeAttr("selected");
		if (code.trim().toLowerCase() === 'critical')
		{
			$("#_priority").val($("#_priority option:contains(High)").val());
			$("#_priority").select2().trigger('change');
			$("select#_priority").prop("disabled", true);
		}
		else
		{
			$("#_priority").val($("#_priority option:contains(Medium)").val());
			$("#_priority").select2().trigger('change');
			$("select#_priority").prop("disabled", false);
		}
    }

    function fetchAssetSubType(assetTypeId)
    {
        clearAndDisableSubAssetType();
		clearAndDisableAssetName();
		$("select#_priority").prop("disabled", false);

        if (assetTypeId && assetTypeId !== "0")
        {
            // Set Asset Type Name in hidden field
            $('input#assetTypeName').val($('select#assetType option:selected').text());

            // Set loading text
            $('select#subAssetType').append("<option value=''>Loading...</option>");
			
			var $subCompanyId = $("#subCompany").val();
			var $locationId = $("select#locationId").val();

            $.ajax({
                type : 'GET',
                url : 'include/client/ajaxFetchSubAssetTypes.php',
                dataType: 'json',
                data: {
					assetTypeId : assetTypeId,
					subCompanyId: $subCompanyId,
					locationId: $locationId,
					accessToken : accessToken
				},
                success: function(json){
                    if (json.success)
                    {
						$("select#subAssetType option").remove();
                        $('select#subAssetType').append(json.html);
						$('select#subAssetType').removeAttr("disabled");
                    }
                },
                failure: function()
                {
                    console.log('error');
                }
            });
        }
    }
    
    function fetchAssetName(assetSubTypeId)
    {
        clearAndDisableAssetName();
		$("select#_priority").prop("disabled", false);
        if (assetSubTypeId && assetSubTypeId !== "0")
        {
			var $subCompanyId = $("#subCompany").val();
			var $locationId = $("select#locationId").val();

            // Set Sub Asset Type Name in hidden field
            $('input#subAssetTypeName').val($('select#subAssetType option:selected').text());

            // Set loading text
            $('select#assetNameId').append("<option value=''>Loading...</option>");
            $.ajax({
                type : 'GET',
                url : 'include/client/ajaxFetchAssets.php',
                dataType: 'json',
                data: {
					assetSubTypeId : assetSubTypeId,
					subCompanyId: $subCompanyId,
					locationId: $locationId,
					accessToken : accessToken
				},
                success: function(json){
                    if (json.success)
                    {
						$("select#assetNameId option").remove();
                        $('select#assetNameId').append(json.html);
						$('select#assetNameId').removeAttr("disabled");
                    }
                },
                failure: function()
                {
                    console.log('error');
                }
            });
        }
    }

    function fetchSubCompanyLocationsZones(subCompanyId)
    {
		
		var $selectedTopic = $('#topicId option:selected').text();
		clearUserType();
		fetchUserType(subCompanyId);
		if($selectedTopic === 'Technical')
		{
			clearAndDisableLocation();
			clearAndDisableAssetType();
			clearAndDisableSubAssetType();
			clearAndDisableAssetName();
		}
		else if($selectedTopic === 'Services Related')
		{
			clearAndDisableZone();
			clearAndDisableServicesRelatedItem();
		}
		else {
			clearAndDisableLocation();
		}
        $('input#subCompanyName').val('');
        if (subCompanyId && subCompanyId !== "0")
        {
			var ajaxUrl = 'include/client/ajaxFetchLocations.php';
			if($selectedTopic === 'Services Related')
			{
				ajaxUrl = 'include/client/ajaxFetchZones.php';
			}
            // Set Sub-Company in hidden field
            $('input#subCompanyName').val($('select#subCompany option:selected').text());

			// Set loading text
			if($selectedTopic === 'Services Related')
			{
				$('select#zoneId').append("<option value=''>Loading...</option>");
			}
			else
			{
				$('select#locationId').append("<option value=''>Loading...</option>");
			}

            $.ajax({
                type : 'GET',
                url : ajaxUrl,
                dataType: 'json',
                data: {subCompanyId : subCompanyId, accessToken : accessToken},
                success: function(json){
                    if (json.success)
                    {
						
						if ($selectedTopic === 'Services Related')
						{
							$("select#zoneId option").remove();
							$('select#zoneId').append(json.html);
							$('select#zoneId').removeAttr("disabled");
						}
						else
						{
							$("select#locationId option").remove();
							$('select#locationId').append(json.html);
							$('select#locationId').removeAttr("disabled");
						}
                    }
                },
                failure: function()
                {
                    console.log('error');
                }
            });
        }
        else
        {
			if($selectedTopic === 'Services Related')
			{
				$("select#zoneId option").remove();
			}
			else
			{
				$('input#locationName').val('');
				$("select#locationId option").remove();
			}
        }
	}

	function clearUserType()
	{
		$("select#selRequestFrom option").remove();
		$("select#selTenant option").remove();
		$("select#selServiceProvider option").remove();
		$("select#selStaff option").remove();
		$("#ulEmailAdd").empty();
		$("#ulPhoneNumberAdd").empty();
		$("input#txtRequestFromEmailAdd").val('');
		$("input#txtPhoneNumber").val('');
		$("label[for=selTenant]").hide();
		$("label[for=selServiceProvider]").hide();
		$("label[for=selStaff]").hide();
		$("label[for=txtRequestFromName]").hide();
		$("label[for=txtRequestFromEmailAdd]").hide();
		$("label[for=txtPhoneNumber]").hide();
	}
	
	function fetchServicesRelatedItems(zoneId)
	{
		if (zoneId && zoneId !== "0")
        {
			$.ajax({
                type : 'GET',
                url : 'include/client/ajaxFetchServicesRelatedItem.php',
                dataType: 'json',
                data: {zoneId : zoneId, accessToken : accessToken},
                success: function(json){
                    if (json.success)
                    {
						$("select#servicesRelatedItem option").remove();
						$('select#servicesRelatedItem').append(json.html);
						$('select#servicesRelatedItem').removeAttr("disabled");
                    }
                },
                failure: function()
                {
                    console.log('error');
                }
            });
		}
		else
		{
			// Empty Services Related Item Name in hidden field
            $('input#servicesRelatedItemName').val('');
  
            // Empty the Services Related Item field
            $("select#servicesRelatedItem option").remove();
		}
	}

	function clearAndDisableServicesRelatedItem()
	{
		$("select#servicesRelatedItem").attr("disabled", true);
		$("select#servicesRelatedItem option").remove();
		$("#servicesRelatedItemName").val('');
	}

	function clearAndDisableZone()
	{
		$("select#zoneId").attr("disabled", true);
		$("select#zoneId option").remove();
	}

	function setservicesRelatedItemName(servicesRelatedItemId)
	{
		if (servicesRelatedItemId !== "0")
            $('input#servicesRelatedItemName').val($('select#servicesRelatedItem option:selected').text());
        else
            $('input#servicesRelatedItemName').val('');
	}

    function setLocationName(locationId)
    {
        if (locationId !== "0")
            $('input#locationName').val($('select#locationId option:selected').text());
        else
            $('input#locationName').val('');
    }

    function fetchAssetType(locationId)
    {
		clearAndDisableAssetType();
		clearAndDisableSubAssetType();
		clearAndDisableAssetName();
		$("select#_priority").prop("disabled", false);

		if($('#topicId option:selected').text() === 'Technical')
		{
			if (locationId && locationId !== "0")
			{
				// Set loading text
				$('select#assetType').append("<option value=''>Loading...</option>");
				$.ajax({
					type : 'GET',
					url : 'include/client/ajaxFetchAssetTypes.php',
					dataType: 'json',
					data: {locationId : locationId, accessToken : accessToken},
					success: function(json){
						if (json.success)
						{
							$("select#assetType option").remove();
							$('select#assetType').append(json.html);
							$('select#assetType').removeAttr("disabled");
						}
					},
					failure: function()
					{
						console.log('error');
					}
				});
			}
		}
    }

	function fetchSubCompany(userCompanyId)
	{
		clearAndDisableLocation();
		clearAndDisableAssetType();
		clearAndDisableSubAssetType();
		clearAndDisableAssetName();
		$("select#_priority").prop("disabled", false);
		
		$.ajax({
			type : 'GET',
			url : 'include/client/ajaxFetchSubCompany.php',
			dataType: 'json',
			data: { companyId : userCompanyId, accessToken : accessToken },
			success: function(json){
				if (json.success)
				{
					$("select#subCompany option").remove();
					$('select#subCompany').append(json.html);
				}
			},
			failure: function()
			{
				console.log('error fetch Sub Companies');
			}
		});
	}

	function fetchTenants()
	{
		// Empty Tenant combo first
		$("select#selTenant option").remove();
		// Set loading text
		$('select#selTenant').append("<option value=''>Loading...</option>");
		// Get sub company id
		var $subCompanyId = $("#subCompany").val();
		$.ajax({
			type : 'GET',
			url : 'include/client/ajaxFetchTenants.php',
			dataType: 'json',
			data: { subCompanyId : $subCompanyId, accessToken : accessToken },
			success: function(json){
				if (json.success)
				{
					$("select#selTenant option").remove();
					$('select#selTenant').append(json.html);
				}
			},
			failure: function()
			{
				console.log('error fetch tenant');
			}
		});
	}
	
	function fetchServiceProviders()
	{
		// Empty ServiceProvider combo first
		$("select#selServiceProvider option").remove();
		// Set loading text
		$('select#selServiceProvider').append("<option value=''>Loading...</option>");
		// Get sub company id
		var $subCompanyId = $("#subCompany").val();
		$.ajax({
			type : 'GET',
			url : 'include/client/ajaxFetchServiceProviders.php',
			dataType: 'json',
			data: { subCompanyId : $subCompanyId, accessToken : accessToken },
			success: function(json){
				if (json.success)
				{
					$("select#selServiceProvider option").remove();
					$('select#selServiceProvider').append(json.html);
				}
			},
			failure: function()
			{
				console.log('error fetch service provider');
			}
		});
	}
	
	function fetchStaffs()
	{
		// Empty Staff combo first
		$("select#selStaff option").remove();
		// Set loading text
		$('select#selStaff').append("<option value=''>Loading...</option>");
		// Get sub company id
		var $subCompanyId = $("#subCompany").val();
		$.ajax({
			type : 'GET',
			url : 'include/client/ajaxFetchStaff.php',
			dataType: 'json',
			data: { companyId : <?php echo $userCompanyId; ?>, accessToken : accessToken },
			success: function(json){
				if (json.success)
				{
					$("select#selStaff option").remove();
					$('select#selStaff').append(json.html);
					var canSelectStaff = <?php echo $canSelectStaffOsticket; ?>;
					var userId = <?php echo $userId; ?>;
					if (!canSelectStaff  && userId != 0)
					{
						$("select#selStaff option:not([value='"+userId+"'])").remove();
						$('select#selStaff').select2().trigger('change');
					}
				}
			},
			failure: function()
			{
				console.log('error fetch staff');
			}
		});
	}

	function fetchRequestFromAssociatedDropdowns()
	{
		var $requestFromCode = $("#selRequestFrom").val();
		if($requestFromCode === "tenant")
		{
			fetchTenants();
		}
		else if($requestFromCode === "service_provider")
		{
			fetchServiceProviders();
		}
		else if($requestFromCode === "staff")
		{
			fetchStaffs();
		}
	}
	
	function clearAndDisableLocation()
	{
		$("select#locationId").attr("disabled", true);
		$("select#locationId option").remove();
		$("#locationName").val('');
	}
	
	function clearAndDisableAssetType()
	{
		$("select#assetType").attr("disabled", true);
		$("select#assetType option").remove();
		$("#assetTypeName").val('');
	}
	
	function clearAndDisableSubAssetType()
	{
		$("select#subAssetType").attr("disabled", true);
		$("select#subAssetType option").remove();
		$("#subAssetTypeName").val('');
	}
	
	function clearAndDisableAssetName()
	{
		$("select#assetNameId").attr("disabled", true);
		$("select#assetNameId option").remove();
		$("#assetName").val('');
	}
	
	function validateForm()
	{
		var valid = true;
		var requiredMessage = '* Field is required';
		var helpTopicVal = $('#topicId').val();
		var helpTopicName = $('#topicId option:selected').text();
		var serviceRelatedErrMsg = false;
		$("input:visible, select:visible").each(function()
		{
			var name = $.trim($(this).attr('name').replace('[]',''));
			var value = $(this).val();
			var type = $(this).attr('type');
			
			if($(this).is('select') || ($(this).is('input') && type != 'submit' && type != 'button' && type != 'reset'))
			{
				if(name === 'topicId')
				{
					$('label[for=topicId]').html('*');
					if(typeof value == 'undefined' || value == '' || value == '0')
					{
						$('label[for=topicId]').html(requiredMessage);
						valid = false;
					}
				}
				else
				{
					$('label[for="' + name + '"] span.error').html('*');
					var invalidCondition = typeof value == 'undefined' || value == '';
					if($(this).is('select'))
					{
						invalidCondition = invalidCondition || value == '0' || value === null;
						if (helpTopicName === 'Services Related')
						{
							// Validate Internal Department and External Services fields
							/*
							if (name == 'internalDepartmentId')
								serviceRelatedErrMsg = (invalidCondition) ? invalidCondition : serviceRelatedErrMsg;
							
							if (name == 'externalServiceId')
								serviceRelatedErrMsg = (invalidCondition && serviceRelatedErrMsg) ? invalidCondition : false;
							*/
						}
						if (helpTopicName === 'Technical')
						{
							$("select#_priority").prop("disabled", false);
						}
					}
					if(invalidCondition)
					{ 
						$('label[for="' + name + '"] span.error').html(requiredMessage);
						valid = false;
					}
				}
			}
		});

		/*
		if (serviceRelatedErrMsg)
		{
			$('tr#intDeptExtServiceError').append("<td colspan='2' style='padding-top:15px;'>Select an Internal Department or External Service</td>");
			$('tr#intDeptExtServiceError').show();
		}
		else
		{
			$('tr#intDeptExtServiceError').html('');
			$('#intDeptExtServiceError').hide();
		}
		*/
		
		// Issue details field
		$('label[for=message]').remove();
		var issueDetailVal = $('textarea[name=message]').val();
		if(typeof issueDetailVal == 'undefined' || issueDetailVal == '')
		{
			var $labelError = $('<label></label>', { "for": "message" } )
								.append(
									$('<span></span>', { "class": "required" } )
										.append($('<span></span>', { "class": "error" } ).text(requiredMessage))
								);
			$('textarea[name=message]').parent().parent().prepend($labelError);
			valid = false;
		}			
		
		return valid;
	}

	$(document).on('change', '#_priority', function(){
		var $selPriorityText = $("#_priority option:selected").text();
		var $selectedSlaText = '48';
		if($selPriorityText == 'High')
		{
			$selectedSlaText = '24';
		}
		$("select#slaId option:selected").removeProp("selected");
		$("select#slaId option:contains(" + $selectedSlaText + ")").prop('selected', true);
	});
	
	$(document).on('change', '#selRequestFrom', function(){
		//var selectedValue = $(this).val();
		//var values = ['public','visitor','shopper'];
		var selected = $(this).find('option:selected');
		var code = selected.data('code');
		
		$("#ulEmailAdd").empty();
		$("#ulPhoneNumberAdd").empty();

		$("select#selTenant option").remove();
		$("select#selServiceProvider option").remove();
		$("select#selStaff option").remove();
		$("input#txtRequestFromName").val('');
		$("label[for=selTenant]").hide();
		$("label[for=selServiceProvider]").hide();
		$("label[for=selStaff]").hide();
		$("label[for=txtRequestFromName]").hide();
		$("label[for=txtRequestFromEmailAdd]").hide();
		$("label[for=txtPhoneNumber]").hide();

		$("input#txtRequestFromEmailAdd").val('');
		$("input#txtPhoneNumber").val('');

		if(code !== 'none') {
			// Email Address
			$("label[for=txtRequestFromEmailAdd]").show();
			$("#spanNoEmailAddress").show();
			$("#ulEmailAdd").show();

			// Phone Number
			$("label[for=txtPhoneNumber]").show();
			$("#spanNoPhoneNumber").show();
			$("#ulPhoneNumberAdd").show();

			if(code === 'shopper' || code === 'public' || code === 'visitor') {
				$("label[for=txtRequestFromName]").show();
				$("#txtRequestFromEmailAdd").show();
				$("#spanNoEmailAddress").hide();
				$("#ulEmailAdd").hide();
				$("#txtRequestFromEmailAdd").show();
				$("label[for=txtRequestFromEmailAdd] .error").text("*");

				$("label[for=txtPhoneNumber]").show();
				$("#txtPhoneNumber").show();
				$("#spanNoPhoneNumber").hide();
				$("#ulPhoneNumberAdd").hide();
				// $("#txtPhoneNumber").show();
				$("label[for=txtPhoneNumber] .error").text("*");
			} else {
				$("#txtRequestFromEmailAdd").hide();
				$("#txtPhoneNumber").hide();
				$("label[for=txtRequestFromEmailAdd] .error").text("");
				$("label[for=txtPhoneNumber] .error").text("");

				if (code === 'tenant') {
					fetchTenants();
					$("label[for=selTenant]").show();
				} else if (code === 'service_provider') {
					fetchServiceProviders();
					$("label[for=selServiceProvider]").show();
				} else if (code === 'staff') {
					fetchStaffs();
					$("label[for=selStaff]").show();
				}
			}
		}
	});

	$(document).on('change', '#selTenant, #selServiceProvider, #selStaff', function(){
		// Empty list of email addresses
		$("#ulEmailAdd").empty();
		$("#spanNoEmailAddress").hide();

		// Empty Phone number
		$("#ulPhoneNumberAdd").empty();
		$("#spanNoPhoneNumber").hide();

		var $selectedId = $(this).val();

		if($selectedId == 0)
		{
			$("#spanNoEmailAddress").show();
			$("#spanNoPhoneNumber").show();
		}
		else
		{
			var email = $(this).find(':selected').data('email');
			if (email !== '' && email !== null)
			{
				$("#ulEmailAdd").append(email);
			}
			else
			{
				$("#spanNoEmailAddress").show();
			}

			var phoneNumber = $(this).find(':selected').data('phone');
			if (phoneNumber !== '' && phoneNumber !== null)
			{
				$("#ulPhoneNumberAdd").append(phoneNumber);
			}
			else
			{
				$("#spanNoPhoneNumber").show();
			} 

			/*
			var $ajaxUrl = 'include/client/ajaxFetchTenants.php';
			var $ajaxData = { tenantId : $selectedId };

			if($(this).attr('id') === "selServiceProvider")
			{
				var email = $(this).find(':selected').data('email');
				if (email !== '' && email !== null)
				{
					$("#ulEmailAdd").append(email);
				}
				else
				{
					$("#spanNoEmailAddress").show();
				}
			}
			else if($(this).attr('id') === "selStaff")
			{
				$ajaxUrl = 'include/client/ajaxFetchStaff.php';
				$ajaxData = { staffId : $selectedId };
			}
			*/

			/*
				$.ajax({
					type : 'GET',
					url : $ajaxUrl,
					dataType: 'json',
					data: $ajaxData,
					beforeSend: function() {
						// Set loading text
						$("#spanLoadingEmailAddress").show();
					},
					success: function(json){
						// Remove loading text
						$("#spanLoadingEmailAddress").hide();
						if (json.success)
						{
							if(json.html === '')
							{
								$("#spanNoEmailAddress").show();
							}
							else
							{
								$("#ulEmailAdd").append(json.html);
							}
						}
					},
					failure: function()
					{
						console.log('Error fetch email addresses');
					}
				});
			*/
		}
	});
</script>
<?php
}
else
{
	?>
	<h1><?php echo __('Open a New Ticket');?></h1>
	<p><?php echo __('An error occured.');?></p>
	<?php
}
?>
