<?php
	define('INCLUDE_DIR', true); // Required for below line..
	require_once('../../services/class.user.php');

	$staffId = (isset($_GET['staffId']) && $_GET['staffId'] !== '') ? $_GET['staffId'] : 0;
	$companyId = (isset($_GET['companyId']) && $_GET['companyId'] !== '') ? $_GET['companyId'] : 0;
	$accessToken = (isset($_GET['accessToken']) && $_GET['accessToken'] !== '') ? $_GET['accessToken'] : '';
    $html = '';

	if($companyId !== 0 && $accessToken !== '')
	{
		$arrayData = UserService::listUser($companyId, $accessToken);
		if(count($arrayData))
		{
			$html = '<option value="0">Please select</option>';
			foreach($arrayData as $row) 
			{
				$html .= "<option value='".$row->userId."' data-email='".$row->userName."' data-phone='".$row->phoneNumber."'>".strtoupper($row->surname)." ".ucfirst(strtolower($row->otherName))."</option>";
			}
		}
	}

	die( json_encode(array(
			'msg' => 'Successfully pulled Staffs',
			'success' => true,
			'html' => $html
		))
	);
?>