<?php
	define('INCLUDE_DIR', true); // Required for below line..
	require_once('../../services/class.tenant.php');

	$tenantId = (isset($_GET['tenantId']) && $_GET['tenantId'] !== '') ? $_GET['tenantId'] : 0;
	$subCompanyId = (isset($_GET['subCompanyId']) && $_GET['subCompanyId'] !== '') ? $_GET['subCompanyId'] : 0;
	$accessToken = (isset($_GET['accessToken']) && $_GET['accessToken'] !== '') ? $_GET['accessToken'] : '';
    $html = '';


	if($subCompanyId !== 0 && $accessToken !== '')
	{
		$arrayData = TenantService::listTenantsBySubCompanyId($subCompanyId, $accessToken);
		if(count($arrayData))
		{
			$html = '<option value="0">Please select</option>';
			foreach($arrayData as $row) 
			{
				$html .= "<option value='".$row->ticketTenantId."' data-email='".$row->tenantEmailAddress."' data-phone='".$row->tenantPhone."'>".$row->tenantName."</option>";
			}
		}
	}

	die( json_encode(array(
			'msg' => 'Successfully pulled Tenants',
			'success' => true,
			'html' => $html
		))
	);
?>