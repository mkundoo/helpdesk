<?php
    define('INCLUDE_DIR', true); // Required for below line..
    require_once('../../services/class.asset_type.php');

    $locationId = (isset($_GET['locationId']) && $_GET['locationId'] !== '') ? $_GET['locationId'] : 0;
    $accessToken = (isset($_GET['accessToken']) && $_GET['accessToken'] !== '') ? $_GET['accessToken'] : '';
    $html = '';

    // Fetch data from DB
    if($locationId !== 0 && $accessToken !== '')
    {
        $arrayData = AssetTypeService::listAssetTypes($locationId, $accessToken);
        if(count($arrayData))
        {
            $html = '<option value="0">Please select</option>';
            foreach($arrayData as $row)
            {
                $html .= "<option value='".$row->assetTypeId."'>".$row->assetTypeName."</option>";
            }
        }

        die( json_encode(array(
                'msg' => 'Successfully pulled Asset Types',
                'success' => true,
                'html' => $html
            ))
        );
    }

    // At this point there is an error
    die( json_encode(array(
        'msg' => 'An error occured',
        'success' => false,
        'html' => ''
        ))
    );
?>