<?php

class ExternalLogin {
    static function login($username, $password, $loginUserType = "agent")
    {
        $url = EC3_CURL_URL.':'.EC3_CURL_URL_PORT.'/api/login';

        $fields = [
            'userName' => $username,
            'password' => $password
        ];

        $payload = json_encode($fields);

        // Prepare new cURL resource
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: '.strlen($payload),
            'Accept: application/json'));

        // Submit the POST request
        $result = curl_exec($ch);
        // Close cURL session handle
        curl_close($ch);

        if(!$result) return null;

        $decodedJsonResult = json_decode($result,true);

        $decodedJsonResult["loginUserType"] = $loginUserType;

        return $decodedJsonResult;
    }

    // $result obtained from postExternalLogin function
    static function processExternalLoginUser($result, $defaultPassword = 'password')
    {
        if($result === null) return null;

        if (true)
        {
            $arResult = $result; // $result['result'];
            $userName = $arResult->userName;
            $name = (isset($arResult->surname) && $arResult->surname !== '' ? $arResult->surname : '');
            $name = ($name !== '') ? $name.' '.$arResult->otherName : $userName;
            $userId = $arResult->userId;
            $password = $defaultPassword; // $arResult->password;
            $listUserRole = $arResult->listRoleAndSubCompanyAsscociated;

            if($listUserRole !== null && count($listUserRole) > 0)
            {
                // Get list of Sub-Companies user is associated
                for($i = 0; $i < count($listUserRole); $i++)
                {
                    $subCompanyId[] = $listUserRole[$i]->subCompany->subCompanyId;
                }

                // Set the default Organisation for all Helpdesk Users
                $organization = Organization::fromVars(array("name" => "Ascensia"));
                // Build the comma-separated list of sub-company IDs
                $listSubCompanyId = (count($subCompanyId) > 0 ) ? implode(",", $subCompanyId) : NULL;

                if($arResult->loginUserType === "staff" /* && in_array($userRoleName,
                        [
                            "admin",
                            "agent",
                            "centre_manager",
                            "operations_manager"
                        ]) */ )
                {
                    $staff = Staff::lookup($userName);
                    if($staff === null)
                    {
                        $staff = Staff::create();
                        $deptId = Dept::getIdByName('Support');
                        $staff->setDepartmentId($deptId);
                        $roleName = 'Expanded Access';
                        $permissions = '';
                        $isAdmin = 0;
                        switch($userRoleName)
                        {
                            case "admin":
                                $roleName = 'All Access';
                                $isAdmin = 1;
                                $permissions = '{"user.create":1,"user.delete":1,"user.edit":1,"user.manage":1,"user.dir":1,"faq.manage":1,"org.create":1,"org.delete":1,"org.edit":1,"emails.banlist":1,"search.all":1,"stats.agents":1}';
                                break;
                            case "agent":
                                $roleName = 'Limited Access 2';
                                $permissions = '{"user.create":1,"user.edit":1,"user.dir":1,"faq.manage":1}';
                                break;
                            case "centre_manager":
                                $roleName = 'Limited Access 1';
                                $permissions = '{"user.create":1,"user.edit":1,"user.dir":1,"faq.manage":1}';
                                break;
                            case "operations_manager":
                                $roleName = 'Limited Access 1';
                                $permissions = '{"user.create":1,"user.edit":1,"user.dir":1,"faq.manage":1}';
                                break;
                            default:
                                $permissions = '{"ticket.assign":1,"ticket.close":1,"ticket.create":1,"ticket.edit":1,"ticket.reply":1,"ticket.refer":1,"ticket.release":1,"ticket.transfer":1,"task.assign":1,"task.close":1,"task.create":1,"task.edit":1,"task.reply":1,"task.transfer":1,"canned.manage":1}';
                                break;
                        }
                        $role = Role::lookup(array('name'=>$roleName));
                        $staff->setRoleId($role->getId());
                        $staff->setAdmin($isAdmin);
                        if($permissions !== '')
                        {
                            $staff->permissions = $permissions;
                        }
                        $staffPositionId = StaffPosition::getIdByCode($userRoleName);
                        if($staffPositionId !== null)
                        {
                            $staff->custom_staff_position_id = $staffPositionId;
                        }
                    }
                    $staff->setUsername($userName);
                    $staff->setFirstName($arResult->surname);
                    $staff->setLastName($arResult->otherName);
                    $staff->setPassword($password, null);
                    $staff->setEmail($userName);
                    $staff->setSubCompanyId($listSubCompanyId);
                    $staff->save();
                    return $staff;
                }
                else if ($arResult->loginUserType === "user")
                {
                    /*
                    $org = Organization::lookup($subCompanyId);
                    if($org === null)
                    {
                        $org = Organization::create();
                        $org->id = $subCompanyId;
                        $org->name = $subCompanyName;
                        $org->autoAddPrimaryContactsAsCollabs();
                        $org->updated = $org->getCreateDate();
                        $org->save();
                    }
                    */
                    $vars = array(
                        'email' => $userName,
                        'name' => $name
                    );
                    $accVars = array(
                        'username' => $userName,
                        'passwd1' => $password,
                        'passwd2' => $password
                    );
                    $updateUser = true;
                    if(User::lookupByEmail($userName) === null)
                    {
                        $updateUser = false;
                    }
                    
                    $user = User::fromVars($vars, !$updateUser, $updateUser);
                    
                    $user->setOrganization($organization);
                    if($user->hasAccount())
                    {
                        $acc = $user->getAccount();
                        $acc->update($accVars,$errors = array());
                    }
                    else
                    {
                        UserAccount::register($user,$accVars,$errors = array());
                    }
                    return $user;
                }
            }
            return null;
        }
        else 
        {
            
        
            $arResult = $result['result'];
            $userName = $arResult['userName'];
            $name = (isset($arResult['name']) && $arResult['name'] !== '' ? $arResult['name'] : $userName);
            $userId = $arResult['userId'];
            $password = $arResult['password'];
            $listUserRole = $arResult['listUserRole'];
            if($listUserRole !== null && count($listUserRole) > 0)
            {
                for($i = 0; $i < count($listUserRole); $i++)
                {
                    $userRoleName = $listUserRole[$i]["userRoleName"];
                    $subCompanyId = $listUserRole[$i]['subCompany']['subCompanyId'];
                    $subCompanyName = $listUserRole[$i]['subCompany']['name'];
                    if($result['loginUserType'] === "staff" && in_array($userRoleName,
                        [
                            "admin",
                            "agent",
                            "centre_manager",
                            "operations_manager"
                        ]))
                    {
                        $staff = Staff::lookup($userName);
                        if($staff === null)
                        {
                            $staff = Staff::create();
                            $deptId = Dept::getIdByName('Support');
                            $staff->setDepartmentId($deptId);
                            $roleName = 'Limited Access 2';
                            $permissions = '';
                            $isAdmin = 0;
                            switch($userRoleName)
                            {
                                case "admin":
                                    $roleName = 'All Access';
                                    $isAdmin = 1;
                                    $permissions = '{"user.create":1,"user.delete":1,"user.edit":1,"user.manage":1,"user.dir":1,"faq.manage":1,"org.create":1,"org.delete":1,"org.edit":1,"emails.banlist":1,"search.all":1,"stats.agents":1}';
                                    break;
                                case "agent":
                                    $roleName = 'Limited Access 2';
                                    $permissions = '{"user.create":1,"user.edit":1,"user.dir":1,"faq.manage":1}';
                                    break;
                                case "centre_manager":
                                    $roleName = 'Limited Access 1';
                                    $permissions = '{"user.create":1,"user.edit":1,"user.dir":1,"faq.manage":1}';
                                    break;
                                case "operations_manager":
                                    $roleName = 'Limited Access 1';
                                    $permissions = '{"user.create":1,"user.edit":1,"user.dir":1,"faq.manage":1}';
                                    break;
                                default:
                                    break;
                            }
                            $role = Role::lookup(array('name'=>$roleName));
                            $staff->setRoleId($role->getId());
                            $staff->setAdmin($isAdmin);
                            if($permissions !== '')
                            {
                                $staff->permissions = $permissions;
                            }
                            $staffPositionId = StaffPosition::getIdByCode($userRoleName);
                            if($staffPositionId !== null)
                            {
                                $staff->custom_staff_position_id = $staffPositionId;
                            }
                        }
                        $staff->setUsername($userName);
                        $staff->setFirstName($userName);
                        $staff->setLastName(null);
                        $staff->setPassword($password, null);
                        $staff->setEmail($userName);
                        $staff->setSubCompanyId($subCompanyId);
                        $staff->save();
                        return $staff;
                    }
                    else if ($result['loginUserType'] === "user" && $userRoleName == "helpdesk_user")
                    {
                        $org = Organization::lookup($subCompanyId);
                        if($org === null)
                        {
                            $org = Organization::create();
                            $org->id = $subCompanyId;
                            $org->name = $subCompanyName;
                            $org->autoAddPrimaryContactsAsCollabs();
                            $org->updated = $org->getCreateDate();
                            $org->save();
                        }
                        $vars = array(
                            'email' => $userName,
                            'name' => $name
                        );
                        $accVars = array(
                            'username' => $userName,
                            'passwd1' => $password,
                            'passwd2' => $password
                        );
                        $updateUser = true;
                        if(User::lookupByEmail($userName) === null)
                        {
                            $updateUser = false;
                        }
                        $user = User::fromVars($vars, !$updateUser, $updateUser);
                        $user->setOrganization($org);
                        if($user->hasAccount())
                        {
                            $acc = $user->getAccount();
                            $acc->update($accVars,$errors = array());
                        }
                        else
                        {
                            UserAccount::register($user,$accVars,$errors = array());
                        }
                        return $user;
                    }
                }
            }
            return null;
        }
    }

    static function fetchUserInfo($accessToken)
    {
        $url = EC3_CURL_URL.':'.EC3_CURL_URL_PORT.'/api/getUserInfo';

        // Prepare new cURL resource
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_GET, true);

        // Set HTTP Header for GET request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$accessToken,
            // 'Content-Length: '.strlen($payload),
            'Accept: application/json'));

        // Submit the GET request
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            var_dump(curl_error($ch)); die();
        }
        // Close cURL session handle
        curl_close($ch);

        return $result;
    }
}

?>