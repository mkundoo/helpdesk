<?php

// Start: Implementation of SSO. Get the access token

if (isset($_GET['accesstoken']) && isset($_COOKIE['accessToken']) && $_GET['accesstoken'] !== $_COOKIE['accessToken'])
{
    // If access token is different, then log current user out and log the new user in Helpdesk creation of ticket part.
    $thisclient->logOut();
    setcookie("accessToken", "", time() - 3600);
    osTicketSession::destroyCookie();
    setcookie('accessToken', $_GET['accesstoken']);
    Http::redirect('open.php?accesstoken='.$_GET['accesstoken']);
}

$accessToken = isset($_GET['accesstoken']) ? $_GET['accesstoken'] : $_COOKIE['accessToken'];
if ($accessToken === '' || $accessToken === null)
    die('An error occured. Please contact your Administrator.');

setcookie('accessToken', $accessToken);
// End: Implementation of SSO. Get the access token

?>