<?php

require('client.inc.php');

$logMsg = '';

try
{
    Ticket::checkOverdue(true);
    Task::checkOverdue(true);

    $logMsg = "Overdue check done!";
}
catch(Exception $ex)
{
    $logMsg = $e->getMessage();
}

db_query("INSERT INTO custom_daily_tasks_log (message) VALUES(".db_input($logMsg).")");

?>