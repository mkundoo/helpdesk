<?php
// Includes
require('includes.php');

class ServiceRelatedItemService {
    static function listServiceRelatedItemByZoneId($zoneId, $accessToken)
    {
        if ($zoneId == 0 || $accessToken == '')
            return array();
        
        $headers = array(
            'Content-Type: application/json',
            sprintf('Authorization: Bearer %s', $accessToken)
        );

        $curl = curl_init(EC3_CURL_URL.':'.EC3_CURL_URL_PORT.'/api/vi/serviceRelatedByZone?zoneId='.$zoneId);
       
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $result = json_decode(curl_exec($curl));

        return ($result) ? $result->result : array();
    }
}