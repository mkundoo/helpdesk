<?php
// Includes
require('includes.php');

class AssetService {
    static function listAssets($assetSubTypeId, $locationId, $accessToken)
    {
        if ($assetSubTypeId == 0 || $locationId == 0 || $accessToken == '')
            return array();
        
        $headers = array(
            'Content-Type: application/json',
            sprintf('Authorization: Bearer %s', $accessToken)
        );

        $curl = curl_init(EC3_CURL_URL.':'.EC3_CURL_URL_PORT.'/api/osticket/assets?assetSubTypeId='.$assetSubTypeId.'&locationId='.$locationId);
       
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $result = json_decode(curl_exec($curl));
        return ($result) ? $result->result : array();
    }
}