<?php
// Includes
require('includes.php');

class SubAssetTypeService {
    static function listSubAssetTypes($assetTypeId, $locationId, $accessToken)
    {
        if ($assetTypeId == 0 || $accessToken == '' || $locationId == 0 || $locationId == '')
            return array();
        
        $headers = array(
            'Content-Type: application/json',
            sprintf('Authorization: Bearer %s', $accessToken)
        );

        $curl = curl_init(EC3_CURL_URL.':'.EC3_CURL_URL_PORT.'/api/osticket/assetSubTypes?assetTypeId='.$assetTypeId.'&locationId='.$locationId);
       
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $result = json_decode(curl_exec($curl));

        return ($result) ? $result->result : array();
    }
}