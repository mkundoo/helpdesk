<?php

// Start: Implementation of SSO. Get the access token
$accessToken = isset($_GET['accesstoken']) ? $_GET['accesstoken'] : $_COOKIE['accessToken'];
if ($accessToken === '' || $accessToken === null)
    die('An error occured. Please contact your Administrator.');

setcookie('accessToken', $accessToken);
// End: Implementation of SSO. Get the access token

?>