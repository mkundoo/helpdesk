<?php
    define('INCLUDE_DIR', true); // Required for below line..
    require('../include/ost-config.php');

    $assetTypeId = (isset($_GET['assetTypeId']) && $_GET['assetTypeId'] !== '') ? $_GET['assetTypeId'] : 0;
    $html = '<option value="0">Please select</option>';

    // Fetch data from DB
    if($assetTypeId !== 0)
    {
        // DB Connnection to Asset Management App
        $myPDO = new PDO('pgsql:host='.DBHOST_EC3.';dbname='.DBNAME_EC3, DBUSER_EC3, DBPASS_EC3);
        $result = $myPDO->query("SELECT * FROM asset_sub_types WHERE asset_type_id = ". $assetTypeId);
        $arrayData = $result->fetchAll();
        if(count($arrayData))
        {
            foreach($arrayData as $row) 
            { 
                $html .= "<option value='".$row['asset_sub_type_id']."'>".$row['asset_sub_type_name']."</option>";
            }
        }
        else
        {
            $html = '';
        }

        die( json_encode(array(
                'msg' => 'Successfully pulled Asset Sub Types',
                'success' => true,
                'html' => $html
            ))
        );
    }

    // At this point there is an error
    die( json_encode(array(
        'msg' => 'An error occured',
        'success' => false,
        'html' => ''
        ))
    );
?>