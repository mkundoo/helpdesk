<?php
    define('INCLUDE_DIR', true); // Required for below line..
    require('../include/ost-config.php');

    $assetSubTypeId = (isset($_GET['assetSubTypeId']) && $_GET['assetSubTypeId'] !== '') ? $_GET['assetSubTypeId'] : 0;
    $html = '<option value="0">Please select</option>';

    // Fetch data from DB
    if($assetSubTypeId !== 0)
    {
        // DB Connnection to Asset Management App
        $myPDO = new PDO('pgsql:host='.DBHOST_EC3.';dbname='.DBNAME_EC3, DBUSER_EC3, DBPASS_EC3);
        $result = $myPDO->query("SELECT * FROM assets WHERE asset_sub_type_id = ".$assetSubTypeId." AND asset_is_deleted = false");
        $arrayData = $result->fetchAll();
        if(count($arrayData))
        {
            foreach($arrayData as $row) 
            { 
                $html .= "<option value='".$row['asset_id']."'>".$row['name']."</option>";
            }
        }
        else
        {
            $html = '';
        }

        die( json_encode(array(
                'msg' => 'Successfully pulled Assets',
                'success' => true,
                'html' => $html
            ))
        );
    }

    // At this point there is an error
    die( json_encode(array(
        'msg' => 'An error occured',
        'success' => false,
        'html' => ''
        ))
    );
?>