<?php
// Includes
require('includes.php');

if (isset($_POST) && isset($_POST['result']) && $_POST['result'] !== NULL)
{
    $result = json_decode($_POST['result']);
    $ec3TicketId = ($result->ec3TicketId !== NULL && $result->ec3TicketId !== '') ? (int)$result->ec3TicketId : NULL; // Ticket Id generated on ASCE
    $assignUserInfo = ($result->user !== NULL && $result->user !== '') ? $result->user : NULL; // Email address

    if ($ec3TicketId === NULL || $assignUserInfo === NULL)
    {
        die(ApiError::invalidRequest());
    }
    else
    {
        // 1). Check if user exists in Helpdesk
        $userEmail = $assignUserInfo->userName;

        if ($userEmail === NULL || $userEmail === '')
            die(ApiError::invalidEmailAddress());

        // 2). Create the user as Staff in table ost_staff
        $staffId = 0;
        $agentId = 0;
        $user = new User($userEmail);
        $staffObj = $user->getUser();
       
        if ($staffObj === false)
        {
            // User does not exist in OsTicket, then create user.
            $staffId = $user->create($assignUserInfo);
            if ($staffId)
            {
                // Fetch the newly created user
                $staffObj = $user->getUserById($staffId);
                if (!$staffObj)
                {
                    die(ApiError::userNotFound());
                }
            }
            else
            {
                die(ApiError::userCouldNotBeCreated());
            }
        }
		else
		{
			$staffId = (int)$staffObj['staff_id'];
		}

        // 3). Create the user as Agent in table ost_user
        $agentId = $user->getAgentId();
		 
        if (!$agentId)
        {
            $agentId = $user->createAgent($staffObj);
            if (!$agentId)
                die(ApiError::userCouldNotBeCreated());
        }

        // 4). Assign the ticket
        if ($staffId && $agentId)
        {
            $success = Ticket::assignTo($ec3TicketId, $staffId);
            if ($success)
            {
                die(
                    json_encode(
                        array(
                            'success' => true
                        )
                    )
                );
            }
            else
            {
                die(ApiError::assignmentError());
            }
        }
        else
        {
            die(ApiError::userCouldNotBeCreated());
        }
    }
}
else
{
    die(ApiError::invalidRequest());
}