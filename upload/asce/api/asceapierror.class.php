<?php

class ApiError {
    static function invalidRequest()
    {
        return json_encode(array(
                'message' => 'Invalid Request.',
                'success' => false
        ));
    }

    static function invalidEmailAddress()
    {
        return json_encode(array(
                'message' => 'No email address provided.',
                'success' => false
        ));
    }

    static function userCouldNotBeCreated()
    {
        return json_encode(array(
            'message' => 'User could not be created.',
            'success' => false
        ));
    }

    static function userNotFound()
    {
        return json_encode(array(
            'message' => 'User could not be found.',
            'success' => false
        ));
    }

    static function noTicket()
    {
        return json_encode(array(
            'message' => 'Ticket array passed by VIMA is empty.',
            'success' => false,
            'tickets' => array()
        ));
    }

    static function noTicketIds()
    {
        return json_encode(array(
            'message' => 'Ticket IDs array is empty.',
            'success' => false,
            'tickets' => array()
        ));
    }

    static function noTicketFound()
    {
        return json_encode(array(
            'message' => 'No ticket found.',
            'success' => false,
            'tickets' => array()
        ));
    }

    static function assignmentError()
    {
        return json_encode(array(
            'message' => 'Assigning ticket unsuccessful due to invalid ec3_ticket_id.',
            'success' => false
        ));
    }
    
}