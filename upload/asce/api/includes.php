<?php
#Get real path for root dir ---linux and windows
$here = dirname(__FILE__);
$here = ($h = realpath($here)) ? $h : $here;
$root_dir = str_replace('\\', '/', $here.'/');
$root_dir = str_replace('asce/api/', '', $root_dir);
unset($here); unset($h);

// Define constants
define('ROOT_DIR', $root_dir);
define('INCLUDE_DIR', ROOT_DIR . 'include/');

// INCLUDES
include_once(INCLUDE_DIR.'ost-config.php');
include_once(INCLUDE_DIR.'class.passwd.php');

require_once('asceapierror.class.php');
require_once('user.class.php');
require_once('ticket.class.php');
?>