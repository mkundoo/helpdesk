<?php

class Ticket {
    private $_connPDO;
    var $ticketsToCreate;
    var $staffId;
    var $agentId;
    var $firstname;
    var $lastname;
    var $maxTicketLength = 6;

    function __construct($ticketsToCreate, $userObj, $agentId)
    {
        $this->ticketsToCreate = $ticketsToCreate;
        $this->staffId = (int)$userObj['staff_id'];
        $this->firstname = $userObj['firstname'];
        $this->lastname = $userObj['lastname'];
        $this->agentId = (int)$agentId;
        $this->_connPDO = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME, DBUSER, DBPASS);
    }

    function create()
    {
        $hasError = false;
        $createdTickets = array('tickets' => array());
        $connPDO = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME, DBUSER, DBPASS);

        foreach($this->ticketsToCreate as $ticket)
        {
            $errorMessage = NULL;

            // Check if the user to whom the ticket is assigned exists in Helpdesk
            $userTicketAssignedObj = ($ticket->user_assigned !== NULL) ? $ticket->user_assigned : NULL;
            $userTicketAssignedEmail = ($userTicketAssignedObj !== NULL) ? $ticket->user_assigned->userName : NULL;
            
            if ($userTicketAssignedEmail !== NULL)
            {
                $staff_id = 0;
                
                // Check if the user exists as Staff (table: OST_STAFF)
                $staffResult = $connPDO->query("SELECT * FROM ost_staff WHERE email = '".$userTicketAssignedEmail."'");
                $staff = $staffResult->fetch();
                
                if ($staff === FALSE)
                {
                    try {
                        // Create Staff in OsTicket
                        $passwordHash = Passwd::hash('password');

                        // Get all Sub Companies associated to the user
                        $subCompanyIDArray = array();
                        $subCompanyId = NULL;
                        foreach($userTicketAssignedObj->listRoleAndSubCompanyAsscociated as $listRoleAndSubCompanyAsscociated)
                        {
                            $subCompany = $listRoleAndSubCompanyAsscociated->subCompany;
                            if ($subCompany !== null || $subCompany !== '')
                            {
                                array_push($subCompanyIDArray, $subCompany->subCompanyId);
                            }
                        }

                        if (count($subCompanyIDArray) > 1)
                        {
                            $subCompanyId = implode(',', $subCompanyIDArray);
                        }
                        else if (count($subCompanyIDArray) === 1)
                        {
                            $subCompanyId = $subCompanyIDArray[0];
                        }
                        else
                        {
                            $subCompanyId = NULL;
                        }

                        // set the PDO error mode to exception
                        $this->_connPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $sql = "INSERT INTO ost_staff (
                            dept_id,
                            role_id,
                            username,
                            firstname,
                            lastname,
                            passwd,
                            email,
                            signature,
                            extra,
                            permissions,
                            created,
                            updated,
                            ar_sub_company_id
                        )
                        VALUES (
                            1, -- Support
                            2, -- Expanded Access
                            '".$userTicketAssignedObj->userName."',
                            '".$userTicketAssignedObj->otherName."',
                            '".$userTicketAssignedObj->surname."',
                            '".$passwordHash."',
                            '".$userTicketAssignedObj->userName."',
                            '".ucfirst(strtolower($userTicketAssignedObj->otherName))." ".ucfirst(strtolower($userTicketAssignedObj->surname))."',
                            '{\'def_assn_role\':false,\'browser_lang\':en_US}',
                            '{\'ticket.assign\':1,\'ticket.close\':1,\'ticket.create\':1,\'ticket.edit\':1,\'ticket.reply\':1,\'ticket.refer\':1,\'ticket.release\':1,\'ticket.transfer\':1,\'task.assign\':1,\'task.close\':1,\'task.create\':1,\'task.edit\':1,\'task.reply\':1,\'task.transfer\':1,\'canned.manage\':1}',
                            NOW(),
                            NOW(),
                            '".$subCompanyId."'
                        )";

                        $connPDO->exec($sql);
                        $staff_id = $connPDO->lastInsertId();
                    }
                    catch(PDOException $e)
                    {
                        $staff_id = 0;
                        $hasError = true;
                    
                        // Staff to whom to assign the ticket could not be created on Helpdesk
                        array_push($createdTickets['tickets'], array(
                                'os_ticket_number' => '', 
                                'issue_summary' => $ticket->issue_summary, 
                                'ec3_ticket_id' => $ticket->ec3_ticket_id, 
                                'success' => false,
                                'message' => $e->getMessage()
                            )
                        );
                    }
                }
                else
                {
                    $staff_id = $staff["staff_id"];
                }

                if ($staff_id === 0 || $staff_id === NULL || $staff_id === '')
                {
                    $hasError = true;
                    // Staff to whom to assign the ticket could not be created on Helpdesk
                    array_push($createdTickets['tickets'], array(
                            'os_ticket_number' => '', 
                            'issue_summary' => $ticket->issue_summary, 
                            'ec3_ticket_id' => $ticket->ec3_ticket_id, 
                            'success' => false,
                            'message' => "Staff could not be found on Helpdesk"
                        )
                    );

                    return;
                }

                // OST_TICKET
                $user_id = $this->agentId;
                $user_email_id = 0;
                $status_id = TicketStatus::getStatusByState('open');
                $dept_id = 1;
                $sla_id = (strtolower($ticket->ticket_priority) === 'high') ? SlaPlan::getSLAByPriority('high') : 0;
                $sla_id = (strtolower($ticket->ticket_priority) === 'medium') ? SlaPlan::getSLAByPriority('medium') : $sla_id;
                $topic_id = Topic::getTopicIdByName($ticket->ticket_category);
                $staff_id = $staff_id; // To whom the ticket is assigned based on rules set in ASCE
                $team_id = 0;
                $lock_id = 0;
                $flags = 0;
                $ip_address = '';
                $source = 'Visual Inspection';
                $isoverdue = 0;
                $isanswered = 0;
                $created = date('Y-m-d H:i:s');
                $updated = date('Y-m-d H:i:s');
                $ar_sub_company_id = $ticket->subcompany_id;
                $escalation_level = 0;
                $days_since_overdue = 0;
                $escalation_active = 1;
                $source_of_ticket_id = $ticket->source_ticket_id;
                $ec3_ticket_id = $ticket->ec3_ticket_id;
                $request_from_list_item_id = RequestFromListItem::getIdByCode('staff'); // From VI, userType defaults to 'staff' always
                $request_from_staff_id = $userTicketAssignedObj->userId; // The userId from ASCE
                
                // OST_TICKET__CDATA
                $subject = $ticket->issue_summary;
                $priority = TicketPriority::getSLAByPriority($ticket->ticket_priority);

                // OST_THREAD_ENTRY
                $body = $ticket->ticket_details;
                $poster = $this->firstname.' '.$this->lastname;

                // OST_THREAD_EVENT
                $event_id = Event::getEventByCode('created');
                $username = $this->firstname.' '.$this->lastname;
                $uid_type = 'U';
                $annulled = 0;

                // OST_FORM_ENTRY
                $object_type = 'T';
                $sort = 0;
                $extra = '{"disable":[38,39]}';

                try{
                    $connPDO->beginTransaction();

                    // Insert: OST_TICKET
                    $connPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $sql ="INSERT INTO ost_ticket (
                        user_id,
                        user_email_id,
                        status_id,
                        dept_id,
                        sla_id,
                        topic_id,
                        staff_id,
                        team_id,
                        lock_id,
                        flags,
                        ip_address,
                        source,
                        isoverdue,
                        isanswered,
                        created,
                        updated,
                        ar_sub_company_id,
                        escalation_level,
                        days_since_overdue,
                        escalation_active,
                        ec3_ticket_id,
                        request_from_list_item_id,
                        request_from_staff_id
                    ) 
                    VALUES (
                        ".$user_id.",
                        ".$user_email_id.",
                        ".$status_id.",
                        ".$dept_id.",
                        ".$sla_id.",
                        ".$topic_id.",
                        ".$staff_id.",
                        ".$team_id.",
                        ".$lock_id.",
                        ".$flags.",
                        '".$ip_address."',
                        '".$source."',
                        ".$isoverdue.",
                        ".$isanswered.",
                        '".$created."',
                        '".$updated."',
                        ".$ar_sub_company_id.",
                        ".$escalation_level.",
                        ".$days_since_overdue.",
                        ".$escalation_active.",
                        ".$ec3_ticket_id.",
                        ".$request_from_list_item_id.",
                        ".$request_from_staff_id."
                    )";
                    $connPDO->exec($sql);
                    $ticket_id = $connPDO->lastInsertId();

                    // Update the 'ticket_number' for the current ticket
                    $stringifyTicketId = (string)$ticket_id;
                    $missingCharCount = ($this->maxTicketLength - strlen($stringifyTicketId));
                    $missingChar='';
                    for ($i = 0; $i < $missingCharCount; $i++)
                    {
                        $missingChar .= '0';
                    }
                    $ticket_number = 'VI'.$missingChar.$stringifyTicketId;
                    $sql = "UPDATE ost_ticket SET number = '".$ticket_number."' WHERE ticket_id = ".(int)$ticket_id;
                    $connPDO->exec($sql);

                    // Insert: OST_TICKET__CDATA
                    $sql = "INSERT INTO ost_ticket__cdata (
                        ticket_id,
                        priority,
                        subject                    
                    )
                    VALUES
                    (
                        ".$ticket_id.",
                        ".$priority.",
                        '".$subject."'
                    )";
                    $connPDO->exec($sql);


                    // Insert: OST_THREAD
                    $sql = "INSERT INTO ost_thread (
                        object_id,
                        object_type,
                        created                    
                    )
                    VALUES
                    (
                        ".$ticket_id.",
                        'T',
                        '".$created."'
                    )";
                    $connPDO->exec($sql);
                    $thread_id = $connPDO->lastInsertId();


                    // Insert: OST_THREAD_ENTRY
                    $sql = "INSERT INTO ost_thread_entry (
                        thread_id,
                        staff_id,
                        user_id,
                        type,
                        poster,
                        title,
                        body,
                        format,
                        created                   
                    )
                    VALUES
                    (
                        ".$thread_id.",
                        ".$staff_id.",
                        ".$user_id.",
                        'M',
                        '".$poster."',
                        '".$subject."',
                        '".$body."',
                        'html',
                        '".$created."'
                    )";
                    $connPDO->exec($sql);


                    // Insert: OST_THREAD_EVENT table
                    $sql = "INSERT INTO ost_thread_event (
                        thread_id,
                        event_id,
                        staff_id,
                        team_id,
                        dept_id,
                        topic_id,
                        username,
                        uid,
                        uid_type,
                        annulled,
                        timestamp                   
                    )
                    VALUES
                    (
                        ".$thread_id.",
                        ".(int)$event_id.",
                        ".$staff_id.",
                        ".$team_id.",
                        ".$dept_id.",
                        ".$topic_id.",
                        '".$username."',
                        ".$user_id.",
                        'U',
                        0,
                        '".$created."'
                    )";
                    $connPDO->exec($sql);


                    // Insert: ost_form_entry table
                    $sql = "INSERT INTO ost_form_entry (
                        form_id,
                        object_id,
                        object_type,
                        sort,
                        extra,
                        created,
                        updated                  
                    )
                    VALUES
                    (
                        (SELECT id from ost_form WHERE type = 'T'),
                        ".$ticket_id.",
                        '".$object_type."',
                        ".$sort.",
                        '".$extra."',
                        '".$created."',
                        '".$created."'
                    )";
                    $connPDO->exec($sql);
                    $form_entry_id = $connPDO->lastInsertId();

                    // Insert: ost_form_entry_values
                    // Get all fields associated to the form first
                    $results = $connPDO->query("SELECT id, name FROM ost_form_field WHERE form_id = (SELECT id from ost_form WHERE type = 'T')");
                    $formFields = $results->fetchAll();
                    $sql = '';
                    foreach($formFields as $formField)
                    {
                        if ($formField['name'] === 'subject')
                        {
                            $sql .= "INSERT INTO ost_form_entry_values (entry_id, field_id, value) VALUES (".$form_entry_id.", ".$formField['id'].", '".$subject."');";
                        }
                        else if ($formField['name'] === 'priority')
                        {
                            $sql .= "INSERT INTO ost_form_entry_values (entry_id, field_id, value, value_id) VALUES (".$form_entry_id.", ".$formField['id'].", (SELECT priority_desc FROM ost_ticket_priority WHERE priority_id = ".(int)$priority."), '".$priority."');";
                        }
                        else
                        {
                            $sql .= "INSERT INTO ost_form_entry_values (entry_id, field_id, value, value_id) VALUES (".$form_entry_id.", ".$formField['id'].", NULL, NULL);";
                        }
                    }
                    if ($sql !== '')
                    {
                        $connPDO->exec($sql);
                    }

                    // Commit the ticket
                    $connPDO->commit();

                    // Add entry in array to return to EC3
                    array_push($createdTickets['tickets'], array(
                            'os_ticket_number' => $ticket_number, 
                            'issue_summary' => $subject, 
                            'ec3_ticket_id' => $ec3_ticket_id, 
                            'success' => true,
                            'message' => "Successfully created ticket on Helpdesk"
                        )
                    );
                }
                catch(PDOException $e)
                {
                    // TODO: Log the error and rollback
                    $hasError = true;
                    array_push($createdTickets['tickets'], array(
                            'os_ticket_number' => '', 
                            'issue_summary' => $subject, 
                            'ec3_ticket_id' => $ec3_ticket_id, 
                            'success' => false,
                            'message' => $e->getMessage()
                        )
                    );
                    $connPDO->rollBack();
                }
            }
            else
            {
                $hasError = true;
                // There is a problem with the userAssigned object being passed to this API.
                array_push($createdTickets['tickets'], array(
                        'os_ticket_number' => '', 
                        'issue_summary' => $subject, 
                        'ec3_ticket_id' => $ec3_ticket_id, 
                        'success' => false,
                        'message' => "Invalid user assigned object provided."
                    )
                );
            }
        }

        $createdTickets['message'] = ($hasError) ? "Not all tickets created." : "Tickets created.";
        $createdTickets['success'] = ($hasError) ? false : true;
        $connPDO = null;

        return $createdTickets;
    }

    public function getTasks($ticketId)
    {
        if ($ticketId !== NULL && $ticketId !== '')
        {
            $sql = "SELECT
                    ost_ticket.ec3_ticket_id as ticket_id,
                    ost_task__cdata.title, 
                    concat(ost_staff.firstname, CONCAT(' ', ost_staff.lastname)) as staff_name,
                    ost_task.closed as closed_date,
                    ost_task.created as created_date
                FROM ost_task
                INNER JOIN ost_ticket ON ost_ticket.ticket_id = ost_task.object_id
                LEFT JOIN ost_task__cdata ON ost_task__cdata.task_id = ost_task.id 
                LEFT JOIN ost_staff ON ost_staff.staff_id = ost_task.staff_id
                WHERE ost_task.object_id = (SELECT ticket_id FROM ost_ticket WHERE ec3_ticket_id = ".$ticketId.") 
                AND ost_task.object_type = 'T'";
			
			$connPDO = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME, DBUSER, DBPASS);
            $results = $connPDO->query($sql);
            $qResult = $results->fetchAll(PDO::FETCH_ASSOC);
			$connPDO = null;
			
            return $qResult;
        }
        return NULL;
    }

    public function getTickets($listOfTicketIds)
    {
        if (is_array($listOfTicketIds) && count($listOfTicketIds) > 0)
        {
            $sql = '';
            $count = count($listOfTicketIds);
            foreach ($listOfTicketIds as $index => $ticketIdFromEc3)
            {
                // Build the sql query
                $sql .= "
                        SELECT  
                        ost_ticket.ticket_id as ticket_id,
                        ost_ticket.ec3_ticket_id as ec3_ticket_id,
                        ost_ticket.number as ticket_number,
                        (
                            SELECT COUNT(*)
                            FROM ost_task
                            WHERE ost_task.object_type = 'T'
                            AND ost_task.object_id = ost_ticket.ticket_id
                        ) as task_count,
                        ost_ticket.created as date_created,
                        CASE
                            WHEN ost_ticket.isoverdue = 1 AND ost_ticket.closed IS NULL THEN 'Overdue'
                            WHEN ost_ticket.created IS NOT NULL AND ost_ticket.closed IS NOT NULL THEN 'closed'
                            ELSE 'open'
                        END AS ticket_status,
                        ost_ticket__cdata.subject as summary,
                        ost_help_topic.topic as category
                    FROM ost_ticket
                    LEFT JOIN ost_help_topic ON ost_help_topic.topic_id = ost_ticket.topic_id
                    LEFT JOIN ost_ticket__cdata ON ost_ticket__cdata.ticket_id = ost_ticket.ticket_id
                    WHERE ost_ticket.ec3_ticket_id = ".$ticketIdFromEc3." 
                ";
                $sql .= ($sql !== '' && ($index + 1) !== $count) ? " UNION ALL " : ' ';
            } 

            if ($sql !== '')
            {
				$connPDO = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME, DBUSER, DBPASS);
                $results = $connPDO->query($sql);
                $qResult = $results->fetchAll(PDO::FETCH_ASSOC);
				$connPDO = null;
                return $qResult;
            }
        }

        return NULL;
    }
}