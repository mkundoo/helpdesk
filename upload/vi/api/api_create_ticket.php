<?php
// Includes
require('includes.php');

if (isset($_POST) && isset($_POST['result']) && $_POST['result'] !== NULL)
{
    $result = json_decode($_POST['result']);
    $userInfo = ($result->user !== NULL) ? $result->user : NULL; // User from VI
    $ticketList = ($result->listTicket !== NULL) ? $result->listTicket : NULL;

    if ($userInfo !== NULL)
    {
        /* START:  creation of user */
        // Check for valid email address
        $userEmail = $userInfo->userName;
        if ($userEmail === NULL || $userEmail === '')
        {
            die(ApiError::invalidEmailAddress());
        }
        
        // Check if user exists in Osticket
        $user = new User($userEmail);
        $userObj = $user->getUser(); // User from OsTicket
        $isAgentCreated = 0;

        if ($userObj === false)
        {
            // User does not exist in OsTicket, then create user.
            $isCreated = $user->create($userInfo);
            if ($isCreated)
            {
                // Fetch the newly created user
                $userObj = $user->getUserById($isCreated);
                if (!$userObj)
                {
                    die(ApiError::userNotFound());
                }

                // We also need to create the user in ost_user table. This is for the user to be able to access creation ticket form and to view tickets.
                $isAgentCreated = $user->createAgent($userObj);
            }
            else
            {
                die(ApiError::userCouldNotBeCreated());
            }
        }
        else
        {
            $isAgentCreated = $user->getAgentId();
        }
        /* END:  creation of user */
        
        /* START:  creation of tickets */
        if ($ticketList === NULL)
        {
            die(ApiError::noTicket());
        }
        else
        {
            $ticket = new Ticket($ticketList, $userObj, $isAgentCreated);
            $ticketsCreated = $ticket->create(); // This will return a list of all tickets created in Osticket with the auto-generated ticket_number
            die(json_encode($ticketsCreated));
        }
        /* END:  creation of tickets */
    }
}
else
{
    die(ApiError::invalidRequest());
}




