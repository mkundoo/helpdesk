<?php
// Includes
require('includes.php');

if (isset($_GET) && isset($_GET['ticketId']) && isset($_GET['accesstoken']))
{
    $jsonArray = array('message' => 'Successful', 'success' => true, 'ticket_tasks' => null);
    $ticketId = $_GET['ticketId']; // This is the ID which is saved in ASCE when generating new tickets.
    $ticket = new Ticket(null,  null, null);
    $jsonArray['ticket_tasks'] = $ticket->getTasks($ticketId);
    die(json_encode($jsonArray));
}
else 
{
    die(ApiError::invalidRequest());
}
?>