<?php

class ApiError {
    static function invalidRequest()
    {
        return json_encode(array(
                'message' => 'Invalid Request.',
                'success' => false,
                'tickets' => array()
        ));
    }

    static function invalidEmailAddress()
    {
        return json_encode(array(
                'message' => 'No email address provided.',
                'success' => false,
                'tickets' => array()
        ));
    }

    static function userCouldNotBeCreated()
    {
        return json_encode(array(
            'message' => 'User could not be created.',
            'success' => false,
            'tickets' => array()
        ));
    }

    static function userNotFound()
    {
        return json_encode(array(
            'message' => 'User could not be found.',
            'success' => false,
            'tickets' => array()
        ));
    }

    static function noTicket()
    {
        return json_encode(array(
            'message' => 'Ticket array passed by VIMA is empty.',
            'success' => false,
            'tickets' => array()
        ));
    }

    static function noTicketIds()
    {
        return json_encode(array(
            'message' => 'Ticket IDs array is empty.',
            'success' => false,
            'tickets' => array()
        ));
    }

    static function noTicketFound()
    {
        return json_encode(array(
            'message' => 'No ticket found.',
            'success' => false,
            'tickets' => array()
        ));
    }
    
}