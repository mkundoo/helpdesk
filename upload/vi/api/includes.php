<?php
#Get real path for root dir ---linux and windows
$here = dirname(__FILE__);
$here = ($h = realpath($here)) ? $h : $here;
$root_dir = str_replace('\\', '/', $here.'/');
$root_dir = str_replace('vi/api/', '', $root_dir);
unset($here); unset($h);

// Define constants
define('ROOT_DIR', $root_dir);
define('INCLUDE_DIR', ROOT_DIR . 'include/');

// INCLUDES
include_once(INCLUDE_DIR.'ost-config.php');
include_once(INCLUDE_DIR.'class.passwd.php');
// include_once(INCLUDE_DIR.'class.sequence.php');
require_once('user.class.php');
require_once('topic.class.php');
require_once('ticket.status.class.php');
require_once('slaplan.class.php');
require_once('ticket.priority.class.php');
require_once('request.from.list.item.class.php');
require_once('event.class.php');
require_once('ticket.class.php');
require_once('apierror.class.php');

?>