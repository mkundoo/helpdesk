<?php
// Includes
require('includes.php');

// Get the JSON object from the HTTP Body
$body = file_get_contents('php://input');
$object = json_decode($body);

if ($_GET['accesstoken'] !== '' && $_GET['accesstoken'] !== NULL && $object !== NULL && $object->ticketId !== null)
{
    $ticketIds = $object->ticketId;
	if (count($ticketIds) > 0)
	{
		$ticket = new Ticket(null,  null, null);
		$tickets = $ticket::getTickets($ticketIds);

		if ($tickets !== NULL)
		{
			die(
				json_encode(array(
				'message' => 'Successfully retrieved tickets.',
				'success' => true,
				'tickets' => $tickets
			)));
		}
		else
		{
			die(ApiError::noTicketFound());
		}
	}
	else
	{
		die(ApiError::noTicketIds());
	}

}
else
{
    die(ApiError::invalidRequest());
}

?>