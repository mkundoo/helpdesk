<?php

class User {
    private $_connPDO;
    var $email;

    function __construct($userEmail)
    {
        $this->email = $userEmail;
        $this->_connPDO = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME, DBUSER, DBPASS);
    }

    public function getUser()
    {
        $user = $this->_connPDO->query("SELECT * FROM ost_staff WHERE email = '".$this->email."'");
        return $user->fetch();
    }

    public function getAgentId()
    {
        $agentObj = $this->_connPDO->query("SELECT user_id FROM ost_user_email WHERE address = '".$this->email."'");
        $result = $agentObj->fetch();
        return ($result !== FALSE) ? $result['user_id'] : 0;
    }

    public function getUserById($id)
    {
        $user = $this->_connPDO->query("SELECT * FROM ost_staff WHERE staff_id = '".$id."'");
        return $user->fetch();
    }

    public function create($userInfo)
    {
        if ($userInfo === null || $userInfo === '')
            return false;
        
        $last_id = 0;
        try {
            // Create Staff in OsTicket
            $passwordHash = Passwd::hash('password');

            // Get all Sub Companies associated to the user
            $subCompanyIDArray = array();
            $subCompanyId = NULL;
            foreach($userInfo->listRoleAndSubCompanyAsscociated as $listRoleAndSubCompanyAsscociated)
            {
                $subCompany = $listRoleAndSubCompanyAsscociated->subCompany;
                if ($subCompany !== null || $subCompany !== '')
                {
                    array_push($subCompanyIDArray, $subCompany->subCompanyId);
                }
            }

            if (count($subCompanyIDArray) > 1)
            {
                $subCompanyId = implode(',', $subCompanyIDArray);
            }
            else if (count($subCompanyIDArray) === 1)
            {
                $subCompanyId = $subCompanyIDArray[0];
            }
            else
            {
                $subCompanyId = NULL;
            }

            // set the PDO error mode to exception
            $this->_connPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO ost_staff (
                dept_id,
                role_id,
                username,
                firstname,
                lastname,
                passwd,
                email,
                signature,
                extra,
                permissions,
                created,
                updated,
                ar_sub_company_id
            )
            VALUES (
                1, -- Support
                2, -- Expanded Access
                '".$userInfo->userName."',
                '".$userInfo->otherName."',
                '".$userInfo->surname."',
                '".$passwordHash."',
                '".$userInfo->userName."',
                '".ucfirst(strtolower($userInfo->otherName))." ".ucfirst(strtolower($userInfo->surname))."',
                '{\'def_assn_role\':false,\'browser_lang\':en_US}',
                '{\'ticket.assign\':1,\'ticket.close\':1,\'ticket.create\':1,\'ticket.edit\':1,\'ticket.reply\':1,\'ticket.refer\':1,\'ticket.release\':1,\'ticket.transfer\':1,\'task.assign\':1,\'task.close\':1,\'task.create\':1,\'task.edit\':1,\'task.reply\':1,\'task.transfer\':1,\'canned.manage\':1}',
                NOW(),
                NOW(),
                '".$subCompanyId."'
            )";

            $this->_connPDO->exec($sql);
            $last_id = $this->_connPDO->lastInsertId();
        }
        catch(PDOException $e)
        {
            // var_dump($e->getMessage());
            // die();
            $last_id = 0;
        }

        return $last_id;
    }

    function createAgent($userInfo)
    {
        try
        {
            $this->_connPDO->beginTransaction();
            $this->_connPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $name = ucfirst(strtolower($userInfo['firstname'])).' '.ucfirst(strtolower($userInfo['lastname']));
            // Create User in OsTicket
            $passwordHash = Passwd::hash('password');

            // Insert into ost_user
            $sql = "INSERT INTO ost_user (
                org_id,
                default_email_id,
                status,
                name,
                created,
                updated
            ) 
            VALUES
            (
                0,
                0,
                0,
                '".$name."',
                '".date('Y-m-d H:i:s')."',
                '".date('Y-m-d H:i:s')."'
            )";
            $this->_connPDO->exec($sql);
            $user_id = $this->_connPDO->lastInsertId();

            // Insert into ost_user_account
            $sql = "INSERT INTO ost_user_account (
                user_id,
                status,
                passwd,
                extra,
                registered
            ) 
            VALUES
            (
                ".(int)$user_id.",
                1,
                '".$passwordHash."',
                '{\'browser_lang\':\'en_US\'}',
                '".date('Y-m-d H:i:s')."'
            )";
            $this->_connPDO->exec($sql);

            // Insert into ost_user_email
            $sql = "INSERT INTO ost_user_email (
                user_id,
                flags,
                address
            ) 
            VALUES
            (
                ".(int)$user_id.",
                0,
                '".$userInfo['email']."'
            )";
            $this->_connPDO->exec($sql);
            $user_default_email_id = $this->_connPDO->lastInsertId();

            // Update user default email in ost_user table
            $sql = "UPDATE ost_user SET default_email_id = ".$user_default_email_id." WHERE id = ".(int)$user_id;
            $this->_connPDO->exec($sql);
            
            $this->_connPDO->commit();
        }
        catch(PDOException $e)
        {
            // TODO: Log the error and rollback
            $this->_connPDO->rollBack();
            // var_dump($e->getMessage()); die();
            $user_id = 0;
        }

        return $user_id;
    }
}